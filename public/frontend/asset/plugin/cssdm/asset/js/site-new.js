﻿
$(document).ready(function () {
    $('.editContentPage').click(function (e) {
        e.preventDefault();
        var edit = $(this).attr("href") == "#edit";
        $.get("/CMS/EditMode/" + edit, function (data) {
            window.location.href = window.location.href;
        });
    });

    $('.editContentArea').click(function (e) {
        e.preventDefault();
        newModal("Edit Content", "/CMS/EditPageContent/" + $(this).attr("href"), 600, 800);
    });

    $('.editContentControl').change(function (e) {
        $.post("/CMS/UpdateContentControl/" + $(this).attr("href"), { control: $(this).val() }, function (data) {
            if(data != "Success")
                alert(data);
        });
    });
}); 

var modalDiv = null;
function newModal(title, href, height, width) {
    if (modalDiv != null) {
        modalDiv.remove();
        modalDiv = null;
    }
    modalDiv = $('<div title="' + title + '">')
    modalDiv.html("<img alt='Loading...' src='https://monnit.blob.core.windows.net/content/images/ajax-loader.gif'/>");

    if (height == null) height = 450;
    if (width == null) width = 550;
    modalDiv.dialog({
        height: height,
        width: width,
        modal: true
    });

    if (href.length > 0) {
        jQuery.get(href, function (data) {
            modalDiv.html(data);
        });
    }
    
}

function hideModal() {
    modalDiv.dialog('close');
    modalDiv.remove();
    modalDiv = null;
}

function postModal(callback, returnType) {
    var form = modalDiv.children('form');

    $.post(form.attr("action"), form.serialize(),
        function (data) {
            modalDiv.html(data);

            if (typeof callback == "string") {
                eval(callback);
            }
            if (jQuery.isFunction(callback)) {
                callback(data);
            }
        }, returnType);
        //.success(function(){alert('success');})
        //.error(function(){alert('error');})
        //.complete(function (jqXHR) { alert('complete'); });

    modalDiv.html("<img alt='Loading...' src='https://monnit.blob.core.windows.net/content/images/ajax-loader.gif'/>");
}


function logout() {
    window.location.href = 'https://www.monnit.com/Account/LogOff';
}

function addToCart(id, element) {
    var button = $(element);
    if (button.length > 0) {
        button.hide().after("<img src='https://monnit.blob.core.windows.net/content/images/ajax-loader.gif'>");
    }
    $.get("/Order/QuickAddToCart/" + id, function (data) {
        if (button.length > 0) {
            button.show().next().remove();
        }
        $('#cart').html(data);//.parent().animateCart();
        $('#cartNotification').slideDown('fast', function () {
            setTimeout("$('#cartNotification').slideUp('slow');", 1500);
        });
    });
}


