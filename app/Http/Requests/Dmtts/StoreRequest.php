<?php

namespace App\Http\Requests\Dmtts;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'            => 'required|unique:skl_dmtt,name',
            'id_parent'       => 'required',
            'vitri'           => 'required',

        ];
    }

    public function messages()
    {
        return [
            'name.required'         => trans('message.name_required'),
            'name.unique'           => trans('message.name_unique'),
            'id_parent.required'    => trans('message.category_required'),
            'vitri.required'        => trans('message.vitri_required'),

        ];
    }
}
