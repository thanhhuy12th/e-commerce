<?php

namespace App\Http\Requests\Logins;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'          => 'required|max:50',
            'password'          => 'required|min:6|max:16',
            'email'             => 'required|unique:skl_member,email|email',
            'fullname'          => 'required|max:50',
            'phone'             => 'required|unique:skl_member,phone|min:10|max:11',            
        ];
    }

    public function messages()
    {
        return [
            'username.required'         => trans('message.username_required'),
            'username.max'              => trans('message.username_max'),

            'password.required'         => trans('message.password_required'),
            'password.min'              => trans('message.passwordnew_min'),
            'password.max'              => trans('message.passwordnew_max'),

            'email.required'            => trans('message.email_required'),
            'email.unique'              => trans('message.email_unique'),
            'email.email'               => trans('message.email_email'),

            'fullname.required'         => trans('message.fullname_required'),
            'fullname.max'              => trans('message.fullname_max'),
            
            'phone.required'            => trans('message.phone_required'),
            'phone.unique'              => trans('message.phone_unique'),
            'phone.min'                 => trans('message.phone_min'),
            'phone.max'                 => trans('message.phone_max'),
        ];
    }
}
