<?php

namespace App\Http\Requests\Orders;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'           => 'required',
            'hoten'           => 'required',
            'sodt'            => 'required|min:10|max:11',
            'diachi'          => 'required',
            
            

        ];
    }

    public function messages()
    {
        return [
            'email.required'         => trans('message.email_required'),
            'hoten.required'         => trans('message.fullname_required'),
            'sodt.required'          => trans('message.phone_required'),
            'diachi.required'        => trans('message.diachi_required'),
            'sodt.min'               => trans('message.phone_min'),
            'sodt.max'               => trans('message.phone_max'),

        ];
    }
}
