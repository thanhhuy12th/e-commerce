<?php

namespace App\Http\Requests\Hinhthucs;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|unique:skl_hinhthuc,name',
        ];
    }

    public function messages()
    {
        return [
            'name.required'         => trans('message.name_required'),
            'name.unique'           => trans('message.name_unique'),
        ];
    }
}
