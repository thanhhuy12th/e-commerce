<?php

namespace App\Http\Requests\Lienhes;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'email'             => 'required',
            'hoten'             => 'required',
            'sdt'               => 'required',
            'noidung'           => 'required'
            
        ];
    }

    public function messages()
    {
        return [
            'email.required'         => trans('message.email_required'),
            'hoten.required'         => trans('message.fullname_required'),
            'sdt.required'           => trans('message.phone_required'),
            'noidung.required'       => trans('message.content_required')
        ];
    }
}
