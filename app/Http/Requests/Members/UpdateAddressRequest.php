<?php

namespace App\Http\Requests\Members;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'      => 'required',
            'fullname'      => 'required',
            'phone'         => 'required',
            'diachi'        => 'required',
            'province'      => 'required',
            'district'      => 'required',
            'ward'          => 'required'

            
        ];
    }

    public function messages()
    {
        return [
            'username.required'         => trans('message.username_required'),
            'username.unique'           => trans('message.username_unique'),
            'fullname.required'         => trans('message.fullname_required'),
            'phone.required'            => trans('message.phone_required'),
            'diachi.required'           => trans('message.diachi_required'),
            'province.required'         => trans('message.province_required'),
            'district.required'         => trans('message.district_required'),
            'ward.required'             => trans('message.ward_required')
        ];
    }
}
