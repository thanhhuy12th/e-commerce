<?php

namespace App\Http\Requests\Slides;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tieude'       => 'required',
            'photos'       => 'required',

        ];
    }

    public function messages()
    {
        return [
            'tieude.required'         => trans('message.title_required'),
            'photos.required'         => trans('message.photos_required'),

        ];
    }
}
