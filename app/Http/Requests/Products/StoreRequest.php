<?php

namespace App\Http\Requests\Products;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tensp'         => 'required|unique:skl_sanpham,tensp',
            'category'      => 'required',
            'price'         => 'required',
            'photos'        => 'required'
        ];
    }

    public function messages()
    {
        return [
            'tensp.required'         => trans('message.tensp_required'),
            'tensp.unique'           => trans('message.tensp_unique'),
            'category.required'      => trans('message.category_required'),
            'price.required'         => trans('message.price_required'),
            'photos.required'        => trans('message.photos_required')
        ];
    }
}
