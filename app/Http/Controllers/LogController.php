<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;

class LogController extends Controller
{
    public function log($content)
    {
    	$exists = Storage::disk('local')->exists('log.txt');
        if($exists)
        {
            $log = Storage::prepend('log.txt', $content.'<br>');
        }else{
            $log = Storage::disk('local')->put('log.txt', $content.'<br>');
        }
        if($log)
        {
        	return true;
        }
        else
        {
        	return false;
        }
    }
}
