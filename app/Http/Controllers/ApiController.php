<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cart;

class ApiController extends Controller
{

    
    public function getProvince($id = null)
    {
        $data = array();

        if($id == null)
        {
            $data = DB::table('skl_province')->orderBy('name','ASC')->get()->toArray();
        }
        else
        {
            $data = DB::table('skl_province')->where('provinceid','=',$id)->orderBy('name','ASC')->get()->toArray();

        }
        echo json_encode($data);
    }

    public function getDistrict($id = null)
    {
        $data = array();
        
        if($id == null)
        {
            $data = DB::table('skl_district')->orderBy('name','ASC')->get()->toArray();

        }
        else
        {
            $data = DB::table('skl_district')->where('provinceid','=',(int)$id)->orderBy('name','ASC')->get()->toArray();

        }
        
        echo json_encode($data);
    }   

    public function getWard($id = null)
    {
        $data = array();
        
        if($id == null)
        {
            $data = DB::table('skl_ward')->orderBy('name','ASC')->get()->toArray();

        }
        else
        {
            $data = DB::table('skl_ward')->where('districtid','=',(int)$id)->orderBy('name','ASC')->get()->toArray();

        }
        
        echo json_encode($data);
    }
    public function getHinhthuc($id = null)
    {
        $data = array();
        
        if($id == null)
        {
            $data = DB::table('skl_hinhthuc')->orderBy('id','ASC')->get()->toArray();

        }       
        echo json_encode($data);
    }

    public function getVanchuyen($id = null)
    {
        $data = array();
        
        if($id == null)
        {
            $data = DB::table('skl_vanchuyen')->orderBy('id','ASC')->get()->toArray();

        }       
        echo json_encode($data);
    }

    public function getGiamgia()
    {
        $data = DB::table('skl_giamgia')->get()->toArray();   
        echo json_encode($data);
    }

}
