<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\Orders\StoreRequest;
use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\LogController;
use Illuminate\Support\Facades\Storage;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Model\Sanpham;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\District;
use App\Model\Ward;
use App\Model\Province;
use App\Model\HinhThuc;
use App\Model\Vanchuyen;
use App\Model\Cauhinh;
use App\Model\Member;
use App\Model\Donhang;
use App\Model\Coupon;
use Cart,Auth;
use Illuminate\Database\QueryException;

class ShoppingCartController extends Controller
{
	private $sanpham;
	private $district;//tinh
    private $ward;
    private $province;
    private $hinhthuc;
    private $vanchuyen;
    private $donhang;
    private $orderdetail;
    private $cauhinh;
    private $member;
    private $coupon;
    private $logcontroller;
	public function __construct(
                                LogController $logcontroller,
                                Sanpham     $sanpham,
                                Order     	$order,
                                OrderDetail $orderdetail,
                                Province 	$province,
                                District 	$district,
                                Ward 		$ward,
                                Hinhthuc    $hinhthuc,
                                Cauhinh     $cauhinh,
                                Vanchuyen   $vanchuyen,
                                Member      $member,
                                Coupon      $coupon
                                )
    {
        $this->logcontroller    = $logcontroller;
        $this->sanpham          = $sanpham;
        $this->order          	= $order;
        $this->orderdetail      = $orderdetail;
        $this->district 		= $district;
        $this->ward 			= $ward;
        $this->province 		= $province;
        $this->hinhthuc         = $hinhthuc;
        $this->vanchuyen        = $vanchuyen;
        $this->cauhinh          = $cauhinh;
        $this->member           = $member;
        $this->coupon           = $coupon;

    }
    public function addProductToCart($id)
	{

	 	$product = $this->sanpham->getProduct($id);
	 	if ($product) {
            $cartInfo = [
                            'id'      => $id, 
                            'name'    => $product->tensp, 
                            'qty'     => 1, 
                            'price'   => $product->price_km, 
                            'weight'  => 550, 
                            'options' => [
                                        'img'          => $product->img,
                                        'price_goc'    => $product->price,
                                        'type_giamgia' => $product->type_giamgia
            							 ]
            			];
            Cart::add($cartInfo);
        }
        return redirect()->back();
	}

	 /**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 * sản phẩm trong giỏ hàng của bạn
	 */
	public function listCartProduct()
	{
            $data['products'] = Cart::content();
            $data['tongtien'] = str_replace(',', '', Cart::total());
            return view('frontend.pages.list_cart',$data);
	}
	public function updownQtyProduct()
	{
        if (Request::get('product_id') && (Request::get('increment')) == 1) {
            $rows = Cart::search(function($key, $value) {
                return $key->id == Request::get('product_id');
            });
            $item = $rows->first();
            Cart::update($item->rowId, $item->qty + 1);
        }

        //decrease the quantity
        if (Request::get('product_id') && (Request::get('decrease')) == 1) {
            $rows = Cart::search(function($key, $value) {
                return $key->id == Request::get('product_id');
            });
            $item = $rows->first();
            Cart::update($item->rowId, $item->qty - 1);
        }
        return redirect()->back();
	}
    public function sendmail($email_gmail,$password_gmail,$tenwebsite,$email_receive,$subject,$body)
    {
        $mail = new PHPMailer(true);                            // Passing `true` enables exceptions
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        ); 
        $mail->CharSet    = 'UTF-8';
        $mail->SMTPDebug  = 0;                                  // Enable verbose debug output
        $mail->isSMTP();                                        // Set mailer to use SMTP                  
        $mail->Host       = 'smtp.gmail.com';                   // Specify main and backup SMTP servers
        $mail->SMTPAuth   = true;                               // Enable SMTP authentication
        $mail->Username   = $email_gmail;                       // SMTP username
        $mail->Password   = $password_gmail;                    // SMTP password
        $mail->SMTPSecure = 'tls';                              // Enable TLS encryption, `ssl` also accepted
        $mail->Port       = 587;                                // TCP port to connect to
        $mail->setFrom( $email_gmail, $tenwebsite);
        $mail->addAddress($email_receive, 'Chào bạn');          // Add a recipient, Name is optional
        $mail->addReplyTo($email_gmail, 'Mailer');
        $mail->isHTML(true);                                                                    
        $mail->Subject    = $subject;
        $mail->Body       = $body;
        return $mail;
    }
    public function getDathang() 
    {
        if(count(Cart::content())>0)
        {
            $data['giatamtinh'] = str_replace(',', '', Cart::total());
            $data['token']      = Request::get('token');
            $data['listcarts']  = Cart::content();
            $gettype  = Cart::content();
            $arr = [];
                //upload tung image
                $arr = [];
                $errMsg1="";
                foreach ($gettype as $type) 
                {
                    array_push($arr, $type->options->type_giamgia);
                }
                if (!empty($arr)) 
                {
                    for ($i = 0; $i<count($arr); $i++) 
                    {
                    if($i + 1 == count($arr))
                        $errMsg1 .= $arr[$i]; 
                    else 
                        $errMsg1 .= $arr[$i]."||"; 
                    }
                }
                else 
                {
                        $errMsg1 = "NULL";
                } 
            $data['getType']      = $errMsg1;
            $data['hinhthuc']   = $this->hinhthuc->getHinhthucData();
            $data['vanchuyen']  = $this->vanchuyen->getVanchuyenData();
            return view('frontend.pages.dathang',$data);
        }else{
            return redirect()->back()->with('alertwar2','Vui lòng chọn sản phẩm trước khi đặt hàng');
        }
    }
    public function postDathang(StoreRequest $request) 
    {

        $email_gmail     = $this->cauhinh->getData('email_gmail')->chitiet;
        $password_gmail  = $this->cauhinh->getData('password_gmail')->chitiet;
        $tenwebsite      = $this->cauhinh->getData('tenwebsite')->chitiet;
        $baseurl         = $this->cauhinh->getData('linkwebsite')->chitiet;
        $emailthongbao   = "lemaiquyen.cntt2019@gmail.com";
        $cartInfor       = Cart::content();
        $tonggoc         = 0;
        $token           = substr(MD5(rand(0,10000)),0,16);
        $getToken        = $this->order->getData();
        foreach ($getToken as $key){
            while ($token == $key->token) {
            $token = substr(MD5(rand(0,10000)),0,16);
            }
        }
        foreach ($cartInfor as $item){
            $giagoc = $item->options->price_goc*$item->qty;
            $tonggoc = $tonggoc + $giagoc;
        }
        $tonggiam = $tonggoc-str_replace(',', '', Cart::total());
        $order                  = new Order;
        $order->hoten           = $request->hoten;
        $order->email           = $request->email;
        $order->sodt            = $request->sodt;
        $order->diachi          = $request->diachi;
        $order->province        = $request->province;
        $order->district        = $request->district;
        $order->ward            = $request->ward;
        $order->hinhthuc        = $request->hinhthuc;
        $order->phivanchuyen    = $request->phivanchuyen;
        $order->ghichu          = $request->ghichu;
        $order->token           = $token;
        $order->tonggiamgia     = $tonggiam;
        // $order->tongtien        = str_replace(',', '', Cart::total())+$request->phivanchuyen;
        $order->tongtien        = $request->laytongtien;
        try {
            $order->save();
        } catch (QueryException $e) {
            $this->logcontroller->log($e->getMessage());
            return redirect()->back()->with('alerterrv2','Vui lòng nhập đúng số điện thoại');
        }
        if (count($cartInfor) >0) {
            foreach ($cartInfor as $item) {
             $data = array([
                         "id_order"      => $order->id,
                         "id_sanpham"    => $item->id,
                         "ten_sp"        => $item->name,
                         "soluong"       => $item->qty,
                         "price"         => $item->options->price_goc,
                         "price_km"      => $item->price,
                        ]);
             $this->orderdetail->storeOrderDetail($data);
            }
        }
       

        if(!$this->member->getMemberEmail($request->email))
        {
            $randomPw = "PW".substr(MD5(rand(0,10000)),0,8);
            $data   = array(
                        'username'      => $request->hoten,
                        'password'      => bcrypt($randomPw),
                        'fullname'      => $request->hoten,
                        'phone'         => $request->sodt,
                        'email'         => $request->email,
                        'diachi'        => $request->diachi,
                        'province'      => $request->province,
                        'district'      => $request->district,
                        'ward'          => $request->ward,
                        'tongtienmua'   => $order->tongtien,
                        'solanmua'      => 1,
                    ); 
            $this->member->store($data);
            if(isset($request->email))
            {
                $subject = "XacNhanDonHang";
                $body = $this->bodyDonhang($order->hoten,
                    $order->sodt,
                    $tenwebsite,
                    $order->email,
                    $order->diachi,
                    getWard($order->ward),
                    getDistrict($order->district),
                    getProvince($order->province),
                    select_hinhthuc($order->hinhthuc)->name,
                    select_vanchuyen($order->phivanchuyen)->name,
                    convert_money($order->phivanchuyen),
                    $order->id,
                    date_format($order->created_at, 'd-m-Y H:i:s'),
                    convert_money($order->tonggiamgia),
                    convert_money($order->tongtien),
                    $baseurl,
                    $order->token,
                    $email_gmail,
                    $randomPw,
                    convert_money($order->tongtien-$order->phivanchuyen));
                // $body = "<span>Tai khoan : ".$request->email."</span><br><span>Mat khau : ".$randomPw."</span><br><a href='".$baseurl."xem-lai-hoa-don?token=".$order->token."'>Click vào đây nếu bạn muốn xem thông tin đơn hàng</a>";                
                try {
                    $this->sendmail($email_gmail,$password_gmail,$tenwebsite,$request->email,$subject,$body)->send();
                } catch (Exception $e) {
                    $this->logcontroller->log($e->getMessage());
                    return redirect()->back()->with('alerterr','Đặt hàng không thành công xin vui lòng thử lại');
                }
            }
        }else{
            $getId          = $this->member->getMemberEmail($request->email)->id;
            $getTongTienMua = $this->member->getMemberEmail($request->email)->tongtienmua;
            $getSoLanmua    = $this->member->getMemberEmail($request->email)->solanmua;
            $data   = array(
                        'username'      => $request->hoten,
                        'fullname'      => $request->hoten,
                        'phone'         => $request->sodt,
                        'email'         => $request->email,
                        'diachi'        => $request->diachi,
                        'province'      => $request->province,
                        'district'      => $request->district,
                        'ward'          => $request->ward,
                        'tongtienmua'   => $order->tongtien,
                        'solanmua'      => 1,
                        'tongtienmua'   => $getTongTienMua+$order->tongtien,
                        'solanmua'      => $getSoLanmua+1,
                    );
            if(isset($request->email))
            {
                $subject = "XacNhanDonHang";
                $body = $this->bodyDonhang1($order->hoten,
                    $order->sodt,
                    $tenwebsite,
                    $order->email,
                    $order->diachi,
                    getWard($order->ward),
                    getDistrict($order->district),
                    getProvince($order->province),
                    select_hinhthuc($order->hinhthuc)->name,
                    select_vanchuyen($order->phivanchuyen)->name,
                    convert_money($order->phivanchuyen),
                    $order->id,
                    date_format($order->created_at, 'd-m-Y H:i:s'),
                    convert_money($order->tonggiamgia),
                    convert_money($order->tongtien),
                    $baseurl,
                    $order->token,
                    $email_gmail,
                    convert_money($order->tongtien-$order->phivanchuyen));
                // $body = "<a href='".$baseurl."xem-lai-hoa-don?token=".$order->token."'>Click vào đây nếu bạn muốn xem thông tin đơn hàng</a>";
                try {
                    $this->sendmail($email_gmail,$password_gmail,$tenwebsite,$request->email,$subject,$body)->send();
                } catch (Exception $e) {
                    $this->logcontroller->log($e->getMessage());
                    return redirect()->back()->with('alerterr','Đặt hàng không thành công xin vui lòng thử lại');
                }
            }
            $this->member->updataMember($data,$getId);
        }
        if(!empty($emailthongbao))
        {
            $subject = "CoDonHang";
            $body = $this->bodyDonhang2($order->hoten,
                    $order->sodt,
                    $tenwebsite,
                    $order->email,
                    $order->diachi,
                    getWard($order->ward),
                    getDistrict($order->district),
                    getProvince($order->province),
                    select_hinhthuc($order->hinhthuc)->name,
                    select_vanchuyen($order->phivanchuyen)->name,
                    convert_money($order->phivanchuyen),
                    $order->id,
                    date_format($order->created_at, 'd-m-Y H:i:s'),
                    convert_money($order->tonggiamgia),
                    convert_money($order->tongtien),
                    $baseurl,
                    $order->token,
                    $email_gmail,
                    convert_money($order->tongtien-$order->phivanchuyen));
            try {
                    $this->sendmail($email_gmail,$password_gmail,$tenwebsite,$emailthongbao,$subject,$body)->send();
                } catch (Exception $e) {
                    $this->logcontroller->log($e->getMessage());
                    return redirect()->back()->with('alerterr','Đặt hàng không thành công xin vui lòng thử lại');
                }
        }
    	return redirect('xem-lai-hoa-don/?token='.$order->token);
        
    }
    public function getRepostCart() 
    {
            // Cart::destroy();s
            $data['token'] = Request::get('token');
            return view('frontend.pages.thanks',$data);
    }
    public function detroyProductCart($id) 
    {
 		$item = Cart::search(function ($cart, $key) use($id) {
   			if($cart->id == $id)
   			{
   				Cart::remove($key);
   			}
		})->first();
		return redirect()->back();
	}
	public function detroyAllProductCart() 
    {
 		Cart::destroy();
		return redirect()->route('trangchu.index');
	}
    public function getCart()
    {
        $data = Cart::content();
        echo json_encode($data);
    }
    public function bodyDonhang($hoten,$sodt,$tenwebsite,$email,$diachi,$ward,$district,$province,$hinhthuc,$tenphi,$phivanchuyen,$id,$created_at,$tonggiamgia,$tongtien,$baseurl,$token,$email_gmail,$randomPw,$giatrukhuyenmai)
    {
        $cartInfor       = Cart::content();
        $listsanpham="";
        foreach($cartInfor as $cif)
        {
            $listsanpham .='<tr style="border:1px solid #d7d7d7"><td style="padding:5px 10px"> '.$cif->name.' </td><td style="text-align:center;padding:5px 10px"></td><td style="text-align:center;padding:5px 10px">'.$cif->qty.'</td><td style="padding:5px 10px;text-align:right">'.convert_money($cif->options->price_goc*$cif->qty).' </td></tr>';
        }
        $Content = '<p>Xin chào '.$hoten.'</p><p>Cảm ơn Anh/chị đã đặt hàng tại <strong> '.$tenwebsite.'</strong>!</p><p>Đây là tài khoản và mật khẩu quý khách có thể dùng để đăng nhập và mua hàng tại website.</p><p>Tài khoản: <strong>'.$email.'</strong></p><p>Mật khẩu: <strong>'.$randomPw.'</strong></p><p>Tài khoản này sẽ giúp quý khách mua hàng thuận tiện và nhanh chóng hơn.</p><p>Lưu ý: <strong>Quý khách xin vui lòng thay đổi mật khẩu sau khi đăng nhập.</strong></p><p>Đơn hàng của Anh/chị đã được tiếp nhận, chúng tôi sẽ nhanh chóng liên hệ với Anh/chị.</p><div><table style="width:100%;border-collapse:collapse"><thead><tr><th style="border-left:1px solid #d7d7d7;border-right:1px solid #d7d7d7;border-top:1px solid #d7d7d7;padding:5px 10px;text-align:left"><strong>Thông tin người mua</strong></th><th style="border-left:1px solid #d7d7d7;border-right:1px solid #d7d7d7;border-top:1px solid #d7d7d7;padding:5px 10px;text-align:left"><strong>Thông tin người nhận</strong></th></tr></thead><tbody><tr><td style="border-left:1px solid #d7d7d7;border-right:1px solid #d7d7d7;border-bottom:1px solid #d7d7d7;padding:5px 10px"><table style="width:100%"><tbody><tr><td>Họ tên:</td><td>'.$hoten.'</td></tr><tr><td>Điện thoại:</td><td>'.$sodt.'</td></tr><tr><td>Email:</td><td><a href="mailto:'.$email.'" target="_blank">'.$email.'</a></td></tr><tr><td>Địa chỉ:</td><td>'.$diachi.'</td></tr><tr><td>Quận huyện:</td><td>'.$ward.','.$district.'</td></tr><tr><td>Tỉnh thành:</td><td>'.$province.'</td></tr></tbody></table></td><td style="border-left:1px solid #d7d7d7;border-right:1px solid #d7d7d7;border-bottom:1px solid #d7d7d7;padding:5px 10px"><table style="width:100%"><tbody><tr><td>Họ tên:</td><td>'.$hoten.'</td></tr><tr><td>Điện thoại:</td><td>'.$sodt.'</td></tr><tr><td>Email:</td><td><a href="mailto:'.$email.'" target="_blank">'.$email.'</a></td></tr><tr><td>Địa chỉ:</td><td>'.$diachi.'</td></tr><tr><td>Quận huyện:</td><td>'.$ward.','.$district.'</td></tr><tr><td>Tỉnh thành:</td><td>'.$province.'</td></tr></tbody></table></td></tr><tr><td colspan="2" style="border-left:1px solid #d7d7d7;border-right:1px solid #d7d7d7;border-bottom:1px solid #d7d7d7;padding:5px 10px"><p><strong>Hình thức thanh toán: </strong>'.$hinhthuc.'</p><p><strong>Hình thức vận chuyển: </strong>'.$tenphi.'<br></p></td></tr></tbody></table></div><div><div style="font-size:18px;padding-top:10px"><strong>Thông tin đơn hàng</strong></div><table><tbody><tr><td>Mã đơn hàng: <strong>#'.$id.'</strong></td><td style="padding-left:40px">Ngày tạo: '.$created_at.'</td></tr></tbody></table><table style="width:100%;border-collapse:collapse"><thead><tr style="border:1px solid #d7d7d7;background-color:#f8f8f8"><th style="padding:5px 10px;text-align:left"><strong>Sản phẩm</strong></th><th style="text-align:center;padding:5px 10px"><strong>Mã SKU</strong></th><th style="text-align:center;padding:5px 10px"><strong>Số lượng</strong></th><th style="padding:5px 10px;text-align:right"><strong>Tổng</strong></th></tr></thead><tbody>'.$listsanpham.'<tr style="border:1px solid #d7d7d7"><td colspan="2">&nbsp;</td><td colspan="2"><table style="width:100%"><tbody><tr><td><strong>Giảm giá</strong></td><td style="text-align:right">'.$tonggiamgia.' VNĐ</td></tr><tr><td><strong>Giá trừ khuyến mãi:</strong></td><td style="text-align:right">'.$giatrukhuyenmai.' VNĐ</td></tr><tr><td><strong>Phí vận chuyển:</strong></td><td style="text-align:right">'.$phivanchuyen.' VNĐ</td></tr><tr><td><strong>Thành tiền</strong></td><td style="text-align:right">'.$tongtien.' VNĐ</td></tr></tbody></table></td></tr></tbody></table></div><hr><p style="text-align:right"><a href="'.$baseurl.'xem-lai-hoa-don?token='.$token.'">Chi tiết đơn hàng</a> <br/>Nếu Anh/chị có bất kỳ câu hỏi nào, xin liên hệ với chúng tôi tại <span style="color:#17a3dd"><a href="mailto:'.$email_gmail.'" target="_blank">'.$email_gmail.'</a></span></p><p style="text-align:right"><i>Trân trọng,</i></p><p style="text-align:right"><strong>Ban quản trị '.$tenwebsite.'</strong></p><div class="yj6qo"></div><div class="adL"></div></div>';
        return $Content;
    }
    public function bodyDonhang1($hoten,$sodt,$tenwebsite,$email,$diachi,$ward,$district,$province,$hinhthuc,$tenphi,$phivanchuyen,$id,$created_at,$tonggiamgia,$tongtien,$baseurl,$token,$email_gmail,$giatrukhuyenmai)
    {
        $cartInfor       = Cart::content();
        $listsanpham="";
        foreach($cartInfor as $cif)
        {
            $listsanpham .='<tr style="border:1px solid #d7d7d7"><td style="padding:5px 10px"> '.$cif->name.' </td><td style="text-align:center;padding:5px 10px"></td><td style="text-align:center;padding:5px 10px">'.$cif->qty.'</td><td style="padding:5px 10px;text-align:right">'.convert_money($cif->options->price_goc*$cif->qty).' VNĐ</td></tr>';
        }
        $Content = '<p>Xin chào '.$hoten.'</p><p>Cảm ơn Anh/chị đã đặt hàng tại <strong> '.$tenwebsite.'</strong>!</p><p>Đơn hàng của Anh/chị đã được tiếp nhận, chúng tôi sẽ nhanh chóng liên hệ với Anh/chị.</p><div><table style="width:100%;border-collapse:collapse"><thead><tr><th style="border-left:1px solid #d7d7d7;border-right:1px solid #d7d7d7;border-top:1px solid #d7d7d7;padding:5px 10px;text-align:left"><strong>Thông tin người mua</strong></th><th style="border-left:1px solid #d7d7d7;border-right:1px solid #d7d7d7;border-top:1px solid #d7d7d7;padding:5px 10px;text-align:left"><strong>Thông tin người nhận</strong></th></tr></thead><tbody><tr><td style="border-left:1px solid #d7d7d7;border-right:1px solid #d7d7d7;border-bottom:1px solid #d7d7d7;padding:5px 10px"><table style="width:100%"><tbody><tr><td>Họ tên:</td><td>'.$hoten.'</td></tr><tr><td>Điện thoại:</td><td>'.$sodt.'</td></tr><tr><td>Email:</td><td><a href="mailto:'.$email.'" target="_blank">'.$email.'</a></td></tr><tr><td>Địa chỉ:</td><td>'.$diachi.'</td></tr><tr><td>Quận huyện:</td><td>'.$ward.','.$district.'</td></tr><tr><td>Tỉnh thành:</td><td>'.$province.'</td></tr></tbody></table></td><td style="border-left:1px solid #d7d7d7;border-right:1px solid #d7d7d7;border-bottom:1px solid #d7d7d7;padding:5px 10px"><table style="width:100%"><tbody><tr><td>Họ tên:</td><td>'.$hoten.'</td></tr><tr><td>Điện thoại:</td><td>'.$sodt.'</td></tr><tr><td>Email:</td><td><a href="mailto:'.$email.'" target="_blank">'.$email.'</a></td></tr><tr><td>Địa chỉ:</td><td>'.$diachi.'</td></tr><tr><td>Quận huyện:</td><td>'.$ward.','.$district.'</td></tr><tr><td>Tỉnh thành:</td><td>'.$province.'</td></tr></tbody></table></td></tr><tr><td colspan="2" style="border-left:1px solid #d7d7d7;border-right:1px solid #d7d7d7;border-bottom:1px solid #d7d7d7;padding:5px 10px"><p><strong>Hình thức thanh toán: </strong>'.$hinhthuc.'</p><p><strong>Hình thức vận chuyển: </strong>'.$tenphi.'<br></p></td></tr></tbody></table></div><div><div style="font-size:18px;padding-top:10px"><strong>Thông tin đơn hàng</strong></div><table><tbody><tr><td>Mã đơn hàng: <strong>#'.$id.'</strong></td><td style="padding-left:40px">Ngày tạo: '.$created_at.'</td></tr></tbody></table><table style="width:100%;border-collapse:collapse"><thead><tr style="border:1px solid #d7d7d7;background-color:#f8f8f8"><th style="padding:5px 10px;text-align:left"><strong>Sản phẩm</strong></th><th style="text-align:center;padding:5px 10px"><strong>Mã SKU</strong></th><th style="text-align:center;padding:5px 10px"><strong>Số lượng</strong></th><th style="padding:5px 10px;text-align:right"><strong>Tổng</strong></th></tr></thead><tbody>'.$listsanpham.'<tr style="border:1px solid #d7d7d7"><td colspan="2">&nbsp;</td><td colspan="2"><table style="width:100%"><tbody><tr><td><strong>Giảm giá</strong></td><td style="text-align:right">'.$tonggiamgia.' VNĐ</td></tr><tr><td><strong>Giá trừ khuyến mãi:</strong></td><td style="text-align:right">'.$giatrukhuyenmai.' VNĐ</td></tr><tr><td><strong>Phí vận chuyển:</strong></td><td style="text-align:right">'.$phivanchuyen.' VNĐ</td></tr><tr><td><strong>Thành tiền</strong></td><td style="text-align:right">'.$tongtien.' VNĐ</td></tr></tbody></table></td></tr></tbody></table></div><hr><p style="text-align:right"><a href="'.$baseurl.'xem-lai-hoa-don?token='.$token.'">Chi tiết đơn hàng</a> <br/>Nếu Anh/chị có bất kỳ câu hỏi nào, xin liên hệ với chúng tôi tại <span style="color:#17a3dd"><a href="mailto:'.$email_gmail.'" target="_blank">'.$email_gmail.'</a></span></p><p style="text-align:right"><i>Trân trọng,</i></p><p style="text-align:right"><strong>Ban quản trị '.$tenwebsite.'</strong></p><div class="yj6qo"></div><div class="adL"></div></div>';
        return $Content;
    }
    public function bodyDonhang2($hoten,$sodt,$tenwebsite,$email,$diachi,$ward,$district,$province,$hinhthuc,$tenphi,$phivanchuyen,$id,$created_at,$tonggiamgia,$tongtien,$baseurl,$token,$email_gmail,$giatrukhuyenmai)
    {
        $cartInfor       = Cart::content();
        $listsanpham="";
        foreach($cartInfor as $cif)
        {
            $listsanpham .='<tr style="border:1px solid #d7d7d7"><td style="padding:5px 10px"> '.$cif->name.' </td><td style="text-align:center;padding:5px 10px"></td><td style="text-align:center;padding:5px 10px">'.$cif->qty.'</td><td style="padding:5px 10px;text-align:right">'.convert_money($cif->options->price_goc*$cif->qty).' </td></tr>';
        }
        $Content = '<p>Xin chào Admin,</p><p>Khách hàng đã gửi một yêu cầu đặt hàng mới đến cửa hàng của Anh/chị.</p><div><table style="width:100%;border-collapse:collapse"><thead><tr><th style="border-left:1px solid #d7d7d7;border-right:1px solid #d7d7d7;border-top:1px solid #d7d7d7;padding:5px 10px;text-align:left"><strong>Thông tin người mua</strong></th><th style="border-left:1px solid #d7d7d7;border-right:1px solid #d7d7d7;border-top:1px solid #d7d7d7;padding:5px 10px;text-align:left"><strong>Thông tin người nhận</strong></th></tr></thead><tbody><tr><td style="border-left:1px solid #d7d7d7;border-right:1px solid #d7d7d7;border-bottom:1px solid #d7d7d7;padding:5px 10px"><table style="width:100%"><tbody><tr><td>Họ tên:</td><td>'.$hoten.'</td></tr><tr><td>Điện thoại:</td><td>'.$sodt.'</td></tr><tr><td>Email:</td><td><a href="mailto:'.$email.'" target="_blank">'.$email.'</a></td></tr><tr><td>Địa chỉ:</td><td>'.$diachi.'</td></tr><tr><td>Quận huyện:</td><td>'.$ward.','.$district.'</td></tr><tr><td>Tỉnh thành:</td><td>'.$province.'</td></tr></tbody></table></td><td style="border-left:1px solid #d7d7d7;border-right:1px solid #d7d7d7;border-bottom:1px solid #d7d7d7;padding:5px 10px"><table style="width:100%"><tbody><tr><td>Họ tên:</td><td>'.$hoten.'</td></tr><tr><td>Điện thoại:</td><td>'.$sodt.'</td></tr><tr><td>Email:</td><td><a href="mailto:'.$email.'" target="_blank">'.$email.'</a></td></tr><tr><td>Địa chỉ:</td><td>'.$diachi.'</td></tr><tr><td>Quận huyện:</td><td>'.$ward.','.$district.'</td></tr><tr><td>Tỉnh thành:</td><td>'.$province.'</td></tr></tbody></table></td></tr><tr><td colspan="2" style="border-left:1px solid #d7d7d7;border-right:1px solid #d7d7d7;border-bottom:1px solid #d7d7d7;padding:5px 10px"><p><strong>Hình thức thanh toán: </strong>'.$hinhthuc.'</p><p><strong>Hình thức vận chuyển: </strong>'.$tenphi.'<br></p></td></tr></tbody></table></div><div><div style="font-size:18px;padding-top:10px"><strong>Thông tin đơn hàng</strong></div><table><tbody><tr><td>Mã đơn hàng: <strong>#'.$id.'</strong></td><td style="padding-left:40px">Ngày tạo: '.$created_at.'</td></tr></tbody></table><table style="width:100%;border-collapse:collapse"><thead><tr style="border:1px solid #d7d7d7;background-color:#f8f8f8"><th style="padding:5px 10px;text-align:left"><strong>Sản phẩm</strong></th><th style="text-align:center;padding:5px 10px"><strong>Mã SKU</strong></th><th style="text-align:center;padding:5px 10px"><strong>Số lượng</strong></th><th style="padding:5px 10px;text-align:right"><strong>Tổng</strong></th></tr></thead><tbody>'.$listsanpham.'<tr style="border:1px solid #d7d7d7"><td colspan="2">&nbsp;</td><td colspan="2"><table style="width:100%"><tbody><tr><td><strong>Giảm giá</strong></td><td style="text-align:right">'.$tonggiamgia.' VNĐ</td></tr><tr><td><strong>Giá trừ khuyến mãi:</strong></td><td style="text-align:right">'.$giatrukhuyenmai.' VNĐ</td></tr><tr><td><strong>Phí vận chuyển:</strong></td><td style="text-align:right">'.$phivanchuyen.' VNĐ</td></tr><tr><td><strong>Thành tiền</strong></td><td style="text-align:right">'.$tongtien.' VNĐ</td></tr></tbody></table></td></tr></tbody></table></div><hr><p style="text-align:right"><a href="'.$baseurl.'xem-lai-hoa-don?token='.$token.'">Chi tiết đơn hàng</a> <br/>Nếu Anh/chị có bất kỳ câu hỏi nào, xin liên hệ với chúng tôi tại <span style="color:#17a3dd"><a href="mailto:'.$email_gmail.'" target="_blank">'.$email_gmail.'</a></span></p><p style="text-align:right"><i>Trân trọng,</i></p><p style="text-align:right"><strong>Ban quản trị '.$tenwebsite.'</strong></p><div class="yj6qo"></div><div class="adL"></div></div>';
        return $Content;
    }
    
}

