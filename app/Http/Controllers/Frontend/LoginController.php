<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Auth;
use Cart;
use Hash;
use App\Model\Member;
use App\Model\District;
use App\Model\Ward;
use App\Model\Province;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;
use App\Http\Requests\Members\ChangePasswordRequest;
use App\Http\Requests\Members\UpdateAddressRequest;
use App\Http\Requests\Logins\StoreRequest;
use App\Http\Requests\Logins\CheckLoginRequest;
use App\Http\Requests\Logins\ChangeEmailRequest;
use App\Model\Cauhinh;
use App\Model\Dmsp;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class LoginController extends Controller
{
	private $guard;
    private $member;
    private $district;//tinhs
    private $ward;
    private $cauhinh;
    private $province;
    private $dmsp;
    
    public function __construct(
                                Member      $member,
                                District    $district,
                                Ward        $ward,
                                Cauhinh     $cauhinh,
                                Dmsp        $dmsp,
                                Province    $province
    ){
 
        $this->guard        = Auth::guard('member');
        $this->member       = $member;
        $this->district     = $district;
        $this->ward         = $ward;
        $this->province     = $province;
        $this->cauhinh      = $cauhinh;
        $this->dmsp         = $dmsp;
    }

    public function getLogin()
    {
        return view('frontend.pages.login');
    }

    public function postLogin(CheckLoginRequest $request)
    {
        if(isset($_POST['btnDangnhapDonhang']))
        {
            $credentials = $request->only('email', 'password');
            if($this->guard->attempt($credentials))
            {
                return redirect()->back();
            }
            return redirect()->back()->with('alerterrv2','Tài khoản hoặc mật khẩu không đúng');
        }
        if(isset($_POST['btnDangnhapMain']))
        {
            $credentials = $request->only('email', 'password');
            if($this->guard->attempt($credentials))
            {
                if(!empty(Cart::restore(Auth::guard('member')->user()->id)))
                {
                    Cart::restore(Auth::guard('member')->user->id);
                    return redirect()->route('trangchu.index');
                }
                else
                {
                    return redirect()->route('trangchu.index');
                }
                
            }
            return redirect()->route('trangchu.getLogin')->with('alerterrv2','Tài khoản hoặc mật khẩu không đúng');
        }
    }
    public function getLogout()
    {
        if(Cart::count()>0 && Auth::guard('member')->check())
        {
            Cart::store(Auth::guard('member')->user()->id);
            Cart::destroy();
            Auth::guard('member')->logout();
            return redirect()->route('trangchu.index');
        }
        else
        {
            Auth::guard('member')->logout();
            return redirect()->route('trangchu.index'); 
        }
    }
     public function getLogout2()
    {
        if(Cart::count()>0 && Auth::guard('member')->check())
        {
            Cart::store(Auth::guard('member')->user()->id);
            Cart::destroy();
            Auth::guard('member')->logout();
            return redirect()->back();
        }
        else
        {
            Auth::guard('member')->logout();
            return redirect()->back();
        }
    }
    public function getRegister()
    {
        return view('frontend.pages.register');
    }
    public function postRegister(StoreRequest $request)
    {
        $data = array(
                        'username'  => $request->username,
                        'password'  => bcrypt($request->password),
                        'fullname'  => $request->fullname,
                        'phone'     => $request->phone,
                        'email'     => $request->email,
                    ); 
                
        $this->member->store($data);
        return redirect()->route('trangchu.getLogin')->with('alertsuc','Đăng ký tài khoản thành công');
    }

    public function getInformation()
    {
        return view('frontend.module.member.member-information');
    }

    public function postPassword(ChangePasswordRequest $request)
    {
        if(Auth::guard('member')->check())
        {
            if(isset($_POST['btnLuuMatKhau']))
            {
                $id       = Auth::guard('member')->user()->id;
                if($request->repeatpasswordnew == $request->passwordnew)
                {
                    $data = array('password' => bcrypt($request->passwordnew));
                    $this->member->updataMember($data,$id);
                    return redirect()->back()->with('alertsuc','Đổi mật khẩu thành công');
                }
                else
                {
                    return redirect()->back()->with('alerterr','Mật khẩu nhập lại phải trùng với mật khẩu mới');
                } 
            }
        }
        return redirect()->route('trangchu.index');
    }
   

    public function getAddress()
    {
        return view('frontend.module.member.member-update');
    }
    public function postAddress(UpdateAddressRequest $request)
    {
        if(isset($_POST['btnLuuThongTin']))
        {
            $getId       = Auth::guard('member')->user()->id;
            $getDiachi   = Auth::guard('member')->user()->diachi;
            $data = array(
                            'username'  => $request->username,
                            'fullname'  => $request->fullname,
                            'phone'     => $request->phone,
                            'diachi'    => $request->diachi,
                            'ward'      => $request->ward,
                            'district'  => $request->district,
                            'province'  => $request->province,
                        );
            $this->member->updataMember($data,$getId);
            return redirect()->back()->with('alertsuc','Sửa thành công');
        }
    }
    public function sendmail($email_gmail,$password_gmail,$tenwebsite,$email_receive,$subject,$body)
    {
        $mail = new PHPMailer(true);                            // Passing `true` enables exceptions
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        ); 
        $mail->SMTPDebug = 0;                                   // Enable verbose debug output
        $mail->isSMTP();                                        // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';                                             // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                                 // Enable SMTP authentication
        $mail->Username = $email_gmail;             // SMTP username
        $mail->Password = $password_gmail;              // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom( $email_gmail, $tenwebsite);
        $mail->addAddress($email_receive, 'Chào bạn');    // Add a recipient, Name is optional
        $mail->addReplyTo($email_gmail, 'Mailer');
        // $mail->addCC('his-her-email@gmail.com');
        // $mail->addBCC('his-her-email@gmail.com');

        //Attachments (optional)
        // $mail->addAttachment('/var/tmp/file.tar.gz');            // Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');   // Optional name

        //Content
        $mail->isHTML(true);                                                                    // Set email format to HTML
        $mail->Subject = $subject;
        // message
        $mail->Body    = $body;
        // $mail->send();
        return $mail;
    }
    public function getChangeEmail()
    {
        if(isset($_POST['btnSendMail']))
        {
            if(Auth::guard('member')->check())
            {
                $random = mt_rand(100000,999999);
                $emailthongbao   = Auth::guard('member')->user()->email;
                $email_gmail     = $this->cauhinh->getData('email_gmail')->chitiet;
                $password_gmail  = $this->cauhinh->getData('password_gmail')->chitiet;
                $tenwebsite      = $this->cauhinh->getData('tenwebsite')->chitiet;
                $baseurl         = $this->cauhinh->getData('linkwebsite')->chitiet;
                $subject         = "Thay Đổi Email";
                $random          = mt_rand(100000,999999);
                $body            = "Mã xác nhận: ".$random."";
                $data = array(
                            'remember_token' => $random,
                        );
                $this->member->updataMemberEmailToken($data,$emailthongbao);
                try {
                    $this->sendmail($email_gmail,$password_gmail,$tenwebsite,$emailthongbao,$subject,$body)->send();
                } catch (Exception $e) {
                    return redirect()->back()->with('alerterr','Gửi email không thành công. Xin vui lòng gửi lại');
                }
                return view('frontend.module.member.member-changeemail');
                // return redirect()->route('trangchu.getChangeEmail');
            }
            return redirect()->route('trangchu.getLogouts');
        }
    }
    public function postChangeEmail(Request $request)
    {
        if(isset($_POST['btnSaveChange']))
        {
            $member     = Auth::guard('member')->user();
            $maxacminh  = $request->maxacminh;
            if($maxacminh == $member->remember_token)
            {
                $data = array(
                            'email'  => $request->email,
                        );
                $this->member->updataMember($data,$member->id);
                return redirect()->route('trangchu.getLogout');
            }
            return redirect()->back()->with('alerterrv2','Mã xác minh không dúng');
        }
    }

}