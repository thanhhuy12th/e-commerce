<?php

namespace App\Http\Controllers\Frontend;

// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Controller;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Model\Member;
use App\Model\Cauhinh;
use App\Http\Requests\Members\ResetPasswordRequest;
use App\Http\Requests\Members\ChangePasswordRequest;
use App\Http\Requests\Members\SendMailPasswordRequest;

class ResetPasswordController extends Controller
{
	private $member;
    private $cauhinh;


    public function __construct(Member $member, Cauhinh $cauhinh)
    {
        $this->member           = $member;
        $this->cauhinh          = $cauhinh;

    }
	public function getTimMatKhau()
    {
        return view('frontend.module.member.member-resetpassword');
    }
    public function sendEmail (SendMailPasswordRequest $request) 
    {
  	// is method a POST ?
        $email_gmail           = $this->cauhinh->getData('email_gmail')->chitiet;
        $password_gmail        = $this->cauhinh->getData('password_gmail')->chitiet;
        $tenwebsite            = $this->cauhinh->getData('tenwebsite')->chitiet;
	  	if (isset($_POST['sendMail']))
        {
        	$email = $this->member->getMemberEmail($_POST['checkemail']);
        	if(isset($email))
        	{
        		$token = "";
        		if(!empty($email->remember_token))
        		{
        			$token = $email->remember_token;
        		}
        		else
        		{
        			$token = substr(MD5(rand(0,10000)),0,16);

        			$data = array(
        						'remember_token' => $token,
        					);
        			$this->member->updataMemberEmailToken($data,$email->email);
        		}
        		$mail = new PHPMailer(true);                            // Passing `true` enables exceptions
				$mail->SMTPOptions = array(
				    'ssl' => array(
				        'verify_peer' => false,
				        'verify_peer_name' => false,
				        'allow_self_signed' => true
				    )
				);
				try {
					// Server settings

		    		$mail->SMTPDebug = 0;                                	// Enable verbose debug output
					$mail->isSMTP();                                     	// Set mailer to use SMTP
					$mail->Host = 'smtp.gmail.com';												// Specify main and backup SMTP servers
					$mail->SMTPAuth = true;                              	// Enable SMTP authentication
					$mail->Username = $email_gmail;             // SMTP username
					$mail->Password = $password_gmail;              // SMTP password
					$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
					$mail->Port = 587;                                    // TCP port to connect to

					//Recipients
					$mail->setFrom( $email_gmail, $tenwebsite);
					$mail->addAddress($_POST['checkemail'], 'Chào bạn');	// Add a recipient, Name is optional
					$mail->addReplyTo($email_gmail, 'Mailer');
					// $mail->addCC('his-her-email@gmail.com');
					// $mail->addBCC('his-her-email@gmail.com');

					//Attachments (optional)
					// $mail->addAttachment('/var/tmp/file.tar.gz');			// Add attachments
					// $mail->addAttachment('/tmp/image.jpg', 'new.jpg');	// Optional name

					//Content
					$mail->isHTML(true); 																	// Set email format to HTML
					$mail->Subject = 'ResetPassword';
					$mail->Body    = "<a href='http://127.0.0.1:8000/mat-khau-moi?email=".$_POST['checkemail']."&remember_token=".$token." '>Click vào đây nếu bạn muốn thay đổi mật khẩu</a>'";						// message

					$mail->send();
					return redirect()->back()->with('alertsuc','Vui lòng kiểm tra email của bạn');
				} catch (Exception $e) {
					return redirect()->back()->with('alerterr','Gửi không thành công');
				}
        	}
        	else
        	{
        		return redirect()->back()->with('alerterr','Không tồn tại email '.$_POST['checkemail']);
        	}
				
		}
  	}
  	public function getNewPassword()
    {
		return view('frontend.module.member.member-newpassword');
	}


	public function postNewPassword(Request $request,ResetPasswordRequest $req)
    {
        if (isset($_POST['updatePass']))
        {
        	$email = Request::get('email');
        	$token = Request::get('remember_token');
        	$check = $this->member->checkUpdataMemberEmailPass($email,$token);
        	if(isset($check) && $token != Null)
        	{
                if($req->renewpassword == $req->newpassword)
                {
    	        	$data = array(
    	        						'password' => bcrypt($req->newpassword),
    	        						'remember_token' => Null,
    	        					);
    	        	$this->member->updataMemberEmailPass($data,$email,$token);
    	        	return redirect()->route('trangchu.getLogin')->with('alertsuc',"Thay đổi mật khẩu thành công");
                }
                else
                {
                    return redirect()->back()->with('alerterr',"Mật khẩu nhập lại không chính xác");
                }

        	}
        	else
        	{
        		return redirect()->back()->with('alerterr',"Không thể thay đổi mật khẩu. Xin vui lòng gửi lại email");
        	}
        }
        else
        {
        	return redirect()->back()->with('alerterr','Lỗi');
        }
	}

}
