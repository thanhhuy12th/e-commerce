<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Donhangs\StoreRequest;
use App\Model\Donhang;
use App\Model\OrderDetail;
use Illuminate\Support\Facades\Auth;
use Input;
use Carbon\Carbon;
use App\Model\Member;
use App\Model\Cauhinh;
use App\Model\Sanpham;


class DonhangController extends Controller
{
    private $member;
    private $donhang;
    private $orderDetail;
    private $cauhinh;
    private $sanpham;
    
    public function __construct(
                                Member      $member,
                                Donhang     $donhang,
                                OrderDetail $orderDetail,
                                Cauhinh     $cauhinh,
                                Sanpham     $sanpham
                                )
    {
        $this->member           = $member;
        $this->donhang          = $donhang;
        $this->orderDetail      = $orderDetail;
        $this->cauhinh          = $cauhinh;
        $this->sanpham          = $sanpham;
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $data['donhang']   = $this->donhang->index();

        return view('admin.module.donhangs.donhang-list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['sanpham'] = $this->orderDetail->getOrderDetail($id);
        $data['donhang'] = $this->donhang->getDonhang($id);
        return view('admin.module.donhangs.donhang-edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,StoreRequest $req)
    {
        if (isset($_POST['btnUndoXacnhan']))
        {
            $data = array('status_paid' => 0);
            $this->donhang->updateDonhang($data,$id);
            return redirect()->back();
        }
        if (isset($_POST['btnXacnhan']))
        {
            $data = array('status_paid' => 1);
            $this->donhang->updateDonhang($data,$id);
            return redirect()->back();
        }
        if (isset($_POST['btnUndoGiaohang']))
        {
            $data = array('status_move' => 1);
            $this->donhang->updateDonhang($data,$id);
            return redirect()->back();
        }
        if (isset($_POST['btnGiaohang']))
        {
            $data = array('status_move' => 0);
            $this->donhang->updateDonhang($data,$id);
            return redirect()->back();
        }
        if($req->ajax())
        {            
            $id = $req->id;
            $data = DB::table('skl_order')->find($id);
        }
        die(json_encode($html));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        if (isset($_POST['btnXoaList']))
        {
            $checked = $req->input('checked',[]);
            foreach ($checked as $id) 
            {
                $this->donhang->destroyDonhang($id);
            }
            return redirect()->route('donhang.index')->with('alertsuc','Xóa thành công');
        }
    }
    public function getInhoadon()
    {
        if(Request::get('token'))
        {
            $token = Request::get('token');
            $datadonhang = $this->donhang->getDonhangToken($token);
            if($datadonhang->token == $token)
            {
                $data['sanphamimg']     = $this->sanpham->getSanphamHoadon();
                $data['tenwebsite']     = $this->cauhinh->getData('tenwebsite')->chitiet;
                $data['dienthoai']      = $this->cauhinh->getData('dienthoai')->chitiet;
                $data['diachi']         = $this->cauhinh->getData('title')->chitiet;
                $data['logo']           = $this->cauhinh->getData('photos')->chitiet;
                $data['linkwebsite']    = $this->cauhinh->getData('linkwebsite')->chitiet;
                $data['member']         = $this->member->getMemberEmail($datadonhang->email);
                $data['sanpham']        = $this->orderDetail->getOrderDetail($datadonhang->id);
                $data['donhang']        = $this->donhang->getDonhang($datadonhang->id);
                return view('admin.module.donhangs.inhoadon',$data);
            }
        }
    }
    public function updatedata()
    {
        $data = array(  'hoten'     => Request::get('hoten'),
                        'email'     => Request::get('email'),
                        'sodt'      => Request::get('sodt'),
                        'diachi'    => Request::get('diachi'),
                        'province'  => Request::get('province'),
                        'district'  => Request::get('district'),
                        'ward'      => Request::get('ward'),
                        'ghichu'    => Request::get('ghichu')
                        );
        $datadonhang = $this->donhang->updateDonhang($data, Request::get('id'));
        return Request::all();
        // DB::table('skl_order')->where('id',$req->id)->update($data);
    }
}
