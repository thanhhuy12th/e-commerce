<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Coupons\StoreRequest;
use App\Model\Vanchuyen;
use Illuminate\Support\Facades\Auth;
use Input;
use Illuminate\Database\QueryException;

class VanchuyenController extends Controller
{
    private $vanchuyen;

    public function __construct(
                                Vanchuyen $vanchuyen
                                )
    {
        $this->vanchuyen          = $vanchuyen;
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['vanchuyen']   = $this->vanchuyen->index();
        

        return view('admin.module.vanchuyens.vanchuyen-list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.module.vanchuyens.vanchuyen-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {   


        $data = array(
                        'name' => $req->name,
                        'gia'  => $req->gia,
                    );
        try {
             $this->vanchuyen->storeVanchuyen($data);
        }catch (QueryException $exception) {
            return back()->with('alerterr','Sai định dạng');
        }
       
        return redirect()->back()->with('alertsuc','Thêm thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['vanchuyen'] = $this->vanchuyen->getVanchuyen($id);
        return view('admin.module.vanchuyens.vanchuyen-edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        if (isset($_POST['addTintuc']))
        {
            $data = array(
                        'name' => $req->name,
                        'gia'  => $req->gia,
                    );
            try {
                $this->vanchuyen->updateVanChuyen($data,$id);
            }catch (QueryException $exception) {
                return back()->with('alerterr','Sai định dạng');
            }
            return redirect()->back()->with('alertsuc','Sửa thành công');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        if (isset($_POST['btnXoaList']))
        {
            $checked = $req->input('checked',[]);
            foreach ($checked as $id) 
            {
                $this->vanchuyen->destroyVanchuyen($id);
            }
            return redirect()->route('vanchuyen.index')->with('alertsuc','Xóa thành công');
        }
    }
}
