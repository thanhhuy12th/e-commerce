<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dmtts\StoreRequest;
use App\Model\Dmtt;
use Illuminate\Support\Facades\Auth;
use Input;
use Illuminate\Database\QueryException;

class DmttController extends Controller
{
    private $dmtt;

    public function __construct(
                                Dmtt $dmtt
                                )
    {
        $this->dmtt          = $dmtt;
        
    }

    public function getParent()
    {
        $data = [];
        
        $optParent = $this->dmtt->listCateByIdParent(0); 
        foreach($optParent  as $oP)
        {
            $dataOptPar = array('id'     => $oP->id, 
                                'name'   => $oP->name);
            array_push($data, $dataOptPar);

            $optChild = $this->dmtt->listCateByIdParent($oP->id);
            foreach ($optChild as $oC) 
            {
                // Tạo mảng 2 chiều danh mục con
                $dataOptChi= [
                                "id"=>$oC->id,
                                "name"=>"-- ".$oC->name
                             ];
                array_push($data, $dataOptChi);
            }
            
        }
        return $data;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['dmtt']   = $this->dmtt->index();
        $data['optArr'] = $this->getParent();

        return view('admin.module.dmtts.dmtt-list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data['optArr'] = $this->getParent();

        return view('admin.module.dmtts.dmtt-create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $req)
    {

        $data = array(
                        'name'      => $req->name,
                        'alias'     => (empty($req->alias)) ? chuyen($req->name) : $req->alias,
                        'vitri'     => $req->vitri,
                        'id_parent' => $req->id_parent,
                    );
        try {
            $this->dmtt->store($data);
        }catch (QueryException $exception) {
            return back()->with('alerterr','Sai định dạng');
        }
        return redirect()->back()->with('alertsuc','Thêm thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['optArr'] = $this->getParent();
        $data['dmtt']   = $this->dmtt->getDmtt($id);
        return view('admin.module.dmtts.dmtt-edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        if (isset($_POST['addTintuc']))
        {
            $data = array(
                        'name' => $req->name,
                        'alias' => (empty($req->alias)) ? chuyen($req->name) : $req->alias,
                        'vitri' => $req->vitri,
                        'id_parent' => $req->id_parent,
                    );
            try {
                $this->dmtt->updateDmtt($data,$id);
            }catch (QueryException $exception) {
                return back()->with('alerterr','Sai định dạng');
            }
            return redirect()->back()->with('alertsuc','Sửa thành công');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        if (isset($_POST['btnXoaList']))
        {
            $checked = $req->input('checked',[]);
            foreach ($checked as $id) 
            {
                $this->dmtt->destroyDmtt($id);
            }
            return redirect()->route('dmtt.index')->with('alertsuc','Xóa thành công');
        }
    }
}
