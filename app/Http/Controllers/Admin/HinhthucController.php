<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Hinhthucs\StoreRequest;
use App\Model\Hinhthuc;
use Illuminate\Support\Facades\Auth;
use Input;
use Illuminate\Database\QueryException;

class HinhthucController extends Controller
{
    private $hinhthuc;

    public function __construct(
                                Hinhthuc $hinhthuc
                                )
    {
        $this->hinhthuc          = $hinhthuc;
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['hinhthuc']   = $this->hinhthuc->index();
        

        return view('admin.module.hinhthucs.hinhthuc-list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.module.hinhthucs.hinhthuc-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $req)
    {   
        $data = array(
                                'name' => $req->name,
                                'vitri' => $req->vitri
                            );   
        try {
            
            $this->hinhthuc->storeHinhthuc($data);
        }catch (QueryException $exception) {
            return back()->with('alerterr','Sai định dạng');
        }
        return redirect()->back()->with('alertsuc','Thêm thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['hinhthuc'] = $this->hinhthuc->getHinhthuc($id);
        return view('admin.module.hinhthucs.hinhthuc-edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        if (isset($_POST['addTintuc']))
        {
            try {
                $data = array(
                        'name' => $req->name,
                        'vitri' => $req->vitri
                    );
                $this->hinhthuc->updateHinhthuc($data,$id);
            } catch (QueryException $e) {
                return back()->with('alerterr','Sai định dạng');
            }
            
            return redirect()->back()->with('alertsuc','Sửa thành công');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        if (isset($_POST['btnXoaList']))
        {
            $checked = $req->input('checked',[]);
            foreach ($checked as $id) 
            {
                $this->hinhthuc->destroyHinhthuc($id);
            }
            return redirect()->route('hinhthuc.index')->with('alertsuc','Xóa thành công');
        }
    }
}
