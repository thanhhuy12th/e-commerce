<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use File;

class OrderDetail extends Model
{
    public $timestamps = true;
    protected $table = 'skl_orderdetail';
    protected $guarded = [];

    public function dbTable()
    {
        return DB::table('skl_orderdetail');
    }
    public function storeOrderDetail($data)
    {
    	return $this->dbTable()->insert($data);
    }
    public function getOrderDetail($id)
    {
        return $this->dbTable()
                    ->where('id_order',$id)->get()->toArray();
    }
}
