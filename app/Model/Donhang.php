<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use File;

class Donhang extends Model
{
    public $timestamps = true;
    protected $table = 'skl_order';
    protected $guarded = [];

    public function dbTable()
    {
        return DB::table('skl_order');
    }

    public function index()
    {
    	return $this->dbTable()
                    ->orderBy('id')
                    ->paginate(10);
    }
    public function destroyDonhang($id)
    {
        return $this->dbTable()
                    ->whereId($id)
                    ->delete();
    }
    public function getDonhang($id)
    {
        return $this->dbTable()
                    ->find($id);
    }
    public function updateDonhang($data,$id)
    {
        return $this->dbTable()
                    ->where('id',$id)
                    ->update($data);
    }
    public function getDonhangToken($token)
    {
        return $this->dbTable()
                    ->where('token',$token)
                    ->first();
    }
}
