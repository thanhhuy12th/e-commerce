<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Member extends Model
{
    public $timestamps = true;
    protected $table = 'skl_member';
    protected $guarded = [];

    public function dbTable()
    {
        return DB::table('skl_member');
    }
    public function index()
    {
    	return $this->dbTable()
                    ->get()
                    ->toArray();
    }
    public function store($data)
    {
    	return $this->dbTable()
    				->insert($data);
    }
    public function getMember($id)
    {
        return $this->dbTable()
                    ->find($id);
    }
    public function updataMember($data,$id)
    {
        return $this->dbTable()
                    ->where('id',$id)
                    ->update($data);
    }
    public function destroyMember($id)
    {
        return $this->dbTable()
                    ->whereId($id)
                    ->delete();
    }
    public function getMemberEmail($email)
    {
        return $this->dbTable()
                    ->where('email',$email)
                    ->first();
    }
    public function updataMemberEmailToken($data,$email)
    {
        return $this->dbTable()
                    ->where('email',$email)
                    ->update($data);
    }
    public function updataMemberEmailPass($data,$email,$remember_token)
    {
        return $this->dbTable()
                    ->where([
                        ['email',$email],
                        ['remember_token',$remember_token],
                    ])
                    ->update($data);
    }
    public function checkUpdataMemberEmailPass($email,$remember_token)
    {
        return $this->dbTable()
                    ->where([
                        ['email',$email],
                        ['remember_token',$remember_token],
                    ])
                    ->first();
    }
    public function checkTokenEmail()
    {
        return $this->dbTable()
                    ->get()
                    ->toArray();
    }
    public function laydulieu()
    {
        return $this->dbTable()
                    ->orderBy('id','ASC')
                    ->get()
                    ->toArray();
    }
    
}
