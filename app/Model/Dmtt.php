<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use File;

class Dmtt extends Model
{
    public $timestamps = true;
    protected $table = 'skl_dmtt';
    protected $guarded = [];

    public function dbTable()
    {
        return DB::table('skl_dmtt');
    }

    public function index()
    {
    	return $this->dbTable()
                    ->orderBy('id')
                    ->paginate(10);
    }
    public function store($data)
    {
    	return $this->dbTable()
                    ->insert($data);
    }
    public function updateDmtt($data,$id)
    {
        return $this->dbTable()
                    ->where('id',$id)
                    ->update($data);
    }
    public function getDmtt($id)
    {
        return $this->dbTable()
                    ->find($id);
    }
    public function destroyDmtt($id)
    {
        return $this->dbTable()
                    ->whereId($id)
                    ->delete();
    }

    public function listCateByIdParent($id)
    {
        return $this->dbTable()
                    ->where('id_parent',$id)
                    ->get()
                    ->toArray();
    }
    
    public function getParent()
    {
        return $this->dbTable()
                    ->select('id','name','id_parent')
                    ->get()
                    ->toArray();
    }

}
