<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Useradmin extends Model
{
    public $timestamps = true;
    protected $table = 'skl_admin';
    // public $remember_token=false;

    public function dbTable()
    {
        return DB::table('skl_admin');
    }
    public function index()
    {
    	return $this->dbTable()
                    ->paginate(10);
    }
    public function store($data)
    {
    	return $this->dbTable()
    				->insert($data);
    }
    public function getUser($id)
    {
        return $this->dbTable()
                    ->find($id);
    }
    public function updateUser($data,$id)
    {
        return $this->dbTable()
                    ->where('id',$id)
                    ->update($data);
    }
     public function destroyUser($id)
    {
        return $this->dbTable()
                    ->whereId($id)
                    ->delete();
    }
}
