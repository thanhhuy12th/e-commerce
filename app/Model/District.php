<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use File;

class District extends Model
{
    //Huyen
    public $timestamps = true;
    protected $table = 'skl_district';
    protected $guarded = [];

    public function dbTable()
    {
        return DB::table('skl_district');
    }

    public function getData()
    {
        return $this->dbTable()
                    ->orderBy('name','ASC')
                    ->get()
                    ->toArray();
    }
    public function getWhereId($id)
    {
        return $this->dbTable()
                    ->where('provinceid',$id)
                    ->orderBy('name','ASC')
                    ->get()
                    ->toArray();
    }
    public function getDistrict($id)
    {
        return $this->dbTable()
                    ->where('districtid',$id)
                    ->first();
    }

    public function getDistrictData()
    {
        return $this->dbTable()
                    ->get()
                    ->toArray();
    }


}
