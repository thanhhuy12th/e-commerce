<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use File;

class Order extends Model
{
    public $timestamps = true;
    protected $table = 'skl_order';
    protected $guarded = [];

    public function dbTable()
    {
        return DB::table('skl_order');
    }
    public function storeOrder($data)
    {
    	return $this->dbTable()->insert($data);
    }
    public function getData()
    {
        return $this->dbTable()
                    ->get()
                    ->toArray();
    }
}
