<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
use File;

class Coupon extends Model
{
    public $timestamps = true;
    protected $table = 'skl_giamgia';
    protected $guarded = [];

    public function dbTable()
    {
        return DB::table('skl_giamgia');
    }

    public function index()
    {
    	return $this->dbTable()
                    ->orderBy('id')
                    ->paginate(10);
    }
    public function date()
    {
    	return $this->dbTable()
                    ->select('begin','end')
                    ->get()
                    ->toArray();
    }
    public function storeCoupon($data)
    {
    	return $this->dbTable()->insert($data);
    	// return Sanpham::create($data);
    }
    public function updateCoupon($data,$id)
    {
        return $this->dbTable()
                    ->where('id',$id)
                    ->update($data);
    }
    public function getCoupon($id)
    {
        return $this->dbTable()
                    ->find($id);
    }
    public function destroyCoupon($id)
    {
        return $this->dbTable()
                    ->whereId($id)
                    ->delete();
    }
    public function selectData()
    {
        return $this->dbTable()
                    ->get()
                    ->toArray();
    }
}
