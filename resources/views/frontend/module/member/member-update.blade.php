@extends('frontend.welcome')

@section('content')
<!--product-home-->    
<div id="product-home">
    
</div>


<!--tintuc-home-->
<div id="tintuc-home">
    
</div>


<style type="text/css">
    .mota{
        style="font-weight: 400;
        text-transform: uppercase;
    }

</style>
<!--mota-home-->
<a href="{{route('trangchu.getInformation')}}"> Back</a>
 <form action="{{route('trangchu.postAddress')}}" method="post">
    @csrf
    @include('admin.blocks.alert') 
    <div class="form-group">
      <label style="width: 120px">Địa chỉ hiện tại:</label>
      <input style="width: 450px" type="text" name="address" value="{{$fullname->diachi}}">
    </div>
    <div class="form-group">
      <label style="width: 120px">Tỉnh/ Thành phố: </label>
      <select style="width: 250px" id="province" name="province">
        <option value="">Vui lòng chọn Tỉnh/ Thành phố</option>
      </select>
    </div>
    <div class="form-group">
      <label style="width: 120px">Quận/ Huyện: </label>
          <select style="width: 250px" id="district" name="district">
             <option value="">Vui lòng chọn Quận/ Huyện</option>
          </select>
    </div>

    <div class="form-group">
      <label style="width: 120px">Phường/ Xã: </label>
          <select style="width: 250px" id="ward" name="ward">
             <option value="">Vui lòng chọn Phường/ Xã</option>
          </select>
    </div>
    <button style="background: blue;color: white;margin-left: 150px" class="btn" type="submit">Luu</button>
  </form>




<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
$(document).ready(function(){
      var url = "./api/province";
      $.getJSON(url,function(result){
        $.each(result, function(i, field){
          var id   = field.provinceid;
          var name = field.name;
          var type = field.type;
          $("#province").append('<option value="'+id+'">'+type+" "+name+'</option>');
        });
      });

    $("#province").change(function(){
        $("#district").html('<option value="-1">Vui lòng chọn Quận/ Huyện</option>');
            var idprovince = $("#province").val();
            var url = "./api/district/"+idprovince;
            $.getJSON(url, function(result){
                console.log(result);
                $.each(result, function(i, field){
                    var id   = field.districtid;
                    var name = field.name;
                    var type = field.type;
                    $("#district").append('<option value="'+id+'">'+type+" "+name+'</option>');
            });
        });
    });

    $("#district").change(function(){
        $("#ward").html('<option value="-1">Vui lòng chọn Phường/ Xã</option>');
            var iddistrict = $("#district").val();
            var url = "./api/ward/"+iddistrict;
            $.getJSON(url, function(result){
                console.log(result);
                $.each(result, function(i, field){
                    var id   = field.wardid;
                    var name = field.name;
                    var type = field.type;
                    $("#ward").append('<option value="'+id+'">'+type+" "+name+'</option>');
            });
        });
    });
});
</script>

@endsection