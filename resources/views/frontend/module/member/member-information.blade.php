@extends('frontend.welcome')
@section('content')
<style type="text/css">
    .chuatt{
        background: -webkit-linear-gradient(178deg, #FDE06B  0%, #FDE06B  100%);
        background: linear-gradient(178deg, #FDE06B  0%, #FDE06B  100%);
        border-radius: 8px;
        padding: 6px 4px 6px 4px;
    }
</style>
<div class="jumbotron" >
    <!-- Nav pills -->
    @include('admin.blocks.alert') 
    <ul class="nav nav-pills" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="pill" href="#userinfo1" style="background-color: #35cc7b; color: #ffffff; margin-right: 5px">Thông tin cá nhân</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#userinfo2" style="background-color: #35cc7b; color: #ffffff;margin-right: 5px">Đơn hàng</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#userinfo3" style="background-color: #35cc7b; color: #ffffff;margin-right: 5px">Đổi mật khẩu</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#userinfo4" style="background-color: #35cc7b; color: #ffffff">Đổi email đăng nhập</a>
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div id="userinfo1" class="container tab-pane active"><br>
            
            <form action="{{route('trangchu.postAddress')}}" method="post">
                @csrf 
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <h4 style="color: #35cc7b">Sửa thông tin cá nhân</h4>
                        <p><label>Họ và tên: </label><input value="{{nameLogin()->fullname}}" type="text" name="fullname" class="form-control" placeholder="Họ và tên"></p>
                        <p><label>Email: </label><input value="{{nameLogin()->email}}" type="text" name="email" class="form-control" placeholder="123456789@gmail.com"></p>
                        <p><label>Số điện thoại: </label><input value="{{nameLogin()->phone}}" type="text" name="phone" class="form-control" placeholder="0859529539"></p>
                        <p><label>Tên hiển thị: </label><input value="{{nameLogin()->username}}" type="text" name="username" class="form-control" placeholder="123 K HCM"></p>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <h4 style="color: #35cc7b">Địa chỉ</h4>
                        <p><label>Địa chỉ giao hàng </label><input value="{{nameLogin()->diachi}}" type="text" name="diachi" class="form-control" placeholder="123 K"></p>
                        <p><label>Tỉnh / Thành phố </label>
                            <input type="hidden" id="idprovince1" value="{{nameLogin()->province}}">
                            <select class="form-control custom-select-value" name="province" id="province">
                            </select></p>
                        <p><label>Quận / Huyện: </label>
                            <input type="hidden" id="iddistrict1" value="{{nameLogin()->district}}">
                            <select class="form-control custom-select-value" name="district" id="district">
                            </select></p>
                        <p><label>Phường / Xã: </label>
                            <input type="hidden" id="idward1" value="{{nameLogin()->ward}}">
                            <select class="form-control custom-select-value" name="ward" id="ward">
                            </select></p>                     
                    </div> 
                </div>
                <button name="btnLuuThongTin" type="submit" class="btn btn-succes pull-right" style="background-color: #35cc7b; color: #ffffff;">Lưu thông tin</button>
                <label style="font-size:20px">Số lần mua hàng:</label>
                <a href="#" style="font-size:20px;"> {{nameLogin()->solanmua}}</a><br>
                <input type="hidden" id="tongtienmua" value="{{nameLogin()->tongtienmua}}">
                <input type="hidden" id="vipmoney" value="{{getCauhinh('vipmoney')}}">
                <div id="khachvip"></div>
            </form>
        </div>
        <div id="userinfo2" class="container tab-pane fade">
            <br>
            <div class="table-responsive">
                <form method="POST" name="frmForm" id="frmForm" enctype="multipart/form-data"> 
                    <table class="table table1 table-striped jambo_table bulk_action" style="margin-top: 10px">
                        <thead>
                            <tr class="headings">
                                <th class="column-title">Đơn hàng </th>
                                <th class="column-title">Tổng giá trị </th>
                                <th class="column-title">Cách thức thanh toán </th>
                                <th class="column-title">Trạng thái</th>
                                <th class="column-title">Ngày đặt</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $orderLists = select_order_email();
                            ?>
                            @foreach($orderLists as $ol)
                            <tr class="even pointer">
                                <td class=" ">{{$ol->id}}</td>
                                <td class=" ">{{convert_money($ol->tongtien)}} VNĐ</td>
                                <td class=" ">{{select_hinhthuc($ol->hinhthuc)->name}}</td>
                                <td class=" ">
                                    @if($ol->status_paid == 0)
                                    <span class="chuatt">Chưa thanh toán</span>
                                    @else
                                    <span style="font-weight: bold;">Đã thanh toán</span>
                                    @endif
                                </td>
                                <td class=" ">
                                    <?php
                                        $date = date_create($ol->created_at);
                                        echo date_format($date, 'd-m-Y H:i');
                                    ?>
                                </td>
                            </tr>   
                            @endforeach
                        </tbody>
                    </table>
                    <div class="btn-group"><button class="btn btn-info active">1 </button></div>                   
                </form>
            </div> 
        </div>
        <div id="userinfo3" class="container tab-pane fade"><br>
            <div class="panel-body"> 
                <form method="post" action="{{route('trangchu.postPassword')}}">
                    @csrf
                    <div class="form-group">
                        <label class="control-label" for="passwordnew">Mật khẩu mới</label>
                        <input type="password" name="passwordnew" class="form-control input-transparent">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="repeatpasswordnew">Xác nhận mật khẩu mới</label>
                        <input type="password" name="repeatpasswordnew" class="form-control input-transparent">
                    </div>
                    <button style="margin-right: 5px" class="btn btn-primary" type="submit" name="btnLuuMatKhau">Lưu thay đổi</button>
                    <button class="btn btn-danger" type="reset">Nhập lại</button>
                </form>
            </div>
        </div>
        <div id="userinfo4" class="container tab-pane fade"><br>
            <form action="{{route('trangchu.getChangeEmail')}}" method="POST" style="background: #ffffff">
                @csrf
                {{method_field('GET')}}
                <div class="row">
                    <div class="col-md-4">
                        <img src="{{asset('frontend/asset/img/guard.jpg')}}" style="width: 100%">
                    </div>
                    <div class="col-md-8">
                        <h4 style="text-align: center; font-size: 16px; margin-top:3rem;">Để bảo vệ bảo mật tài khoản của bạn, chúng tôi cần xác minh danh tính của bạn
                        <br>
                        <br> Vui lòng chọn cách xác minh:</h4>
                        <br>
                        <button type="submit" name="btnSendMail" class="btn btn-primary" style="width: 75%; margin-left:12.5%; background-color: #35cc7b; color: #ffffff; border:#35cc7b ">
                        <i style="font-size: 25px" class="fa fa-envelope">&ensp;</i>Xác minh qua Email</button>
                    </div>
                </div>
            </form>
          </div>
    </div>
</div>
<script>
$(document).ready(function(){
    var url = "./api/province";
    $.getJSON(url,function(result){
        $.each(result, function(i, field){
            var idprovince1  = $("#idprovince1").val();
            var id   = field.provinceid;
            var name = field.name;
            var type = field.type;
            var select = "";
            if(id==idprovince1)
            {
                select = 'selected=""';
            }
            $("#province").append('<option value="'+id+'" '+select+'>'+type+" "+name+'</option>');
        });
    });
    var idprovince1 = $("#idprovince1").val();
    $("#district").html('<option value="-1">Lựa chọn Quận/ Huyện</option>');
    var url = "./api/district/"+idprovince1;
    $.getJSON(url, function(result){
        console.log(result);
        $.each(result, function(i, field){
            var iddistrict1  = $("#iddistrict1").val();
            var id   = field.districtid;
            var name = field.name;
            var type = field.type;
            var select = "";
            if(id==iddistrict1)
              {
                select = 'selected=""';
              }
            $("#district").append('<option value="'+id+'" '+select+'>'+type+" "+name+'</option>');
        });
    });
    var iddistrict1 = $("#iddistrict1").val();
    $("#ward").html('<option value="-1">Lựa chọn Phường/ Xã</option>');
        var url = "./api/ward/"+iddistrict1;
        $.getJSON(url, function(result){
            console.log(result);
            $.each(result, function(i, field){
                var idward1  = $("#idward1").val();
                var id   = field.wardid;
                var name = field.name;
                var type = field.type;
                var select = "";
                if(id==idward1)
                  {
                    select = 'selected=""'
                  }
                $("#ward").append('<option value="'+id+'" '+select+'>'+type+" "+name+'</option>');
        });
    });
    $("#province").change(function(){
        $("#district").html('<option value="-1">---- Lựa chọn Quận/ Huyện ----</option>');
        var idprovince = $("#province").val();
        var url = "./api/district/"+idprovince;
        $.getJSON(url, function(result){
            console.log(result);
            $.each(result, function(i, field){
                var id   = field.districtid;
                var name = field.name;
                var type = field.type;
                $("#district").append('<option value="'+id+'">'+type+" "+name+'</option>');
            });
        });
    });
    $("#district").change(function(){
        $("#ward").html('<option value="-1">---- Lựa chọn Phường/ Xã ----</option>');
        var iddistrict = $("#district").val();
        var url = "./api/ward/"+iddistrict;
        $.getJSON(url, function(result){
            console.log(result);
            $.each(result, function(i, field){
                var idward1  = $("#idward1").val();
                var id   = field.wardid;
                var name = field.name;
                var type = field.type;
                var select = "";
                $("#ward").append('<option value="'+id+'" '+select+'>'+type+" "+name+'</option>');
            });
        });
    });
    if($("#tongtienmua").val() >= $("#vipmoney").val()){    
        $("#khachvip").html('<label id="khachvip" style="font-size:20px;">Khách hàng thân thiết: <span class="badge badge-warning" style="font-size:20px;">VIP</span></label>');
    }else{ 
        $("#khachvip").html('');
    }
});
</script>
@endsection