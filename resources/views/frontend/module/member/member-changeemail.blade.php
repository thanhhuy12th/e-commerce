@extends('frontend.welcome')

@section('content')
<div class="jumbotron">
    <div style="margin-top: -20px;background: #ffffff" class="container">
        @include('admin.blocks.alert') 
        <h5 style="padding-top: 5px"><i style="font-size: 25px;color: #35cc7b" class="fa fa-envelope">&ensp;</i>Chúng tôi đã gửi một mã xác minh về email của bạn.</h5>
            <div class="row">
                <div class="col-md-4">
                    <img src="{{asset('frontend/asset/img/guard.jpg')}}" style="width: 100%">
                </div>
                <div class="col-md-8">
                    <form action="{{route('trangchu.postChangeEmail')}}" method="POST" >
                    @csrf   
                    <div class="form-group" style="margin-top: 1.5rem">
                        <label>Nhập Email đăng ký mới:</label>
                        <input type="text" required="" name="email" placeholder="123456@gmail.com" class="form-control input-transparent" style="width: 75%">
                    </div>
                    <div class="form-group">
                        <label>Nhập mã xác minh:</label>
                        <input type="number" name="maxacminh" placeholder="Gồm 6 chữ số" class="form-control input-transparent" style="width: 75%">
                    </div>
                    <button onClick="return xacnhanxoa('Bạn sẽ chuyển sang trang đăng nhập');" type="submit" name="btnSaveChange"class="btn btn-primary" style="margin-left: 25%;width: 50%; background-color: #35cc7b; color: #ffffff; border:#35cc7b ">
                        Xác nhận
                    </button>
                    </form>
                </div>
            </div>   
    </div>
</div>
@endsection