<!doctype html>
<html class="no-js" lang="en">

<head>
    @include('admin.blocks.head')
</head>

<body>
    <style type="text/css">
        .btn_login{
            margin-bottom: 30px;
            margin-top: 40px
        }
        .pages{
            margin-top: 50px
        }
    </style>

    <div class="color-line"></div>
    <!-- <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="back-link back-backend">
                    <a href="home" class="btn btn-primary">Back to Dashboard</a>
                </div>
            </div>
        </div>
    </div> -->
    <div class="container-fluid pages">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"></div>
            <div class="col-md-4 col-md-4 col-sm-4 col-xs-12">
                <div class="btn_login text-center m-b-md custom-login">
                    <h3>Thông tin cá nhân </h3>
                    <p><a style="color: blue " href="{{route('trangchu.getInformation')}}">Back to Page Information</a></p>
                </div>
                @include('admin.blocks.alert')      
                <div class="hpanel">
                    <div class="panel-body">
                        <form action="{{route('trangchu.postPassword')}}" method="post">
                           <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div style="margin-top: 30px" class="form-group">
                                <label class="control-label" for="password">Password cũ</label>
                                <input name="password" type="password" placeholder="******" required="" class="form-control">
                                <span class="help-block small"></span>
                            </div>
                            <div style="margin-top: 30px" class="form-group">
                                <label class="control-label" for="passwordnew">Password mới</label>
                                <input name="passwordnew" type="password" placeholder="******" required="" class="form-control">
                                <span class="help-block small"></span>
                            </div>
                            <div style="margin-top: 30px" class="form-group">
                                <label class="control-label" for="repeatpasswordnew">Nhập lại Password mới</label>
                                <input name="repeatpasswordnew" type="password" placeholder="******" required="" class="form-control">
                                <span class="help-block small"></span>
                            </div>
                            <!-- <div class="checkbox login-checkbox">
                                <label>
										<input type="checkbox" class="i-checks"> Remember me </label>
                                <p class="help-block small">(if this is a private computer)</p>
                            </div> -->
                            <button class="btn btn-success btn-block loginbtn btn_login" type="submit" >Signup</button>
                            
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"></div>
        </div>
        <div class="row">
            <div class="col-md-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <p>Bản quyền &copy; 2019 <a href="https://cloudone.vn">Cloudone</a>
            </div>
        </div>
    </div>

    <!-- jquery
		============================================ -->
    @include('admin.blocks.script')
</body>

</html>