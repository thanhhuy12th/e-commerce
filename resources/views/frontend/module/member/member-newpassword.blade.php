<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"> <!--320-->
	<title>Bách Khoa | Quên mật khẩu</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <style>
	.frm-forgot-password{
		max-width: 512px;
		margin-top: 49px;
	}
	.logo-bk > a{
		width: 280px;
		height: 180px;
		padding: 30px 20px;
		border-radius: 50%;
		text-align: center;
		display: block;
		margin: 0 auto 20px auto;
	}
	</style>
</head>
<body>    
	<div class="container frm-forgot-password">
		<div class="logo-bk">
			<a href="{{route('trangchu.index')}}">
				<img src="{{ asset('image/'.getcauhinh('photos')) }}" class="img-responsive logo-bk">
			</a>
        </div>
        @include('admin.blocks.alert')
        <form action="" method="POST">
        {{ csrf_field() }}
	        <div class="panel panel-success">
	            <div class="panel-heading">THAY ĐỔI MẬT KHẨU</div>
	            <div class="panel-body">
					<div class="msg-password"></div>
					<div class="form-group">
						<input name="newpassword" type="password" class="form-control" placeholder="Nhập mật khẩu mới">
					</div>
					<div class="form-group">
						<input name="renewpassword" type="password" class="form-control" placeholder="Nhập lại mật khẩu mới">
					</div>
				</div>
				<div class="panel-footer">
					<button name="updatePass" type="submit" class="btn btn-success btn-forgot-password">Save</button>
					<a style="float: right;" href="{{route('trangchu.getLogin')}}" class="btn">Quay về trang đăng nhập</a>			
				</div>
			</div>
		</form>	
	</div>
</body>
</html>