<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
	@include('frontend.template.head')
</head>
<body>
	<!-- header -->
	@include('frontend.template.header')
	<!-- navigation -->
	@include('frontend.template.navigation')
	<!-- content -->
	@yield('content')
	<!-- footer -->
	@include('frontend.template.footer')
	<!-- Script website -->
	@include('frontend.template.footer_script')
</body>
</html>
