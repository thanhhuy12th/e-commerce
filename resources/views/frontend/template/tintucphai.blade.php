<div class="header-menu" style="margin-top: 25px">
    <h5 style="color:#35cc7b"><img src="{{ asset('frontend/asset/img/list.png') }}"" width="24px" height="24px">&nbsp;<strong> {{trans('message.tintuc')}}</strong></h5>
</div>
<div class="side-tintuc">
    <ul class="category-new">
    	<?php 
            $tintuc = select_tintuc();
        ?>
        @foreach($tintuc as $ttp)
        	<li><a href="{{route('trangchu.tintucdetail', ['alias' => $ttp->alias])}}">{{$ttp->title}}</a></li>
        @endforeach

    </ul>
</div>