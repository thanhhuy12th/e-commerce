
<script> 
    window.onscroll = function() {scrollFunction()};    
    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("mBtntotop").style.display = "block";
        } else {
            document.getElementById("mBtntotop").style.display = "none";
        }
    }    
    $("#mBtntotop").click(function(e){
        e.preventDefault();
        $('html,body').animate({scrollTop: 0}, 700);
    });
    $("section.alert").delay(4000).slideUp();

    $("div.alert").delay(5000).slideUp();
</script>

 