<style type="text/css">
    .website{
            background-color: #191818; 
            color: #ffffff; 
            text-decoration: none;
            padding: 10px;
            text-align: center;
            width: auto;
    } 
    .websitetext{
            text-align: center;
    }
    .websitename{
            color: #ffffff; 
            text-decoration: none;
    }
    .websitehotline{
            color: white; 
            size: 50px;
    }
</style>

<div id="footer">
    <div class="row">
        <div class="col-md-3">
            <div class="col-md-3 col-3" style="float:left">
                <img id="minilg" src="{{ asset('image/img/icon-hang-chinh-hang.png') }}" style="height: 40px">
            </div>
            <div class="col-md-9 col-9"style="font-weight: bold;color: #35cc7b;float:right">
                <span>Sản phẩm, hàng hóa</span><br> 
                <span>chính hãng, phong phú</span>
            </div>   
        </div>
        <div class="col-md-3 ">
            <div class="col-md-3 col-3" style="float:left">
                <img id="minilg" src="{{ asset('image/img/icon-kmkn.png') }}" style="height: 40px">
            </div>
            <div class="col-md-9 col-9"style="font-weight: bold;color: #35cc7b;float:right">
                <span>Luôn luôn giá rẻ &</span><br> 
                <span>Khuyến mãi không ngừng</span>
            </div>   
        </div>
        <div class="col-md-3">
            <div class="col-md-3 col-3" style="float:left">
                <img id="minilg" src="{{ asset('image/img/icon-dv.png') }}" style="height: 40px">
            </div>
            <div class="col-md-9 col-9"style="font-weight: bold;color: #35cc7b;float:right">
                <span>Dịch vụ</span><br> 
                <span>Chăm sóc khách hàng uy tín</span>
            </div>   
        </div>
        <div class="col-md-3">
            <div class="col-md-3 col-3" style="float:left">
                <img id="minilg" src="{{ asset('image/img/icon-tvbh.png') }}" style="height: 40px">
            </div>
            <div class="col-md-9 col-9"style="font-weight: bold;color: #35cc7b;float:right">
                <span>Tư vấn khách hàng</span><br> 
                <b><a href="" style="color: red; text-decoration: none"><?php echo getCauhinh("dienthoai"); ?></a></b>
            </div>   
        </div>
    </div> 
</div> <!--footer-->

<div class="footer2" style="padding-top: 10px" >
    <div class="row">
        <div class="col-md-4">
            <h6 style="font-weight: bold; padding-top: 25px; text-align:center; font-size: 19px; color: #fffd87">ĐĂNG KÝ NHẬN KHUYẾN MÃI</h6>
        </div>
        <div class="col-md-4" style="padding-top:15px; padding-bottom:25px">
            <form action="" method="POST">
                <input type="text" name="txtEmailContact_cloudone_anti_spam" class="form-dky" placeholder="  Nhập email" aria-label="Recipient's username" aria-describedby="button-addon2" style="width: 72%;border-radius: 4px">
                <input type="hidden" name="post_type" value="post">
                <button type="submit" name="btnSubmitContact" class="btn btn-success" style="width: 26%;background: #fffd87;color: #007C17">Đăng ký</button>
            </form>
        </div>
        <div class="col-md-4">
            <div class="social padding" style="text-align:center; margin-bottom: 10px; padding-top:10px;">
                <a href="https://www.facebook.com/<?php echo getCauhinh('facebook'); ?>" style="background-color: #fffd87 ;width: 40px;height: 40px;"><i class="fab fa-facebook"></i></a  >
                <a href="https://www.instagram.com/<?php echo getCauhinh('instagram'); ?>" style="background-color: #fffd87 ;width: 40px;height: 40px;"><i class="fab fa-instagram"></i></a>
                <a href="https://www.youtube.com/channel/<?php echo getCauhinh('youtube'); ?>" style="background-color: #fffd87 ;width: 40px;height: 40px;"><i class="fab fa-youtube"></i></a>
            </div>  
        </div>
    </div>    
</div><!--Footer2-->


<div class="support">
    <div class="row">
        <div style="margin-left: -120px" class="col-md-4 addr">
            <p style="font-size: 14px; padding-top: 22px">
                <?php echo getCauhinh('tenwebsite'); ?><br>
                Mọi thắc mắc, đóng góp ý kiến hoặc tư vấn hỗ trợ xin vui lòng liên hệ thông tin bên dưới
            </p >
            <p style="font-size: 14px"> – Phone: <?php echo getCauhinh("dienthoai"); ?>
                <br>– Email: <?php echo getCauhinh('email'); ?>
                <br>– Website: <?php echo getCauhinh('linkwebsite'); ?>
            </p>
        </div>
        <div style="margin-left: -45px" class="col-md-4">
            <h6 style="font-weight: bold; padding-top: 22px; text-align:center; font-size: 17px; color: #ede628">Thông tin liên hệ</h6>
            <span>
                <ul>
                    <li><strong>Địa chỉ:</strong> <?php echo getCauhinh('title'); ?></li>
                    <li><strong>Điện thoại:</strong> <?php echo getCauhinh("dienthoai"); ?></li>
                    <li><strong>Email:</strong> <?php echo getCauhinh('email'); ?></li>
                    <li><strong>Website:</strong> <?php echo getCauhinh('linkwebsite'); ?></li>
                </ul>
            </span>
        </div>
        <div style="margin-left: -45px" class="col-md-4">
            <h6 style="font-weight: bold; text-align:center; padding-top: 22px; font-size: 17px;color: #ede628">Tin tức</h6>
            <span>
                <ul>
                    <li><strong>Email:</strong> <?php echo getCauhinh('email'); ?></li>
                </ul>
            </span>
        </div>
        <div style="margin-left: -5px" class="col-md-2">
            <h6 style="font-weight:  bold; padding-top: 22px; font-size: 17px; color: #ede628">Fanpage</h6>
<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2F<?php echo getCauhinh('facebook'); ?>%2F&tabs&width=280&height=214&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="280" height="214" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>    </div>
</div><!--support-->
</div>
<div class="col-12" style="background-color: #191818; color: #ffffff; text-decoration: none;padding: 10px;text-align: center;">
                <span style="text-align: center">Duy trì bởi &copy; <a href="https://cloudone.vn/" style=" color: #ffffff; text-decoration: none"> www.Cloudone.vn</a></span>
            </div>
<div>
    <button id="mBtntotop" title="Go to top">
        <i class="fas fa-angle-up"></i>
    </button>
    <button a="" href="tel:<?php echo getCauhinh('dienthoai'); ?>" class="hotline">
        <i class="fas fa-phone faa-pulse animated fa-2x websitehotline"></i>
    </button>
</div>


