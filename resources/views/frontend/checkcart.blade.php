<!doctype html>
<html class="no-js" lang="en">

<head>
    @include('admin.blocks.head')
</head>
<body>
<section>
        <div class="container">
            <div class="row">
                <form action="{{ route('post.postCheckCart') }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="col-sm-12 clearfix">
                    <div class="container">
                        <div class="breadcrumbs">
                            <ol class="breadcrumb">
                                <li><a href="{{route('get.list.cart')}}">List cart </a></li><li> / </li> 
                                <li class="active"> Shopping Cart</li>
                            </ol>
                        </div>
                        <div class="input-group mg-b-pro-edt">
                            @foreach($hinhthuc as $ht)
                            <span class="input-group-addon">
                                <input type="radio" name="phuongthuc" value="{{$ht->id}}" {{($ht->id==1 ? 'checked' : "")}} > {{$ht->name}}<br/>
                            </span>
                            @endforeach
                        </div>
                        <div class="bill-to">
                            <p style="font-size: 20px;font-weight: bold;">Chọn phương thức vận chuyển</p>
                            @foreach($vanchuyen as $vc)
                                <input type="radio" name="vanchuyen" value="{{$vc->id}}" {{($vc->id==1 ? 'checked' : "")}} > {{$vc->name}}  <br>Giá: {{convert_money($vc->gia)}} đ<br/>
                            @endforeach
                        </div>
                        <div class="bill-to">
                            <p style="font-size: 20px;font-weight: bold;">Thông tin tài khoản</p>
                            <label>Ho ten</label>
                            <input value="" type="text" name="hoten" value="{{ old('hoten') }}" placeholder="Họ và Tên *">
                            <label>Số tk</label>
                            <input value="" type="text" name="sotk" value="{{ old('sotk') }}" placeholder="Số tk *">
                            <label>Ngày mở</label>
                            <input value="" type="text" name="ngaymo" value="{{ old('ngaymo') }}" placeholder="Ngay">
                        </div>
                        <div style="margin-top: 30px" class="input-group mg-b-pro-edt">
                            <span class="input-group-addon">
                                <input type="radio" name="type_product" value="0"  checked=""> Dia chi hien tai <br/>
                            </span>
                            <span class="input-group-addon">
                                <input type="radio" name="type_product" value="1"  > Khac <br/>
                            </span>
                        </div>
                        <div class="bill-to">
                            <p style="font-size: 20px;font-weight: bold;">Thông tin khách hàng</p>
                            <p style="color: red; font-size: 16px">{{$fullname->fullname}}</p>
                            <p style="color: red; font-size: 16px">{{$fullname->email}}</p>
                            <p style="color: red; font-size: 16px">{{$fullname->diachi}} {{$area}}</p>
                            <p style="color: red; font-size: 16px">{{$fullname->phone}}</p>
                            <textarea name="ghichu" value="{{ old('message') }}"  placeholder="Ghi chú" rows="10">1</textarea>
                        </div>
                        <div class="bill-to">
                            <p style="font-size: 20px;font-weight: bold;">***Nhap dia chi khac </p>
                                <div class="form-one">
                                    <input value="1" type="text" name="hoten" value="{{ old('hoten') }}" placeholder="Họ và Tên *">
                                    <input value="1" type="text" name="email" value="{{ old('email') }}" placeholder="Email *">
                                    <input value="1" type="text" name="diachi" value="{{ old('diachi') }}" placeholder="Địa Chỉ *">
                                    <input value="1" type="text" name="sodt" value="{{ old('sodt') }}" placeholder="Số điện thoại *">
                                    <p style="color: red; font-size: 14px">(*) Thông tin quý khách phải nhập đầy đủ</p>
                                </div>
                                <div class="form-two">
                                    <textarea name="ghichu" value="{{ old('message') }}"  placeholder="Ghi chú" rows="10">1</textarea>
                                </div>
                        </div>

                    </div>
                </div>
                <div class="col-sm-12">
                    <section id="cart_items">
                        <div class="container">
                            <div class="table-responsive cart_info">
                                <table class="table table-condensed">
                                    <thead>
                                    <tr class="cart_menu">
                                        <td class="image">Ảnh minh họa</td>
                                        <td class="description">Tên sản phẩm</td>
                                        <td class="price">Giá gốc</td>
                                        <td class="price">Giá km</td>
                                        <td class="quantity">Số lượng</td>
                                        <td class="total">Tổng</td>
                                        <td></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($cart))
                                        @foreach($cart as $item)
                                            <tr>
                                                <td class="cart_product" style="margin: 0px">
                                                    @php $arr_img = explode("||",$item->options->img);@endphp 
                                                     <img width="100px" height="100px" src="{{ asset('image/'.$arr_img[0]) }}" alt="">
                                                </td>
                                                <td class="cart_description">
                                                    <h4><a href="">{{ $item->name }}</a></h4>

                                                    <p>Web ID: {{ $item->id }}</p>
                                                </td>
                                                <td class="cart_price">
                                                    <p>{{ number_format($item->options->price_goc)}} VNĐ</p>
                                                </td>
                                                <td class="cart_price">
                                                    <p>{{ number_format($item->price)}} VNĐ</p>
                                                </td>
                                                <td class="cart_quantity">
                                                    {{ $item->qty }}
                                                </td>
                                                <td class="cart_total">
                                                    <p class="cart_total_price">{{ number_format($item->subtotal)}}
                                                        VNĐ</p>
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="4">&nbsp;
                                            <span>
                                            <a class="btn btn-default update" href="{{ route('get.list.cart')}}">Quay về giỏ
                                                hàng</a>
                                            </span>

                                            </td>
                                            <td colspan="2">

                                                <table class="table table-condensed total-result">
                                                    <tbody>
                                                        
                                                    <tr>
                                                        <td>Tổng gia :</td>
                                                        <td><span style="float: right">{{ convert_money($total+ 11000)}} VNĐ</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <button name="sendMail" type="submit" class="btn btn-default check_out" href="{{ route('post.postCheckCart')}}">Gửi đơn hàng</button>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    @else
                                        <tr>
                                            <td>You have no items in the shopping cart</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">&nbsp;
                                                <a class="btn btn-default update" href="{{ route('get.list.cart')}}">Mua hàng</a>
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                    <!--/#cart_items-->
                </div>
                </form>
            </div>
        </div>
    </section>
        @include('admin.blocks.script')
</body>

</html>