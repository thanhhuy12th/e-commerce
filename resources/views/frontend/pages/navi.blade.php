@extends('frontend.welcome')

@section('content')
    
<div class="row jumbotron" style="padding-top:1em">
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xl-9" style="background: #fff;padding:10px 20px;" >
        <div class="row" id="title">
            <div class="col-lg-12 col-mg-12 col-sm-12 col-xs-12">
                <h2 class="product-name"><a href="#">{{$getdmcha->name}}</a></h2>
            </div>
        </div>

        <div class="product-list">
            <div class="row">
                <?php 
                    $con = [];
                    for($j=0;$j<count($idcon);$j++)
                    {   //17 cha  == 17 con
                        if($idcon[$j]->id_parent == $getdmcha->id)
                        {
                           $id = $idcon[$j]->id;
                           array_push($con, $id);
                        }
                    }
                    for($i=0;$i<count($con);$i++)
                    {
                        $data = DB::table('skl_sanpham')->where('category',$con[$i])
                                        ->paginate(6); 
                        for($s=0;$s<count($data);$s++)
                        {
                    ?>
                            <div id="box-product1" class="col-lg-4 col-sm-12 col-12">
                                <div class="thumbnail-c1">
                                    @if($data[$s]->img == "")
                                        Không có hình ảnh
                                    @else
                                        @php $arr_img = explode("||",$data[$s]->img);@endphp
                                        <img src="{{ asset('image/'.$arr_img[0]) }}" alt="{{$data[$s]->tensp}}" class="image-prod" >
                                    @endif
                                    <div class="overlay">
                                        {{$data[$s]->chitietngan}}
                                    </div>
                                </div>
                                s<h5>{{$data[$s]->tensp}}</h5>
                                <div class="price-area">
                                    <del><span class="price">{{$data[$s]->price}}</span></del> &nbsp;&nbsp; 
                                    <span class="price discount">{{$data[$s]->price_km}}</span>
                                </div>  
                                <div class="circles" style="padding-top: 8px" >
                                    <?php echo ceil((($data[$s]->price-$data[$s]->price_km)/$data[$s]->price)*100)."%"; ?>
                                </div>
                                <p><button><a href="" style="text-decoration: none; color: #ffffff">Xem chi tiết</a></button></p>
                            </div>

                <?php 
                        }
                        ?>
                        
                        <!-- <row class="col-md-12"> -->
                            
                        <!-- </row> -->
                        <row class="col-md-12">
                        <div style="margin-top: 30px">
                            <center>
                            @if ($data->lastPage() > 1)
                                <ul  class="pagi">
                                    <!-- <li class="{{ ($data->currentPage() == 1) ? ' disabled' : '' }} btn-group" style="margin-right:8px">
                                        <span class='pagi-item'>
                                             <a href="{{ $data->url(1) }}"><&nbsp;</a>
                                         </span>
                                     </li> -->
                                    
                                    @for ($i = 1; $i <= $data->lastPage(); $i++)
                                        <li class="{{ ($data->currentPage() == $i) ? ' active' : '' }} btn-group" style="margin-right:8px">
                                            <span class="pagi-item">
                                                <a href="{{ $data->url($i) }}">{{ $i }}&nbsp;</a>
                                            </span>
                                        </li>
                                    @endfor
                                   <!--  <li class="{{ ($data->currentPage() == $data->lastPage()) ? ' disabled' : '' }} btn-group">
                                        <span class='pagi-item'>
                                            <a href="{{ $data->url($data->currentPage()+1) }}">>&nbsp;</a>
                                        </span>
                                    </li> -->
                                </ul>
                            @endif
                            </center>
                        </div>
                        </row>
                        <?php
                    } ?>
        
            </div>
        </div>
        
        
    </div>
    @include('frontend.template.menupages')
</div>  

@endsection