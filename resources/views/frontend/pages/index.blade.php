@extends('frontend.welcome')
@section('content')
@include('frontend.template.menu')
<!--product-home-->    
<div id="product-home">
    <?php 
        $data_category = select_categoty_parent();
    ?>
    @foreach($data_category as $dc)
    <div class="row" id="title">
        <div class="col-lg-9 col-mg-9 col-sm-9 col-xs-12">
            <h2 class="product-name">
                <a href="#">{{ucwords($dc->name)}}</a>
            </h2>
        </div>
        <div class="col-lg-3 col-mg-3 col-sm-3" style="text-align: right">
            <a href="{{route('trangchu.getPages', ['alias' => $dc->alias])}}"><button class="btn default btn-xtc">Xem tất cả</button></a>
        </div>
    </div>
    <div class="product-list">
        <div class="row">


            <?php
            $data = select_products_to_category_parent_limit($dc->id,0,4);
            ?>
            @foreach($data as $dt)
            <div id="box-product1" class="col-lg-3 col-sm-12 col-12">
                <div style="position: relative;">
                    @php $arr_img = explode("||",$dt->img);@endphp
                    <img src="{{ asset('image/'.$arr_img[0]) }}" alt="" class="image-prod" >
                    <div class="overlay">
                        {!!$dt->chitietngan!!}
                    </div>
                </div>
                <h5>{{$dt->tensp}}</h5>
                <div class="price-area">
                    <del><span class="price">{{$dt->price}} </span></del> &nbsp;&nbsp; 
                    <span class="price discount">{{$dt->price_km}}</span>
                </div>
                <div class="circles" style="padding-top: 8px" >
                    <?php echo ceil((($dt->price-$dt->price_km)/$dt->price)*100)."%"; ?>
                </div>
                <p><a href="{{route('trangchu.detail', ['alias' => $dt->alias])}}" style="text-decoration: none; color: #ffffff"><button>Xem chi tiết</button></a></p>
            </div>
            @endforeach
        </div>
    </div>
    @endforeach
</div>
<!--tintuc-home-->
<div id="tintuc-home">
    <div class="row" id="title" style="padding-top: 10px">
        <div class="col-md-12">
            <h2 class="product-name"><a href="#">Tin Tức</a></h2>
        </div>  
    </div>
    <div class="row" >
        <?php 
            $tintuc = select_tintuc();
        ?>
        @foreach($tintuc as $tt)
        <div id="tintuc-card" class="col-lg-3 col-sm-6 col-12">
            <div class="thumbnail-c1" style="margin-bottom: 15px">
                @php $arr_img = explode("||",$tt->img);@endphp
                <img src="{{ asset('image/'.$arr_img[0]) }}" alt="{{$tt->title}}" class="image-tintuc">
            </div>
            <p><a href="">{!!$tt->title !!}</a></p>
            <span>
                <i class="fas fa-clock"></i> 20/10/2020
                <a href="{{route('trangchu.tintucdetail', ['alias' => $tt->alias])}}">Xem chi tiết</a>
            </span>
        </div>
        @endforeach
    </div>
</div>
<style type="text/css">
    .mota{
        style="font-weight: 400;
        text-transform: uppercase;
    }
</style>
<!--mota-home-->
<div id="info-home">
    <h4 style="text-align: center;">
        <span class="mota"><?php echo getCauhinh("tenwebsite"); ?></span>
    </h4>
    <?php echo getCauhinh("gioithieu"); ?>
</div>
@endsection