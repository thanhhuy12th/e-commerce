@extends('frontend.welcome')

@section('content')
<div class="row jumbotron" style="padding-top:1em">
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xl-9"style="background-color:#ffffff; padding:15px 40px; text-align: justify;">
        <br>
        <h4><b><strong>Giới thiệu</strong></b></h4>
        <hr>
        <?php echo getCauhinh("gioithieu"); ?>
        
    </div>
    @include('frontend.template.menupages')
</div>
@endsection

