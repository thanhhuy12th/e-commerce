@extends('frontend.welcome')
@section('content')
<div class="row jumbotron" style="padding-top:1em">
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xl-9"style="background-color:#ffffff; padding:15px 20px; text-align: justify;">
        @include('admin.blocks.alert')
        <div style="background-color: #ffffff; margin:5px 0px; padding: 15px;">
            <div class="row">
                <div class="col-md-6" >
                <h5 style="color: #35cc7b">Gửi thông tin liên hệ</h5>
                <hr>
                <form action="{{route('trangchu.store')}}" method="POST">
                    @csrf
                    <p><input type="text" name="hoten" class="form-control" placeholder="Họ và tên"></p>
                    <p><input type="text" name="email" class="form-control" placeholder="Email"></p>
                    <p><input type="text" name="sdt" class="form-control" placeholder="Số điện thoại"></p>
                    <p><textarea class="form-control" name="noidung" placeholder="Nội dung"></textarea></p>
                    <p><input type="text" name="spam_cloudone_anti_spam" class="form-control" placeholder="Viết tắt của nước Việt Nam ? (gợi ý: vn)"></p>
                    <p><button type="submit" name="btnSubmitContact" class="btn btn-success" >Gửi ngay</button></p>
                </form>
            </div>
            <div class="col-md-6" >
                <h5 style="color: #35cc7b">Thông tin liên hệ</h5>
                <hr>
                <p>Địa chỉ: <?php echo getCauhinh("title"); ?></p>
                <p>Điện thoại: <?php echo getCauhinh("dienthoai"); ?></p>
                <p>Email: <?php echo getCauhinh("email"); ?></p>
                <p>Website: <?php echo getCauhinh("linkwebsite"); ?></p>
            </div>
            <div class="col-md-12 google-map">
                <hr>
                <p><iframe src="<?php echo getCauhinh("googlemap"); ?>" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen=""></iframe>
                </p>
            </div>
            </div>
        </div>
    </div>
    @include('frontend.template.menupages')
</div>
@endsection