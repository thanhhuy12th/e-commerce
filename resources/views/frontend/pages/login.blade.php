<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width"/>
    <title ><?php echo getCauhinh("tenwebsite"); ?></title>
    <link rel="shortcut icon" type="image/png" href="{{ asset('image/'.getCauhinh('photos')) }}"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="{{ asset('frontend/asset/loginstyle.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/asset_admin/css/dropzone/dropzone.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/asset_admin/css/alerts.css') }}">
    <script src="{{ asset('frontend/asset_admin/js/myscript.js') }}"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
</head>
<body style="background-color: #35cc7b">
    <div class="container" style="padding: 0 0;" >
        <div class="row login-row" style="border-radius: 10px;">
            <div class="col-md-6 left-col">
                <img class="background img-responsive" src="../image/img/team2.1.jpg">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-8 text-center" style="padding-top: 50px; padding-right: 50px">
                            <a href="{{route('trangchu.index')}}"><img class="avt img-responsive" src="{{ asset('image/'.getcauhinh('photos')) }}" width="165px" height="180px"></a>
                            <p class="status">BÁCH KHOA SHOP</p>
                            <hr class="line">
                            <p class="status" style="margin-bottom:0px;margin-left: 45%;">Owners</p>
                            <p class="status">Questionnaire</p>
                            <hr class="line" style="padding-top:70px">
                            <div class="col-12" style=" text-decoration: none;padding: 10px;text-align: right;">
                                <span style="text-align: center">Duy trì bởi © <a href="#" style=" color: #191919; text-decoration: none"> www.cloudone.vn</a></span>
                            </div>
                        </div>
                </div> 
            </div>
            <div class="col-md-6 right-col">
                <h5 style="text-align:center; color: #757575">Nhập email đăng ký và password để đăng nhập</h5>
                <hr>
                <div class="col-sm-8" style="margin-left: 10%;position: absolute; z-index:9; font-size: 18px">@include('admin.blocks.alert')</div>
                <div class="row">
                    <div class="col-md-2" style="background-color: #ffffff"></div>
                    <form action="" method="post" style="margin-top: 0px">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-md-10" style="margin-top: 50px">
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <input name="email" type="text" class="form-control" id="input" placeholder="example@gmail.com" style="opacity: 0.5; width: 300px">
                                </div>    
                            </div>
                            <br>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <input name="password" type="password" class="form-control" id="input" placeholder="Password" style="opacity: 0.5; width: 300px">
                                </div>
                            </div>
                            <br>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <button name="btnDangnhapMain" type="submit" class="button" style="height: calc(1.5em + .75rem + 2px);width: max-content">LOGIN <i class="fas fa-arrow-right"></i></button>
                                </div>
                            </div>
                        </div>
                        </form>
                        </div>
                <div class="row forgot-info" style="margin-top: 95px; margin-bottom: 5px; margin-left: 60px;">
                    <div class="col-md-3">
                        <a href="{{route('trangchu.getTimMatKhau')}}" id="forgot" style="font-size: 12px; right:0">Quên mật khẩu? </a>
                    </div>
                    <div class="col-md-3">
                        <a href="{{route('trangchu.getRegister')}}" id="forgot" style="font-size: 12px; left: 0">Đăng ký</a>
                    </div>
                    <div class="col-md-3">  
                            <a href="#" id="forgot" style="font-size: 12px; left: 0">Điều khoản </a>
                    </div>
                    <div class="col-md-3">  
                            <a href="{{route('trangchu.index')}}" id="forgot" style="font-size: 12px; left: 0">Trang chủ </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $("section.alert").delay(4000).slideUp();
    </script>
    @include('frontend.template.footer_script')
</body>
</html>
