@extends('frontend.welcome')

@section('content')
    
<div class="row jumbotron" style="padding-top:1em">
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 col-xl-9" style="background: #fff;padding:10px 20px;" >
        <div class="row" id="title">
            <div class="col-lg-12 col-mg-12 col-sm-12 col-xs-12">
                <h2 class="product-name"><a href="#">{{$idcon->name}}</a></h2>
            </div>
        </div>

        <div class="product-list">
            <div class="row">
                <?php
                    $id = $idcon->id;
                    $data_category_child = select_products_to_category_child($id);
                ?>
                @foreach($data_category_child as $dcc)
                    <div id="box-product1" class="col-lg-4 col-sm-12 col-12">
                        <div style="position: relative;">
                            @php $arr_img = explode("||",$dcc->img);@endphp
                            <img src="{{ asset('image/'.$arr_img[0]) }}" alt="{{$dcc->tensp}}" class="image-prod">
                            <div class="overlay">
                                {{$dcc->chitietngan}}
                            </div>
                        </div>
                        <h5>{{$dcc->tensp}}</h5>
                        <div class="price-area">
                            <del><span class="price">{{convert_money($dcc->price)}} đ</span></del> &nbsp;&nbsp; 
                            <span class="price discount">{{convert_money($dcc->price_km)}} đ</span>
                        </div>  
                        <div class="circles" style="padding-top: 8px" >
                            <?php echo ceil((($dcc->price-$dcc->price_km)/$dcc->price)*100)."%"; ?>
                        </div>
                        <p><button><a href="{{route('trangchu.detail', ['alias' => $dcc->alias])}}" style="text-decoration: none; color: #ffffff">Xem chi tiết</a></button></p>
                    </div>
                @endforeach
                        <row class="col-md-12">
                            <hr>
                        </row>
                        <row class="col-md-12">
                        <div  >
                            <center>
                            @if ($data_category_child->lastPage() > 1)
                                <ul  class="pagi">
                                    @for ($i = 1; $i <= $data_category_child->lastPage(); $i++)
                                        <li class="{{ ($data_category_child->currentPage() == $i) ? ' active' : '' }} btn-group" style="margin-right:8px">
                                            <span class="pagi-item">
                                                <a href="{{ $data_category_child->url($i) }}">{{ $i }}&nbsp;</a>
                                            </span>
                                        </li>
                                    @endfor
                                </ul>
                            @endif
                            </center>
                        </div>
                        </row>
        
            </div>
        </div>
        
        
    </div>
    @include('frontend.template.menupages')
</div>  

@endsection