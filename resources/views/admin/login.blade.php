<!doctype html>
<html class="no-js" lang="en">

<head>
    @include('admin.blocks.head')
</head>

<body>
    <style type="text/css">
        .btn_login{
            margin-bottom: 30px;
            margin-top: 40px
        }
        .pages{
            margin-top: 50px
        }
    </style>

    <div class="color-line"></div>
    <!-- <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="back-link back-backend">
                    <a href="home" class="btn btn-primary">Back to Dashboard</a>
                </div>
            </div>
        </div>
    </div> -->
    <div class="container-fluid pages">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"></div>
            <div class="col-md-4 col-md-4 col-sm-4 col-xs-12">
                <div class="btn_login text-center m-b-md custom-login">
                    <h3>Đăng nhập admin</h3>
                    <p> </p>
                </div>
                <div class="hpanel">
                    <div class="panel-body">
                        <form action="{{route('admin.postLogin')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label class="control-label" for="email">Email</label>
                                <input name="email" type="email" placeholder="example@gmail.com" required="" class="form-control">
                                <span class="help-block small"></span>
                            </div>
                            <div style="margin-top: 30px" class="form-group">
                                <label class="control-label" for="password">Password</label>
                                <input name="password" type="password" placeholder="******" required="" class="form-control">
                                <span class="help-block small"></span>
                            </div>
                            <!-- <div class="checkbox login-checkbox">
                                <label>
										<input type="checkbox" class="i-checks"> Remember me </label>
                                <p class="help-block small">(if this is a private computer)</p>
                            </div> -->
                            <button class="btn btn-success btn-block loginbtn btn_login" type="submit" >Login</button>
                            
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"></div>
        </div>
        <div class="row">
            <div class="col-md-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <p>Bản quyền &copy; 2019 <a href="https://cloudone.vn">Cloudone</a>
            </div>
        </div>
    </div>

    <!-- jquery
		============================================ -->
    @include('admin.blocks.script')
</body>

</html>