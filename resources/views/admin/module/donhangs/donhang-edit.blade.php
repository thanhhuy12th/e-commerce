@extends('admin.welcome')
@section('breadcome')
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-top: -23px;">
                            <ul class="breadcome-menu">
                                <li><a href="{{ route('index') }}">{{trans('message.trangchu')}}</a>
                                    <span class="bread-slash">/</span>
                                </li>
                                <li><a href="{{ route('donhang.index') }}">{{trans('message.danhsach')}}</a>
                                    <span class="bread-blod">/</span>
                                </li>
                                <li><span class="bread-blod">{{trans('message.btn_sua')}}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<style type="text/css">
    .edit{
        /*background: #EEECEC;*/
        padding: 10px;
        font-size: 26px;
        font-weight: bold;
        /*border-radius: 5px;*/
    }
    .modalback{
        background: -webkit-linear-gradient(178deg, #01A9DB 0%, #00BFFF 100%);
        background: linear-gradient(178deg, #01A9DB 0%, #00BFFF 100%);
    }
    .inhd{
        background: -webkit-linear-gradient(178deg, #01A9DB 0%, #00BFFF 100%);
        background: linear-gradient(178deg, #01A9DB 0%, #00BFFF 100%);
        padding: 7px 4px 4px 7px;
        color: #fff;
        border-radius: 7px;
    }
    .btna{
        padding: 6px 20px;
        color: #fff;
        border-radius: 5px;
    }
    .modal-close-area.modal-close-df {
    right: 35px;
}
    .modal-close-area {
    position: absolute;
    right: -30px;
    top: -20px;
}
.modal-close-area .close {
    opacity: 1;
}
.modal-close-area a {
    color: #fff;
    font-size: 16px;
    background: #03a9f4;
    height: 30px;
    width: 30px;
    text-align: center;
    line-height: 28px;
    display: block;
    border-radius: 50%;
}
.close {
    padding: 8px 10px;
    margin-top: -88px;
    margin-right: -58px;
    float: right;
    font-size: 15px;
    line-height: 1;
    color: #ffffffba;
    opacity: 1;
    background: #35cc7b;
    border-radius: 50%;
}
.modal-adminpro-general .modal-footer {
    border-top: 0px solid #fff;
}
.modal-footer {
    padding: 15px;
    text-align: right;
    border-top: 1px solid #e5e5e5;
}
a:visited, a:link, a:active {
    text-decoration: none;
}
.btnsave{
    padding: 12px 23px;
    color: #fff;
    font-size: 14px;
    background: #03a9f4;
    margin-left: 15px
}
.btnclose{
    padding: 12px 23px;
    color: #fff;
    font-size: 14px;
    background: #F42F2F;
    margin-left: 15px
}
</style>
<div class="single-product-tab-area mg-tb-15">
    @include('admin.blocks.alert')
    <!-- Single pro tab review Start-->
    <div class="blog-area mg-tb-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-7 col-md-3 col-sm-3 col-xs-12" style="margin-left: 20px;">
                    <div class="hpanel blog-box responsive-mg-b-30">
                        <div class="panel-heading custom-blog-hd">
                            <div class="col-sm-6 mg-b-15">
                                <p style="font-size: 21px;font-weight: bold;">Thông tin đơn hàng </p>
                            </div>
                            <div class="mailbox-pagination mg-b-15 ">
                                <a href='{{url("admin/in-hoa-don?token=$donhang->token")}}' onclick="window.open(this.href,'wìn','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=1000,height=1200,directories=no,location=no'); return false;" rel="nofollow"><button class="inhd" style="font-size: 19px;">IN ĐƠN HÀNG</button></a>
                            </div>
                        </div>
                        <div class="panel-body blog-pra">
                            <table style="width: 100%;font-size: 14px">
                                <tbody>
                                    @foreach($sanpham as $sp)
                                    <tr>
                                        <td style="width: 60%;padding: 5px"><a href="../../index.php?go=chitietsp&amp;id=38">{{$sp->ten_sp}}</a></td>
                                        <td style="width: 25%;padding: 5px;text-align: right;">{{convert_money($sp->price_km)}} x {{$sp->soluong}}</td>
                                        <td style="width: 15%;padding: 5px;text-align: right;">{{convert_money($sp->price_km*$sp->soluong)}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <hr>
                            <table style="width: 100%">
                                <tbody style="vertical-align: text-bottom;">
                                    <tr>
                                        <td style="width: 60%"></td>
                                        <td style="width: 15%;text-align: right;font-size: 16px;font-weight: bold">Giá</td>
                                        <td style="width: 15%;text-align: right;">{{convert_money($donhang->tongtien)}}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%"></td>
                                        <td style="width: 15%;text-align: right;font-size: 16px;font-weight: bold">Vận chuyển</td>
                                        <td style="width: 15%;text-align: right;">{{convert_money($donhang->phivanchuyen)}}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%"></td>
                                        <td style="width: 15%;text-align: right;font-size: 19px;color: blue;font-weight: bold;">Tổng cộng</td>
                                        <td style="width: 15%;text-align: right;">{{convert_money($donhang->tongtien+$donhang->phivanchuyen)}}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr>
                            <form action="{{ route('donhang.update', ['donhang' => $donhang->id]) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            {{ method_field('PUT') }}
                                <?php
                                if($donhang->status_paid!=1)
                                {?>
                                    <div style="width: 100%">
                                        <div style="width: 75%;float: left;">
                                            <h4>ĐƠN HÀNG CHƯA ĐƯỢC THANH TOÁN</h4>
                                        </div>
                                        <div style="width: 25%;float: right; text-align:right">
                                            <button style="background: #5cb85c;width: 107px" name="btnXacnhan" type="submit" class="btna" onclick="return xacnhan('Xác nhận khách hàng đã thanh toán toàn bộ số tiền cho đơn hàng này?')">Xác nhận
                                        </div>
                                    </div>
                                <?php } else {?>
                                <div style="width: 100%;color: green">
                                    <div style="width: 75%;float: left;">
                                        <h4>Đơn hàng đã xác nhận thanh toán {{convert_money($donhang->tongtien+$donhang->phivanchuyen)}}</h4>
                                    </div>
                                    <div style="width: 25%;float: right; text-align:right">
                                        <button  style="background: #FE2E2E;width: 107px" name="btnUndoXacnhan" type="submit" class="btna">Hoàn tác
                                    </div>
                                </div>
                                <?php } ?>
                                <div style="clear: both;"></div>
                            <hr>
                                <?php
                                if($donhang->status_move!=1)
                                {?>
                                <div style="width: 100%">
                                    <div style="width: 75%;float: left;">
                                        <h4>ĐƠN HÀNG CHƯA ĐƯỢC GIAO</h4>
                                    </div>
                                    <div style="width: 25%;float: right; text-align:right;">
                                        <button style="background: #5cb85c" name="btnUndoGiaohang" type="submit" class="btna">Giao hàng</button>
                                    </div>
                                </div>
                                <?php } else {?>
                                <div style="width: 100%;color: green">
                                    <div style="width: 75%;float: left;">
                                        <h4>Đơn hàng đã được giao</h4>
                                    </div>
                                    <div style="width: 25%;float: right; text-align:right" >
                                        <button  style="background: #FE2E2E;width: 107px" name="btnGiaohang" type="submit" class="btna">Hoàn tác</button>
                                    </div>
                                </div>
                                <?php } ?>
                                <div style="clear: both;"></div>
                            </form>
                        </div>
                        <div class="panel-footer">

                        </div>
                    </div>
                </div>
                <div style="margin-left: 50px" class="col-lg-4 col-md-3 col-sm-3 col-xs-12">
                    <div class="hpanel blog-box responsive-mg-b-30">
                        <div class="panel-heading custom-blog-hd">
                                <div class="col-sm-6 mg-b-15">
                                <p style="font-size: 19px;font-weight: bold;">Khách hàng</p>
                            </div>
                            <div class="mailbox-pagination mg-b-15 ">
                                <a data-toggle="modal" data-target="#myModal" style="font-size: 19px" class="inhd fa fa-edit"></a>
                                <div class="modal modal-adminpro-general default-popup-PrimaryModal PrimaryModal-bgcolor fade" tabindex="-1" role="dialog" id="myModal">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header modalback">
                                        <div class="modal-close-area modal-close-df">
                                            <a style="padding-top: 1px;" class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                                        </div>
                                        <div class="modal-title pull-right"><h3>Chỉnh sửa thông tin</h3></div>
                                      </div>
                                      <div class="modal-body">
                                        <input type="hidden" id="id" value="{{$donhang->id}}">
                                        <p><input type="text" placeholder="Text" id="hoten" class="form-control" value="{{$donhang->hoten}}"></p>
                                        <p><input type="text" placeholder="Text" id="email" class="form-control" value="{{$donhang->email}}"></p>
                                        <p><input type="number" placeholder="Text" id="sodt" class="form-control" value="+84 {{$donhang->sodt}}"></p>
                                        <p><input type="text" placeholder="Text" id="diachi" class="form-control" value="{{$donhang->diachi}}"></p>
                                        <p><div class="form-select-list">
                                            <input type="hidden" id="idprovince1" value="{{$donhang->province}}">
                                            <select class="form-control custom-select-value" id="province">
                                            </select>
                                        </div></p>
                                        <p><div class="form-select-list">
                                            <input type="hidden" id="iddistrict1" value="{{$donhang->district}}">
                                            <select class="form-control custom-select-value" id="district">
                                            </select>
                                        </div></p>
                                        <p><div class="form-select-list">
                                            <input type="hidden" id="idward1" value="{{$donhang->ward}}">
                                            <select class="form-control custom-select-value" id="ward">
                                            </select>
                                        </div></p>
                                        <p><textarea id="ghichu" class="form-control">{{$donhang->ghichu}}</textarea></p>
                                      </div>
                                      <div class="modal-footer notification-bt responsive-btn">
                                        <button type="button" class="btn btnclose" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btnsave " data-dismiss="modal" id="SaveButton">Save </button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body blog-pra" >
                            <div id="items">
                                <h4>Họ tên: {{$donhang->hoten}}</h4>
                                <br>
                                <p style="padding-bottom: 12px;font-size: 15px">Email: {{$donhang->email}}</p>
                                <p style="padding-bottom: 12px;font-size: 15px">Phone: +84 {{$donhang->sodt}}</p>
                                <p style="padding-bottom: 12px;font-size: 15px">Đia chỉ: {{$donhang->diachi}}
                                <br>&emsp;&emsp;&emsp;&ensp;<?php echo getWard($donhang->ward); ?> <?php echo getDistrict($donhang->district); ?>
                                <br>&emsp;&emsp;&emsp;&ensp;<?php echo getProvince($donhang->province); ?></p>
                                <p style="padding-bottom: 12px;font-size: 15px">Ghi chú: {{$donhang->ghichu}}</p>
                            </div>
                        </div>
                        <div class="panel-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{csrf_field()}}
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
$(document).ready(function(){
    var url = "../../../api/province";
    $.getJSON(url,function(result){
        $.each(result, function(i, field){
            var idprovince1  = $("#idprovince1").val();
            var id   = field.provinceid;
            var name = field.name;
            var type = field.type;
            var select = "";
            if(id==idprovince1)
            {
                select = 'selected=""';
            }
            $("#province").append('<option value="'+id+'" '+select+'>'+type+" "+name+'</option>');
        });
    });
    var idprovince1 = $("#idprovince1").val();
    var url = "../../../api/district/"+idprovince1;
    $.getJSON(url, function(result){
        console.log(result);
        $.each(result, function(i, field){
            var iddistrict1  = $("#iddistrict1").val();
            var id   = field.districtid;
            var name = field.name;
            var type = field.type;
            var select = "";
            if(id==iddistrict1)
              {
                select = 'selected=""';
              }
            $("#district").append('<option value="'+id+'" '+select+'>'+type+" "+name+'</option>');
        });
    });
    var iddistrict1 = $("#iddistrict1").val();
        var url = "../../../api/ward/"+iddistrict1;
        $.getJSON(url, function(result){
            console.log(result);
            $.each(result, function(i, field){
                var idward1  = $("#idward1").val();
                var id   = field.wardid;
                var name = field.name;
                var type = field.type;
                var select = "";
                if(id==idward1)
                  {
                    select = 'selected=""'
                  }
                $("#ward").append('<option value="'+id+'" '+select+'>'+type+" "+name+'</option>');
        });
    });
    $("#province").change(function(){
        $("#district").html('<option value="-1">---- Lựa chọn Quận/ Huyện ----</option>');
        var idprovince = $("#province").val();
        var url = "../../../api/district/"+idprovince;
        $.getJSON(url, function(result){
            console.log(result);
            $.each(result, function(i, field){
                var id   = field.districtid;
                var name = field.name;
                var type = field.type;
                $("#district").append('<option value="'+id+'">'+type+" "+name+'</option>');
            });
        });
    });
    $("#district").change(function(){
        $("#ward").html('<option value="-1">---- Lựa chọn Phường/ Xã ----</option>');
        var iddistrict = $("#district").val();
        var url = "../../../api/ward/"+iddistrict;
        $.getJSON(url, function(result){
            console.log(result);
            $.each(result, function(i, field){
                var idward1  = $("#idward1").val();
                var id   = field.wardid;
                var name = field.name;
                var type = field.type;
                var select = "";
                $("#ward").append('<option value="'+id+'" '+select+'>'+type+" "+name+'</option>');
            });
        });
    });
    $('#SaveButton').click(function (even){
        var id       = $("#id").val();
        var hoten    = $("#hoten").val();
        var email    = $("#email").val();
        var sodt     = $("#sodt").val();
        var diachi   = $("#diachi").val();
        var province = $("#province").val();
        var district = $("#district").val();
        var ward     = $("#ward").val();
        var ghichu   = $("#ghichu").val();
        function UserError(){
            this.throwTrong = function(msg){
                Lobibox.notify('warning', {
                    position: 'top right',
                    msg: msg
                });
            };
            return this;
        }
        try{
            if(hoten=="" || email=="" ||sodt=="" ||diachi=="" ){            
                var UserError = new UserError();
                var msg = "Vui lòng nhập đầy đủ thông tin";
                UserError.throwTrong(msg);  
            
            }
            else if (sodt.length <= 9 || sodt.length >=12)
            {
                var UserError = new UserError();
                var msg = "Vui lòng nhập đúng số điện thoại.";
                UserError.throwTrong(msg);
            }
            else{
                $.post('../../../admin/updatedata', {'_token':$('input[name=_token]').val(),
                                                'id'        : id,
                                                'hoten'     : hoten,
                                                'email'     : email,
                                                'sodt'      : sodt,
                                                'diachi'    : diachi,
                                                'province'  : province,
                                                'district'  : district,
                                                'ward'      : ward,
                                                'ghichu'    : ghichu
                                            },
                function(data){
                $('#items').load(' ' +'#items');
                console.dir(data);
                });
            }
        } catch (e){
            console.log(e.message);
        }
///////////end
    });
});
</script>
<!-- <script
  src="http://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script> -->
@endsection