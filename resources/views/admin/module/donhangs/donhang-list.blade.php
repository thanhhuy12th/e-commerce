@extends('admin.welcome')

@section('breadcome')
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-top: -23px;">
                            <ul class="breadcome-menu">
                                <li><a href="{{route('index')}}">{{trans('message.trangchu')}}</a> <span class="bread-slash">/</span>
                                </li>
                                <li><span class="bread-blod">{{trans('message.danhsach')}}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<style type="text/css">
    .chuatt{
        background: -webkit-linear-gradient(178deg, #FDE06B  0%, #FDE06B  100%);
        background: linear-gradient(178deg, #FDE06B  0%, #FDE06B  100%);
        border-radius: 8px;
        padding: 7px 5px 7px 5px;
    }
</style>
<div class="product-status mg-tb-15">
    @include('admin.blocks.alert')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="product-status-wrap">
                    <h4>{{trans('message.danhsach_donhang')}}</h4>
                    <div class="add-product">
                        <a href="{{ route('donhang.create') }}">{{trans('message.btn_them')}}</a>
                    </div>
                    <form action="" name="listForm" id="listForm" method="post" >
                        {!! csrf_field() !!}
                    <table>
                        <tr>
                            <td>
                                <input type="checkbox" name="checkAll" onClick="checkallClick(checkAll);">
                            </td>
                            <th>Đơn hàng</th>
                            <th>Ngày đặt</th>
                            <th>Khách hàng</th>
                            <th>Thanh toán</th>
                            <th>Giao hàng</th>
                            <th>Tổng tiền</th>
                        </tr>
                        @foreach($donhang as $item)
                        <tr style="vertical-align: top">
                            
                            <td style="width: 4%">
                                <input value="{{ $item->id }}" type="checkbox" name="checked[]">         
                            </td>
                            <td style="width: 12%">
                                <a href="{{ route('donhang.edit', ['donhang' => $item->id]) }}">#{{$item->id}}</a></td>
                            <td style="width: 18%">
                                <?php
                                    $date = date_create($item->created_at);
                                    echo date_format($date, 'd-m-Y H:i');
                                ?>
                            </td>
                            <td style="width: 18%">{{$item->hoten}}</td>
                            <td style="width: 16%">
                                @if($item->status_paid == 0)
                                <span class="chuatt">Chưa thanh toán</span>
                                @else
                                <span style="font-weight: bold;">Đã thanh toán</span>
                                @endif
                            </td>
                            <td style="width: 16%">
                                @if($item->status_move == 0)
                                <span class="chuatt">Chưa giao hàng</span>
                                @else
                                <span style="font-weight: bold;">Đã giao hàng</span>
                                @endif
                            </td>
                            <td style="width: 16%">
                                <?php echo convert_money($item->tongtien)." VNĐ";?>
                            </td>                 
                        </tr>
                        @endforeach
                    </table>
                    {{ method_field('DELETE') }}
                    <button onClick="return xacnhanxoa('Bạn có chắc chắn muốn xóa ?');" id="btnXoaList" name="btnXoaList" type="submit" data-toggle="tooltip" title="Xóa" class="pd-setting-ed-delete">
                        <i class="fa fa-trash-o" aria-hidden="true"></i> {{trans('message.btn_xoa')}}
                    </button>
                    </form>
                     {{$donhang->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection