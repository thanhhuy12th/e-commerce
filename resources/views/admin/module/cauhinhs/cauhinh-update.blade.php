@extends('admin.welcome')
@section('breadcome')
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-top: -23px;">
                            <ul class="breadcome-menu">
                                <li><a href="{{route('index')}}">{{trans('message.trangchu')}}</a>
                                    <span class="bread-slash">/</span>
                                </li>
                                <li><a href="{{ route('cauhinh.editNoId') }}">{{trans('message.cauhinh')}}</a>
                                    <span class="bread-blod">/</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<style type="text/css">
    .edit{
        /*background: #EEECEC;*/
        padding: 10px;
        font-size: 26px;
        font-weight: bold;
        /*border-radius: 5px;*/
    }
    .edit2{
        font-size: 16px;
        height: auto;
    }
</style>
<form action="{{ route('cauhinh.updateNoId') }}" method="POST" enctype="multipart/form-data">
    @csrf
    {{ method_field('PUT') }}
<div class="single-product-tab-area mg-tb-15">
    @include('admin.blocks.alert')
    <!-- Single pro tab review Start-->
    <div class="single-pro-review-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="review-tab-pro-inner">
                        <div class="color-line"></div>
                        <span class="edit">{{trans('message.cauhinh')}}</span>
                        <div style="margin-bottom: 20px" class="color-line"></div>
                        <ul id="myTab3" class="tab-review-design">
                            <li class="active"><a style="font-weight: normal;" href="#khac"><i class="fa fa-pencil" aria-hidden="true"> </i>Khác</a></li>
                            <li><a style="font-weight: normal;" href="#lienhe">     <i class="fa fa-file-image-o" aria-hidden="true">   </i>Liên hệ</a></li>
                            <li><a style="font-weight: normal;" href="#gioithieu">  <i class="fa fa-file-image-o" aria-hidden="true">   </i>Giới thiệu</a></li>
                            <li><a style="font-weight: normal;" href="#footer">     <i class="fa fa-file-image-o" aria-hidden="true">   </i>Footer</a></li>
                            <li><a style="font-weight: normal;" href="#mangxahoi">  <i class="fa fa-file-image-o" aria-hidden="true">   </i>Mạng xã hội</a></li>
                            <li><a style="font-weight: normal;" href="#khachhang">  <i class="fa fa-file-image-o" aria-hidden="true">   </i>Khách hàng</a></li>
                            <li><a style="font-weight: normal;" href="#emailgmail">      <i class="fa fa-file-image-o" aria-hidden="true">   </i>Email</a></li>
                            <li><a style="font-weight: normal;" href="#logo">       <i class="fa fa-file-image-o" aria-hidden="true">   </i>Logo</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content custom-product-edit">
                            <div class="product-tab-list tab-pane fade active in" id="khac">
                                <div class="row">
                                        <div class="review-content-section">
                                            <div  class="input-group mg-b-pro-edt">
                                                <span class="edit2 input-group-addon">Email</span>
                                                <input value="{{old('email',$cauhinh[0]->chitiet)}}" type="text" name="email" class="edit2 form-control" placeholder="Viết email">
                                            </div>
                                            <div  class="input-group mg-b-pro-edt">
                                                <span class="edit2 input-group-addon">Địa chỉ</span>
                                                <input value="{{old('title',$cauhinh[1]->chitiet)}}" type="text" name="title" class="edit2 form-control" placeholder="Viết địa chỉ ">
                                            </div>
                                            <div class="input-group mg-b-pro-edt">
                                                <span class="edit2 input-group-addon">Điện thoại</span>
                                                <input value="{{old('dienthoai',$cauhinh[2]->chitiet)}}" type="text" name="dienthoai" class="edit2 form-control" placeholder="Viết điện thoại ">
                                            </div>
                                            <div class="input-group mg-b-pro-edt">
                                                <span class="edit2 input-group-addon">Tên website</span>
                                                <input value="{{old('tenwebsite',$cauhinh[3]->chitiet)}}" type="text" name="tenwebsite" class="edit2 form-control" placeholder="Viết tên website">
                                            </div>
                                            <div class="input-group mg-b-pro-edt">
                                                <span class="edit2 input-group-addon">Link website</span>
                                                <input value="{{old('linkwebsite',$cauhinh[4]->chitiet)}}" type="text" name="linkwebsite" class="edit2 form-control" placeholder="Nhập link">
                                            </div>
                                            <div class="input-group mg-b-pro-edt">
                                                <span class="edit2 input-group-addon">Link SEO</span>
                                                <input value="{{old('keyword',$cauhinh[15]->chitiet)}}" type="text" name="keyword" class="edit2 form-control" placeholder="Nhập link">
                                            </div>
                                            <div  class="input-group mg-b-pro-edt">
                                                <span class="edit2 input-group-addon">Google Map</span>
                                                <textarea rows="3" name="googlemap" class="edit2 form-control" placeholder="Viết code googlemap">{{old('googlemap',$cauhinh[5]->chitiet)}}</textarea>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="product-tab-list tab-pane fade" id="lienhe">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="review-content-section">
                                            <textarea rows="10" id="editor1" name="lienhe" class="form-control col-md-12 col-xs-12" >{{$cauhinh[6]->chitiet}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-tab-list tab-pane fade" id="gioithieu">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="review-content-section">
                                            <textarea rows="10" id="editor2" name="gioithieu" class="form-control col-md-12 col-xs-12" >{{$cauhinh[7]->chitiet}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-tab-list tab-pane fade" id="footer">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="review-content-section">
                                            <textarea rows="10" id="editor3" name="footer" class="form-control col-md-12 col-xs-12" >{{$cauhinh[8]->chitiet}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-tab-list tab-pane fade " id="mangxahoi">
                                <div class="row">
                                        <div class="review-content-section">
                                            <div  class="input-group mg-b-pro-edt">
                                                <span class="edit2 input-group-addon">FaceBook</span>
                                                <input value="{{old('facebook',$cauhinh[9]->chitiet)}}" type="text" name="facebook" class="edit2 form-control" placeholder="Link">
                                            </div>
                                            <div  class="input-group mg-b-pro-edt">
                                                <span class="edit2 input-group-addon">Youtube</span>
                                                <input value="{{old('youtube',$cauhinh[10]->chitiet)}}" type="text" name="youtube" class="edit2 form-control" placeholder="Link">
                                            </div>
                                            <div class="input-group mg-b-pro-edt">
                                                <span class="edit2 input-group-addon">Instagram</span>
                                                <input value="{{old('instagram',$cauhinh[11]->chitiet)}}" type="text" name="instagram" class="edit2 form-control" placeholder="Link">
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="product-tab-list tab-pane fade " id="khachhang">
                                <div class="row">
                                    <div class="review-content-section">
                                    <p>Nhập số tiền mà khách hàng phải sử dụng nếu muốn trở thành khách hàng thân thiết</p>
                                        <div  class="input-group mg-b-pro-edt">
                                            <span class="edit2 input-group-addon">Số tiền</span>
                                            <input value="{{old('vipmoney',$cauhinh[12]->chitiet)}}" type="text" name="vipmoney" class="edit2 form-control" placeholder="Link">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-tab-list tab-pane fade " id="emailgmail">
                                <div class="row">
                                    <div class="review-content-section">
                                        <div  class="input-group mg-b-pro-edt">
                                            <span class="edit2 input-group-addon">Email</span>
                                            <input value="{{old('email_gmail',$cauhinh[13]->chitiet)}}" type="text" name="email_gmail" class="edit2 form-control" placeholder="Link">
                                        </div>
                                        <div  class="input-group mg-b-pro-edt">
                                            <span class="edit2 input-group-addon">Password</span>
                                            <input value="{{old('password_gmail',$cauhinh[14]->chitiet)}}" type="text" name="password_gmail" class="edit2 form-control" placeholder="Link">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-tab-list tab-pane fade" id="logo">
                                <div class="row">
                                    <div style="text-align: center;margin-top: 15px;margin-bottom: 10px">
                                        <img style="width: 275px;height: 275px" src="{{ asset('image/'.$cauhinh[16]->chitiet) }}" alt="">
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="review-content-section">
                                            <div class="input-group mg-b-pro-edt">
                                                <input class="hide" type="text" name="hinhcu" value="{{$cauhinh[16]->chitiet}}">
                                                <span class="edit2 input-group-addon ">Hình ảnh đại diện</span>
                                                <input type="file" name="photos" class="edit2 form-control btn btn-primary  ">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="padding-top: 30px" class="mg-b-pro-edt custom-pro-edt-ds">
                            <button name="addProduct" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                            </button>
                            <button type="button" class="btn btn-warning waves-effect waves-light">Discard
                            </button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
@endsection