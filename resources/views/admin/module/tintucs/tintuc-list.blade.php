@extends('admin.welcome')

@section('breadcome')
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-top: -23px;">
                            <ul class="breadcome-menu">
                                <li><a href="{{route('index')}}">{{trans('message.trangchu')}}</a> <span class="bread-slash">/</span>
                                </li>
                                <li><span class="bread-blod">{{trans('message.danhsach')}}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="product-status mg-tb-15">
    @include('admin.blocks.alert')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="product-status-wrap">
                    <h4>{{trans('message.danhsach_tintuc')}}</h4>
                    <div class="add-product">
                        <a href="{{ route('tintuc.create') }}">{{trans('message.btn_them')}}</a>
                    </div>
                    <form action="" name="listForm" id="listForm" method="post" >
                        {!! csrf_field() !!}
                    <table>
                        <tr>
                            <td>
                                <input type="checkbox" name="checkAll" onClick="checkallClick(checkAll);">
                            </td>
                            <th>Mã</th>
                            <th>Tiêu đề</th>
                            <th>Thông tin</th>
                            <th>Thể loại</th>
                            <th>Chọn</th>
                        </tr>
                        @foreach($tintuc as $item)
                        <tr style="vertical-align: top">
                            <td >
                                <input value="{{ $item->id }}" type="checkbox" name="checked[]">         
                            </td>
                            <td style="width: 5%">{{$item->id}}</td>
                            <td style="width: 25%">{{$item->title}}</td>
                            <td style="width: 45%">{{$item->intro}}</td>
                            <td style="width: 15%">
                                    @foreach($dmtt as $ctlg)
                                        @if($ctlg->id == $item->category)
                                         <i style="font-style:normal;" data-toggle="tooltip" title="{{$item->category}}">{{$ctlg->name}}</i>
                                        @endif
                                    @endforeach
                            </td>
                            <td style="width: 10%">
                                <a href="{{ route('tintuc.edit', ['tintuc' => $item->id]) }}" data-toggle="tooltip" title="Sửa" class="pd-setting-ed">
                                    <i class="fa fa-pencil-square-o clear" aria-hidden="true">{{trans('message.btn_sua')}}</i>
                                </a>
                            </td>                   
                        </tr>
                        @endforeach
                    </table>
                    {{ method_field('DELETE') }}
                    <button onClick="return xacnhanxoa('Bạn có chắc chắn muốn xóa ?');" id="btnXoaList" name="btnXoaList" type="submit" data-toggle="tooltip" title="Xóa" class="pd-setting-ed-delete">
                        <i class="fa fa-trash-o" aria-hidden="true"></i> {{trans('message.btn_xoa')}}
                    </button>
                    </form>
                     {{$tintuc->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection