@extends('admin.welcome')

@section('breadcome')
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-top: -23px;">
                            <ul class="breadcome-menu">
                                <li><a href="{{route('index')}}">{{trans('message.trangchu')}}</a>
                                    <span class="bread-slash">/</span>
                                </li>
                                <li><a href="{{ route('slide.index') }}">{{trans('message.danhsach')}}</a>
                                    <span class="bread-blod">/</span>
                                </li>
                                <li><span class="bread-blod">{{trans('message.btn_them')}}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<style type="text/css">
    .edit{
        /*background: #EEECEC;*/
        padding: 10px;
        font-size: 26px;
        font-weight: bold;
        /*border-radius: 5px;*/
    }
</style>
<form action="{{ route('slide.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
<div class="single-product-tab-area mg-tb-15">
    @include('admin.blocks.alert')
    <!-- Single pro tab review Start-->
    <div class="single-pro-review-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="review-tab-pro-inner">
                        <div class="color-line"></div>
                        <span class="edit">{{trans('message.btn_them')}}</span>
                        <div class="color-line"></div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="review-content-section">
                                        <div class="input-group mg-b-pro-edt">
                                            <span class="input-group-addon">Tiêu đề</span>
                                            <input value="{{old('tieude')}}" type="text" name="tieude" class="form-control" placeholder="Viết tiêu đề ">
                                        </div>
                                        <div class="input-group mg-b-pro-edt">
                                            <span class="input-group-addon">Links</span>
                                            <input value="{{old('link')}}" type="text" name="link" class="form-control" placeholder="Links">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="review-content-section">
                                        <div class="input-group mg-b-pro-edt">
                                            <span class="input-group-addon">Vị trí</span>
                                            <input value="{{old('vitri')}}" type="text" name="vitri" class="form-control" placeholder="Vị trí">
                                            </div>
                                        <div class="input-group mg-b-pro-edt">
                                            <span class="input-group-addon ">Hình ảnh</span>
                                            <input type="file" name="photos" class="form-control btn btn-primary  ">                                       
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div style="margin-top: 45px" class="color-line"></div>
                        <span class="edit">Nội Dung</span>
                        <div class="color-line"></div>
                        <ul id="myTab3" class="tab-review-design">
                            <li class="active"><a style="font-weight: normal;" href="#description"><i class="fa fa-pencil" aria-hidden="true"></i>Nội dung chi tiết</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content custom-product-edit">
                            <div class="product-tab-list tab-pane fade active in" id="description">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="review-content-section">
                                            <textarea rows="10" id="editor1" name="chitiet" placeholder="Type the content here!"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="padding-top: 30px" class="mg-b-pro-edt custom-pro-edt-ds">
                            <button name="addProduct" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                            </button>
                            <button type="button" class="btn btn-warning waves-effect waves-light">Discard
                            </button></div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>
</form>
@endsection