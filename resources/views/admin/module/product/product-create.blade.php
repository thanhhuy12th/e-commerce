@extends('admin.welcome')

@section('breadcome')
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-top: -23px;">
                            <ul class="breadcome-menu">
                                <li><a href="{{route('index')}}">{{trans('message.trangchu')}}</a>
                                    <span class="bread-slash">/</span>
                                </li>
                                <li><a href="{{ route('sanpham.index') }}">{{trans('message.danhsach')}}</a>
                                    <span class="bread-blod">/</span>
                                </li>
                                <li><span class="bread-blod">{{trans('message.btn_them')}}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<style type="text/css">
    .edit{
        /*background: #EEECEC;*/
        padding: 10px;
        font-size: 26px;
        font-weight: bold;
        /*border-radius: 5px;*/
    }
</style>
<form action="{{ route('sanpham.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
<div class="single-product-tab-area mg-tb-15">
    @include('admin.blocks.alert')
    <!-- Single pro tab review Start-->
    <div class="single-pro-review-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="review-tab-pro-inner">
                        <div class="color-line"></div>
                        <span class="edit">{{trans('message.btn_them')}}</span>
                        <div class="color-line"></div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="review-content-section">
                                            <div class="input-group mg-b-pro-edt">
                                                <span class="input-group-addon">Tên sản phẩm</span>
                                                <input value="{{old('tensp')}}" type="text" name="tensp" class="form-control" placeholder="Tên sản phẩm">
                                            </div>
                                            <div class="input-group mg-b-pro-edt">
                                                <span class="input-group-addon">Giá ban đầu</span>
                                                <input value="{{old('price')}}" type="text" name="price" class="form-control" placeholder="Giá ban đầu">
                                            </div>
                                            <div class="input-group mg-b-pro-edt">
                                                <span class="input-group-addon">Danh mục</span>
                                                <select name="category" class="form-control pro-edt-select form-control-primary">
                                                     <option value="">Hãy chọn thể loại</option>
                                                    @foreach($dmsp as $ctlg)
                                                    @if($ctlg->id_parent >= 1)
                                                     <option value="{{ $ctlg->id }}" {{ (old('category') == $ctlg->id) ? 'selected' : '' }}>{{$ctlg->name}}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="input-group mg-b-pro-edt">
                                                <span class="input-group-addon">Alias</span>
                                                <input value="{{old('alias')}}" type="text" name="alias" class="form-control" placeholder="Alias">
                                            </div>
                                             <div class="input-group mg-b-pro-edt">
                                                <span class="input-group-addon">Loại giảm giá</span>
                                                <select name="type_giamgia" class="form-control">
                                                    <option value="0">Giảm theo giá</option>
                                                    <option value="1">Giảm theo sản phẩm</option>
                                                </select>
                                            </div>
                                        </div>
                                </div>
                               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="review-content-section">
                                        <div class="input-group mg-b-pro-edt images-cropper-pro ">
                                            <span class="input-group-addon ">Hình ảnh đại diện</span>
                                            <input type="file" name="photos[]" multiple="" class="form-control btn btn-primary  ">                                       
                                        </div>
                                        <div class="input-group mg-b-pro-edt">
                                            <span class="input-group-addon">Giá khuyến mãi</span>
                                            <input value="{{old('price_km')}}" type="text" name="price_km" class="form-control" placeholder="Giá khuyến mãi">
                                        </div>
                                        {{-- <div class="input-group mg-b-pro-edt">
                                            <span class="input-group-addon">Số lượng</span>
                                            <input value="{{old('soluong')}}" type="text" name="soluong" class="form-control" placeholder="Số lượng">
                                        </div> --}}
                                        <div class="input-group mg-b-pro-edt">
                                            <span class="input-group-addon">Mã giảm giá</span>
                                            <select name="giamgia" class="form-control pro-edt-select form-control-primary">
                                                <option value="">Không chọn mã giảm giá</option>
                                                @foreach($coupon as $cp)
                                                    <option value="{{ $cp->id }}">{{$cp->id}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="input-group mg-b-pro-edt">
                                            <span class="input-group-addon">
                                                <input type="radio" name="type_product" value="0"  checked=""> Bình thường <br/>
                                            </span>
                                            <span class="input-group-addon">
                                                <input type="radio" name="type_product" value="1"  > Bán chạy <br/>
                                            </span>
                                            <span class="input-group-addon">
                                                <input type="radio" name="type_product" value="2" > Bán nhiều<br/>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div style="margin-top: 45px" class="color-line"></div>
                        <span class="edit">Nội Dung</span>
                        <div class="color-line"></div>
                        <ul id="myTab3" class="tab-review-design">
                            <li class="active"><a style="font-weight: normal;" href="#description"><i class="fa fa-pencil" aria-hidden="true"></i>Mô tả ngắn</a></li>
                            <li><a style="font-weight: normal;" href="#reviews"><i class="fa fa-file-image-o" aria-hidden="true"></i>Chi tiết</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content custom-product-edit">
                            <div class="product-tab-list tab-pane fade active in" id="description">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="review-content-section">
                                            <textarea rows="10" id="editor1" name="chitietngan" placeholder="Type the content here!" class="form-control col-md-12 col-xs-12" >{{old('chitietngan')}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-tab-list tab-pane fade" id="reviews">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="review-content-section">
                                            <textarea rows="10" id="editor2" name="information" placeholder="Type the content here!" class="form-control col-md-12 col-xs-12" >{{old('information')}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="padding-top: 30px" class="mg-b-pro-edt custom-pro-edt-ds">
                            <button name="addProduct" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                            </button>
                            <button type="button" class="btn btn-warning waves-effect waves-light">Discard
                            </button>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>
</form>
@endsection