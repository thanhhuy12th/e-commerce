@extends('admin.welcome')

@section('breadcome')
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-top: -23px;">
                            <ul class="breadcome-menu">
                                <li><a href="{{ route('index') }}">{{trans('message.trangchu')}}</a> <span class="bread-slash">/</span>
                                </li>
                                <li><span class="bread-blod">{{trans('message.danhsach')}}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="product-status mg-tb-15">
    @include('admin.blocks.alert')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="product-status-wrap">
                    <h4>{{trans('message.danhsach_admin')}}</h4>
                    <div class="add-product">
                        <a href="{{ route('useradmin.create') }}">{{trans('message.btn_them')}}</a>
                    </div>
                    <form action="" name="listForm" id="listForm" method="post" >
                        {!! csrf_field() !!}
                    <table>
                        <tr>
                            <th>
                                <input type="checkbox" name="checkAll" onClick="checkallClick(checkAll);">
                            </th>
                            <th>Mã</th>
                            <th>Họ tên</th>
                            <th>Username</th>
                            <th>Quyền</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Hoạt động</th>
                            <th>Chọn</th>
                        </tr>
                        @foreach($useradmin as $item)
                        <tr>
                            <td >
                                <input value="{{ $item->id }}" type="checkbox" name="checked[]">         
                            </td>
                            <td style="width: 5%">{{$item->id}}</td>
                            <td style="width: 12%">{{$item->fullname}}</td>
                            <td style="width: 13%">{{$item->username}}</td>
                            <td style="width: 15% ">
                                @if($item->rule==0)
                                    Admin
                                @elseif($item->rule==1)
                                    Admin post
                                @elseif($item->rule==2)
                                    Admin product
                                @elseif($item->rule==3)
                                    Admin cấp 3
                                @endif
                            </td>
                            <td style="width: 10%">{{$item->phone}}</td>
                            <td style="width: 25%">{{$item->email}}</td>
                            <td style="width: 10%">
                                    {{convertdate1($item->created_at)}}
                            </td>
                            <td style="width: 10%">
                                <a href="{{ route('useradmin.edit', ['useradmin' => $item->id]) }}" data-toggle="tooltip" title="Sửa" class="pd-setting-ed">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"> {{trans('message.btn_sua')}}</i>
                                </a>
                            </td>                   
                        </tr>
                        @endforeach
                    </table>
                    {{ method_field('DELETE') }}
                    <button onClick="return xacnhanxoa('Bạn có chắc chắn muốn xóa ?');" id="btnXoaList" name="btnXoaList" type="submit" data-toggle="tooltip" title="Xóa" class="pd-setting-ed-delete">
                        <i class="fa fa-trash-o" aria-hidden="true"></i> {{trans('message.btn_xoa')}}
                    </button>
                    </form>
                     {{$useradmin->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection