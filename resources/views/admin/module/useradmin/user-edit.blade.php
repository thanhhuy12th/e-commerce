@extends('admin.welcome')

@section('breadcome')
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-top: -23px;">
                            <ul class="breadcome-menu">
                                <li><a href="{{ route('index') }}">{{trans('message.trangchu')}}</a>
                                    <span class="bread-slash">/</span>
                                </li>
                                <li><a href="{{ route('useradmin.index') }}">{{trans('message.danhsach')}}</a>
                                    <span class="bread-blod">/</span>
                                </li>
                                <li><span class="bread-blod">{{trans('message.btn_sua')}}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')


<div class="single-product-tab-area mg-tb-15">
    @include('admin.blocks.alert')
    <!-- Single pro tab review Start-->
    <div class="single-pro-review-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="review-tab-pro-inner">
                        <ul id="myTab3" class="tab-review-design">
                            <li class="active"><a href="#description"><i class="fa fa-pencil" aria-hidden="true"></i> Thông Tin Chi Tiết</a></li>                            
                        </ul>
                         <form action="{{route('useradmin.update', ['sanpham' => $useradmin->id]) }}" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                        <div id="myTabContent" class="tab-content custom-product-edit">
                            <div class="product-tab-list tab-pane fade active in" id="description">
                                <div class="row">
                                    <div class="font col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="review-content-section">
                                            <div class="mg-b-pro-edt">
                                                <label class="lable_edit">Họ tên *</label>
                                                <input value="{{old('fullname',$useradmin->fullname)}}" type="text" name="fullname" class="form-control" >
                                            </div>
                                            <div class="mg-b-pro-edt">
                                                <label class="lable_edit">Số điện thoại *</label>
                                                <input value="{{old('phone',$useradmin->phone)}}" type="text" name="phone" class="form-control"  >
                                            </div>
                                            <div class="mg-b-pro-edt">
                                                 <label class="lable_edit">Email *</label>
                                                <input value="{{old('email',$useradmin->email)}}" type="text" name="email" class="form-control" >
                                            </div>
                                            <div class="mg-b-pro-edt">
                                                <label class="lable_edit">Quyền *</label>
                                                <select name="rule" class="form-control pro-edt-select form-control-primary">
                                                    <?php
                                                            $sele = "";
                                                            if($useradmin->rule=="0")
                                                            { ?>
                                                                <option value='0' selected="">Admin</option>
                                                                <option value='1'>Admin post</option>
                                                                <option value='2'>Admin product</option>
                                                                <option value='3'>Admin cấp 3</option>
                                                            <?php }
                                                            else if($useradmin->rule=="1")
                                                            { ?>
                                                                <option value='0'>Admin</option>
                                                                <option value='1' selected="">Admin post</option>
                                                                <option value='2'>Admin product</option>
                                                                <option value='3'>Admin cấp 3</option>
                                                            <?php }
                                                            else if($useradmin->rule=="2")
                                                            { ?>
                                                                <option value='0'>Admin</option>
                                                                <option value='1'>Admin post</option>
                                                                <option value='2' selected="">Admin product</option>
                                                                <option value='3'>Admin cấp 3</option>
                                                            <?php }
                                                            else if($useradmin->rule=="3")
                                                            { ?>
                                                                <option value='0'>Admin</option>
                                                                <option value='1'>Admin post</option>
                                                                <option value='2'>Admin product</option>
                                                                <option value='3' selected="">Admin cấp 3</option>
                                                            <?php }
                                                        ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="text-center mg-b-pro-edt custom-pro-edt-ds">
                                            {{ method_field('PUT') }}
                                            <button name="addProduct" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                                                </button>
                                            <button type="button" class="btn btn-warning waves-effect waves-light">Discard
                                                </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection