@extends('admin.welcome')

@section('breadcome')
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-top: -23px;">
                            <ul class="breadcome-menu">
                                <li><a href="{{route('index')}}">{{trans('message.trangchu')}}</a>
                                    <span class="bread-slash">/</span>
                                </li>
                                <li><a href="{{ route('member.index') }}">{{trans('message.danhsach')}}</a>
                                    <span class="bread-blod">/</span>
                                </li>
                                <li><span class="bread-blod">{{trans('message.btn_them')}}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<style type="text/css">
 .lable_edit{
    font-weight: normal;
 }
 .font{
    font-size: 16px;
 }
</style>
<form action="{{ route('member.store') }}" method="POST" enctype="multipart/form-data">
    @csrf

<div class="single-product-tab-area mg-tb-15">
    @include('admin.blocks.alert')
    <!-- Single pro tab review Start-->
    <div class="single-pro-review-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="review-tab-pro-inner">
                        <ul id="myTab3" class="tab-review-design">
                            <li class="active"><a href="#description"><i class="fa fa-pencil" aria-hidden="true"></i> Thông Tin Tài Khoản</a></li>
                        <div id="myTabContent" class="tab-content custom-product-edit">
                            <div class="product-tab-list tab-pane fade active in" id="description">
                                <div class="row">
                                    <div class="font col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="review-content-section">
                                            <div class="mg-b-pro-edt">
                                                <label class="lable_edit">Họ tên *</label>
                                                <input value="{{old('fullname')}}" type="text" name="fullname" class="form-control" >
                                            </div>
                                            <div class="mg-b-pro-edt">
                                                <label class="lable_edit">Số điện thoại *</label>
                                                <input value="{{old('phone')}}" type="text" name="phone" class="form-control"  >
                                            </div>
                                            <div class="mg-b-pro-edt">
                                                <label class="lable_edit">Email *</label>
                                                <input value="{{old('email')}}" type="text" name="email" class="form-control" >
                                            </div>
                                            <div class="form-group">
                                                <label class="lable_edit" for="diachi">Địa chỉ nhận hàng</label>
                                                <input value="{{old('diachi')}}" name="diachi" type="text" placeholder="Địa chỉ" class="form-control">
                                            <span class="help-block small"></span>
                                        </div>
                                        <div class="form-group">
                                            <label class="lable_edit" for="province">Tỉnh/ Thành phố</a></label>
                                            <select class="form-control" id="province" name="province">
                                                <option value="-1">Vui lòng chọn Tỉnh/ Thành phố</option>
                                            </select>
                                            <span class="help-block small"></span>
                                        </div>
                                        <div class="form-group">
                                            <label class="lable_edit" for="district">Quận/ Huyện</a></label>
                                            <select class="form-control" id="district" name="district">
                                                <option value="-1">Vui lòng chọn Quận/ Huyện</option>
                                            </select>
                                            <span class="help-block small"></span>
                                        </div>
                                        <div class="form-group">
                                            <label class="lable_edit" for="ward">Phường/ Xã</a></label>
                                            <select class="form-control" id="ward" name="ward">
                                                <option value="-1">Vui lòng chọn Phường/ Xã</option>
                                            </select>
                                            <span class="help-block small"></span>
                                        </div>
                                        <div class="mg-b-pro-edt">
                                            <label class="lable_edit">Tên đăng nhập *</label>
                                            <input value="{{old('username')}}" type="text" name="username" class="form-control">
                                        </div>
                                        <div class="mg-b-pro-edt">
                                            <label class="lable_edit">Mật khẩu *</label>
                                            <input value="{{old('password')}}" type="password" name="password" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="mg-b-pro-edt custom-pro-edt-ds">
                                            <button name="button" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
												</button>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
$(document).ready(function(){
  var url = "../../api/province";
  $.getJSON(url,function(result){
    $.each(result, function(i, field){
      var id   = field.provinceid;
      var name = field.name;
      var type = field.type;
      $("#province").append('<option value="'+id+'">'+type+" "+name+'</option>');
    });
  });


$("#province").change(function(){
    $("#district").html('<option value="-1">Vui lòng chọn Quận/ Huyện</option>');
        var idprovince = $("#province").val();
        var url = "../../api/district/"+idprovince;
        $.getJSON(url, function(result){
            console.log(result);
            $.each(result, function(i, field){
                var id   = field.districtid;
                var name = field.name;
                var type = field.type;
                $("#district").append('<option value="'+id+'">'+type+" "+name+'</option>');
        });
    });
});

$("#district").change(function(){
    $("#ward").html('<option value="-1">Vui lòng chọn Phường/ Xã</option>');
        var iddistrict = $("#district").val();
        var url = "../../api/ward/"+iddistrict;
        $.getJSON(url, function(result){
            console.log(result);
            $.each(result, function(i, field){
                var id   = field.wardid;
                var name = field.name;
                var type = field.type;
                $("#ward").append('<option value="'+id+'">'+type+" "+name+'</option>');
        });
    });
});
});
</script>
@endsection