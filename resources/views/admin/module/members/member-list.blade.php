@extends('admin.welcome')

@section('breadcome')
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-top: -23px;">
                            <ul class="breadcome-menu">
                                <li><a href="{{route('index')}}">{{trans('message.trangchu')}}</a> <span class="bread-slash">/</span>
                                </li>
                                <li><span class="bread-blod">{{trans('message.danhsach')}}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="product-status mg-tb-15">
    @include('admin.blocks.alert')
    <div class="container-fluid">
        <div class="row">
        <!-- Static Table Start -->
            <div class="data-table-area mg-tb-15">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="sparkline13-list">
                                <div class="sparkline13-hd">
                                    <div class="main-sparkline13-hd">
                                        <h1>Danh sách <span class="table-project-n">Người</span> Dùng</h1>
                                         <div class="add-product">
                                            <a href="{{ route('member.create') }}">{{trans('message.btn_them')}}</a>
                                        </div>
                                    </div>
                                </div>
                                <form action="" name="listForm" id="listForm" method="post" >
                                    {!! csrf_field() !!}
                                    <div class="sparkline13-graph">
                                        <div class="datatable-dashv1-list custom-datatable-overright">
                                            <div id="toolbar">
                                                <select class="form-control">
                                                        <option value="">Xuất Bình Thường</option>
                                                        <option value="all">Xuất Tất Cả</option>
                                                        <option value="selected">Xuất Dòng Đã Chọn</option>
                                                </select>
                                            </div>
                                            <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true"
                                                data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                                <thead>
                                                    <tr>
                                                        <th data-field="state" data-checkbox="true"></th>
                                                        <th data-field="id">Mã</th>
                                                        <th data-field="name" data-editable="true">Họ tên</th>
                                                        <th data-field="date" data-editable="true">Phone</th>
                                                        <th data-field="email" data-editable="true">Email</th>
                                                        <th data-field="company" data-editable="true">Địa chỉ</th>
                                                        <th data-field="price" data-editable="true">Số lần mua</th>
                                                        <th data-field="task" data-editable="true">Tổng tiền mua</th>
                                                        <th data-field="vip">Khách vip</th>
                                                        <th data-field="action">Chọn</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($member as $item)
                                                    <tr>
                                                        <td></td>
                                                        <td>{{$item->id}}</td>
                                                        <td>{{$item->fullname}}</td>
                                                        <td>+84{{$item->phone}}</td>
                                                        <td>{{$item->email}}</td>
                                                        <td>
                                                            <?php 
                                                            $wardname           = "";
                                                            $districtname       = "";
                                                            $provincename       = "";
                                                            foreach ($province as $key) {
                                                                if($key->provinceid == $item->province)
                                                                {
                                                                    $provincename = $key->type." ".$key->name;
                                                                }
                                                            }
                                                            foreach ($district as $key) {
                                                                if($key->districtid == $item->district)
                                                                {
                                                                    $districtname = $key->type." ".$key->name;
                                                                }
                                                            }
                                                            foreach ($ward as $key) {
                                                                if($key->wardid ==  $item->ward)
                                                                {
                                                                    $wardname = $key->type." ".$key->name;
                                                                }
                                                            }
                                                            ?>
                                                            {{$item->diachi}} {{$wardname}} {{$districtname}} {{$provincename}}</td>
                                                        <td>{{$item->solanmua}}</td>
                                                        <td>{{convert_money($item->tongtienmua)}} đ</td>
                                                        <td>
                                                            @if($item->tongtienmua > getCauhinh('vipmoney'))
                                                                <span class="badge badge-warning" style="color: #212529;font-size:15px;background-color: #ffc107;">VIP</span>
                                                            @else
                                                                Thường
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('member.edit', ['member' => $item->id]) }}" data-toggle="tooltip" title="Sửa" class="pd-setting-ed">
                                                                <i class="fa fa-pencil-square-o" aria-hidden="true">{{trans('message.btn_sua')}}</i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        {{ method_field('DELETE') }}
                                    <button onClick="return xacnhanxoa('Bạn có chắc chắn muốn xóa ?');" id="btnXoaList" name="btnXoaList" type="submit" data-toggle="tooltip" title="Xóa" class="pd-setting-ed-delete">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i> {{trans('message.btn_xoa')}}
                                    </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Static Table End -->
    </div>
</div>
    
@endsection