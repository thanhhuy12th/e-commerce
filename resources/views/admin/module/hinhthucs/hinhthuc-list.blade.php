@extends('admin.welcome')

@section('breadcome')
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-top: -23px;">
                            <ul class="breadcome-menu">
                                <li><a href="{{route('index')}}">{{trans('message.trangchu')}}</a> <span class="bread-slash">/</span>
                                </li>
                                <li><span class="bread-blod">{{trans('message.danhsach')}}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="product-status mg-tb-15">
    @include('admin.blocks.alert')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="product-status-wrap">
                    <h4>{{trans('message.danhsach_hinhthuc')}}</h4>
                    <div class="add-product">
                        <a href="{{ route('hinhthuc.create') }}">{{trans('message.btn_them')}}</a>
                    </div>
                    <form action="" name="listForm" id="listForm" method="post" >
                        {!! csrf_field() !!}
                    <table>
                        <tr>
                            
                            <td>
                                <input type="checkbox" name="checkAll" onClick="checkallClick(checkAll);">
                            </td>
                            <th>Tên</th>
                            <th>Vitri</th>
                            <th>Chọn</th>
                        </tr>
                        @foreach($hinhthuc as $item)
                        <tr style="vertical-align: top">
                            
                            <td>
                                <input value="{{ $item->id }}" type="checkbox" name="checked[]">         
                            </td>
                            <td style="width: 50%">{{$item->name}}</td>
                            <td style="width: 30%">{{$item->vitri}}</td>
                            <td style="width: 20%">
                                <a href="{{ route('hinhthuc.edit', ['hinhthuc' => $item->id]) }}" data-toggle="tooltip" title="Sửa" class="pd-setting-ed">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true">{{trans('message.btn_sua')}}</i>
                                </a>
                            </td>                   
                        </tr>
                        @endforeach
                    </table>
                    {{ method_field('DELETE') }}
                    <button onClick="return xacnhanxoa('Bạn có chắc chắn muốn xóa ?');" id="btnXoaList" name="btnXoaList" type="submit" data-toggle="tooltip" title="Xóa" class="pd-setting-ed-delete">
                        <i class="fa fa-trash-o" aria-hidden="true"></i> {{trans('message.btn_xoa')}}
                    </button>
                    </form>
                     {{$hinhthuc->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection