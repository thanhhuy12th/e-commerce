@extends('admin.welcome')

@section('breadcome')
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-top: -23px;">
                            <ul class="breadcome-menu">
                                <li><a href="{{ route('index') }}">{{trans('message.trangchu')}}</a>
                                    <span class="bread-slash">/</span>
                                </li>
                                <li><a href="{{ route('dmsp.index') }}">{{trans('message.danhsach')}}</a>
                                    <span class="bread-blod">/</span>
                                </li>
                                <li><span class="bread-blod">{{trans('message.btn_sua')}}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<style type="text/css">
    .edit{
        /*background: #EEECEC;*/
        padding: 10px;
        font-size: 26px;
        font-weight: bold;
        /*border-radius: 5px;*/
    }
</style>
<form action="{{ route('dmsp.update', ['dmsp' => $dmsp->id]) }}" method="POST" enctype="multipart/form-data">
    @csrf
    {{ method_field('PUT') }}
<div class="single-product-tab-area mg-tb-15">
    @include('admin.blocks.alert')
    <!-- Single pro tab review Start-->
    <div class="single-pro-review-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="review-tab-pro-inner">
                        <div class="color-line"></div>
                        <span class="edit">{{trans('message.btn_sua')}}</span>
                        <div class="color-line"></div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="review-content-section">
                                        <div class="input-group mg-b-pro-edt">
                                            <span class="input-group-addon">Tên danh mục</span>
                                            <input value="{{old('title',$dmsp->name)}}" type="text" name="name" class="form-control" placeholder="Tên danh mục ">
                                        </div>
                                        <div class="input-group mg-b-pro-edt">
                                            <span class="input-group-addon">Alias</span>
                                            <input value="{{old('alias',$dmsp->alias)}}" type="text" name="alias" class="form-control" placeholder="Alias">
                                        </div>
                                    </div>
                                </div>
                               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="review-content-section">
                                    <div class="input-group mg-b-pro-edt">
                                        <span class="input-group-addon">Danh mục</span>
                                        <select name="id_parent" class="form-control pro-edt-select form-control-primary">
                                            <option value="">Please Choose Category</option>
                                            @if($dmsp->id_parent == 0)
                                                @for($i = 0; $i < count($optArr); $i++)
                                                <option value="{!!$optArr[$i]['id']!!}" {{ $dmsp->id == $optArr[$i]['id'] ? "selected" : "" }}>{!!$optArr[$i]['name']!!}</option>
                                                @endfor
                                            @else
                                            @for($i = 0; $i < count($optArr); $i++)
                                                <option value="{!!$optArr[$i]['id']!!}" {{ $dmsp->id_parent == $optArr[$i]['id'] ? "selected" : "" }}>{!!$optArr[$i]['name']!!}</option>
                                            @endfor
                                            @endif
                                        </select>
                                    </div>
                                    <div class="input-group mg-b-pro-edt">
                                        <span class="input-group-addon ">Vị trí</span>
                                        <input value="{{old('vitri',$dmsp->vitri)}}" type="text" name="vitri" class="form-control" placeholder="Vị trí">
                                    </div>
                                </div>
                                </div>
                            </div>
                        <div style="padding-top: 30px" class="mg-b-pro-edt custom-pro-edt-ds">
                            <button name="addTintuc" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                            </button>
                            <button type="button" class="btn btn-warning waves-effect waves-light">Discard
                            </button></div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>
</form>
@endsection