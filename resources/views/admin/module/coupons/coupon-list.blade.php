@extends('admin.welcome')
@section('breadcome')
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 " style="margin-top: -23px;">
                            <ul class="breadcome-menu">
                                <li><a href="{{route('index')}}">{{trans('message.trangchu')}}</a> <span class="bread-slash">/</span>
                                </li>
                                <li><span class="bread-blod">{{trans('message.danhsach')}}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="product-status mg-tb-15">
    @include('admin.blocks.alert')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="product-status-wrap">
                    <h4>{{trans('message.danhsach_phieu')}}</h4>
                    <div class="add-product">
                        <a href="{{ route('coupon.create') }}">{{trans('message.btn_them')}}</a>
                    </div>
                    <form action="" name="listForm" id="listForm" method="post" >
                        {!! csrf_field() !!}
                    <table>
                        <tr>
                            <td>
                                <input type="checkbox" name="checkAll" onClick="checkallClick(checkAll);">
                            </td>
                            <th>Mã phiếu</th>
                            <th>Số tiền</th>
                            <th>Đơn vị</th>
                            <th>Loại giảm giá</th>
                            <th>Bắt đầu</th>
                            <th>Kết thúc</th>
                            <th>Thời hạn</th>
                            <th>Chọn</th>
                        </tr>
                        @foreach($coupon as $item)
                        <tr style="vertical-align: top">
                            <td>
                                <input value="{{ $item->id }}" type="checkbox" name="checked[]">         
                            </td>
                            <td style="width: 10%">{{$item->id}}</td>
                            <td style="width: 10%">
                                @if($item->donvi==0)
                                    <?php echo number_format($item->money, 0, '.', ',');?>
                                @else
                                    {{$item->money}}
                                @endif
                            </td>
                             <td style="width: 7%">
                                @if($item->donvi==0)
                                    VNĐ
                                @else
                                    %
                                @endif
                            </td>
                            <td style="width: 17%">
                                @if($item->loai==0)
                                    Giảm theo giá
                                @else
                                    Giảm theo sản phẩm
                                @endif
                            </td>
                            <td style="width: 15%">
                                <?php
                                    $date = convertdate($item->begin); 
                                    echo date_format($date, 'd-m-Y');
                                ?>
                            </td>
                            <td style="width: 15%">
                                <?php
                                    $date = convertdate($item->end); 
                                    echo date_format($date, 'd-m-Y');
                                ?>
                            </td>
                            <td style="width: 16%">
                                {{convertdate2($item->begin,$item->end)}}
                            </td>
                            <td style="width: 10%">
                                <a href="{{ route('coupon.edit', ['coupon' => $item->id]) }}" data-toggle="tooltip" title="Sửa" class="pd-setting-ed">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true">{{trans('message.btn_sua')}}</i>
                                </a>
                            </td>                   
                        </tr>
                        @endforeach
                    </table>
                    {{ method_field('DELETE') }}
                    <button onClick="return xacnhanxoa('Bạn có chắc chắn muốn xóa ?');" id="btnXoaList" name="btnXoaList" type="submit" data-toggle="tooltip" title="Xóa" class="pd-setting-ed-delete">
                        <i class="fa fa-trash-o" aria-hidden="true"></i> {{trans('message.btn_xoa')}}
                    </button>
                    </form>
                     {{$coupon->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection