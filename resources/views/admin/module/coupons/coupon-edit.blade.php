@extends('admin.welcome')
@section('breadcome')
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-top: -23px;">
                            <ul class="breadcome-menu">
                                <li><a href="{{ route('index') }}">{{trans('message.trangchu')}}</a>
                                    <span class="bread-slash">/</span>
                                </li>
                                <li><a href="{{ route('coupon.index') }}">{{trans('message.danhsach')}}</a>
                                    <span class="bread-blod">/</span>
                                </li>
                                <li><span class="bread-blod">{{trans('message.btn_sua')}}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<style type="text/css">
    .edit{
        /*background: #EEECEC;*/
        padding: 10px;
        font-size: 26px;
        font-weight: bold;
        /*border-radius: 5px;*/
    }
</style>
<form action="{{ route('coupon.update', ['coupon' => $coupon->id]) }}" method="POST" enctype="multipart/form-data">
    @csrf
    {{ method_field('PUT') }}
<div class="single-product-tab-area mg-tb-15">
    @include('admin.blocks.alert')
    <!-- Single pro tab review Start-->
    <div class="single-pro-review-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="review-tab-pro-inner">
                        <span class="edit">{{trans('message.btn_sua')}}</span>
                        <hr>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="review-content-section">
                                        <div class="input-group mg-b-pro-edt">
                                            <span class="input-group-addon">Mã phiếu</span>
                                            <input value="{{old('id',$coupon->id)}}" type="text" name="id" class="form-control" placeholder="Mã phiếu">
                                        </div>
                                        <div class="input-group mg-b-pro-edt">
                                            <span class="input-group-addon">Thời gian</span>
                                            <?php $date = date("Y-m-d", strtotime($coupon->begin)); ?>
                                            <input value="{{old('begin',$date)}}" type="date" name="begin" class="form-control" placeholder="Nhập ngày">
                                            <span class="input-group-addon">Đến</span>
                                            <?php $date = date("Y-m-d", strtotime($coupon->end)); ?>
                                            <input value="{{old('end',$date)}}" type="date" name="end" class="form-control" placeholder="Nhập ngày">
                                        </div>
                                    </div>
                                </div>
                               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-1  2">
                                    <div class="review-content-section">
                                        <div class="input-group mg-b-pro-edt">
                                            <span class="input-group-addon">Money</span>
                                            <input value="{{old('money',$coupon->money)}}" type="text" name="money" class="form-control" placeholder="Nhập số tiền">
                                            <div class="input-group-btn custom-dropdowns-button open">
                                                <select name="donvi" style="background: #E6E6E6;color: #333;border: 2px solid #BDBDBD;height: 100% " data-toggle="dropdown" class="btn dropdown-toggle" type="button" aria-expanded="true">
                                                    @if($coupon->donvi == 0)
                                                    <option value="{{old('donvi',$coupon->donvi)}}" selected="">VNĐ</option>
                                                    <option value="1">%</option>
                                                    @else
                                                    <option value="0">VNĐ</option>
                                                    <option value="{{old('donvi',$coupon->donvi)}}" selected="">%</option>
                                                    @endif
                                                </select>
                                            </div>  
                                        </div>
                                        <div class="input-group mg-b-pro-edt">
                                            <span class="input-group-addon">Loại giảm giá</span>
                                            <select name="loai" id="loai" class="form-control">
                                                @if($coupon->loai == 0)
                                                    <option value="1">Giảm theo sản phẩm</option>
                                                    <option value="{{old('loai',$coupon->loai)}}" selected="">Giảm theo giá đơn hàng</option>
                                                @else
                                                    <option value="{{old('loai',$coupon->loai)}}" selected="">Giảm theo sản phẩm</option>
                                                    <option value="0">Giảm theo giá đơn hàng</option>
                                                @endif
                                            </select>
                                        </div>
                                        <div class="input-group mg-b-pro-edt" id="donhang">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div style="padding-top: 30px" class="mg-b-pro-edt custom-pro-edt-ds">
                            <button name="addTintuc" type="submit" class="btn btn-primary waves-effect waves-light m-r-10">{{trans('message.btn_luu')}}
                            </button>
                            <button type="button" class="btn btn-warning waves-effect waves-light">Discard
                            </button></div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<script type="text/javascript">
    $(document).ready(function(){
        $("#loai").change(function(){
            var id = $("#loai").val();
            console.log(id);
            if(id==0)
            {
                $("#donhang").html('<span>Nhập tổng số tiền của đơn hàng để có thể áp dụng mã giảm giá</span><input value="{{old('tongtien',$coupon->tongtien)}}" type="text" name="tongtien" class="form-control" placeholder="VD: 10000">');
            }
            else
            {
                $("#donhang").html('');
            }

        });
        $("#loai option:selected").each(function() {
            var id        = parseInt($(this).val());
            if(id==0)
            {
                $("#donhang").html('<span>Nhập tổng số tiền của đơn hàng để có thể áp dụng mã giảm giá</span><input value="{{old('tongtien',$coupon->tongtien)}}" type="text" name="tongtien" class="form-control" placeholder="VD: 10000">');
            }
        });
    });
</script>
@endsection