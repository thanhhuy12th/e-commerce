<!doctype html>
<html class="no-js" lang="en">

<head>
    @include('admin.blocks.head')
</head>

<body>
    @include('admin.blocks.menu')
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
        @include('admin.blocks.header')
        @yield('content')
        @include('admin.blocks.footer')
    </div>
    @include('admin.blocks.script')
</body>

</html>