    <!-- jquery
        ============================================ -->
    <script src="{{ asset('frontend/asset_admin/js/vendor/jquery-1.11.3.min.js') }}"></script>
    <!-- bootstrap JS
        ============================================ -->
    <script src="{{ asset('frontend/asset_admin/js/bootstrap.min.js') }}"></script>
    <!-- wow JS
        ============================================ -->
    <script src="{{ asset('frontend/asset_admin/js/wow.min.js') }}"></script>
    <!-- price-slider JS
        ============================================ -->
    <script src="{{ asset('frontend/asset_admin/js/jquery-price-slider.js') }}"></script>
    <!-- meanmenu JS
        ============================================ -->
    <script src="{{ asset('frontend/asset_admin/js/jquery.meanmenu.js') }}"></script>
    <!-- owl.carousel JS
        ============================================ -->
    <script src="{{ asset('frontend/asset_admin/js/owl.carousel.min.js') }}"></script>
    <!-- sticky JS
        ============================================ -->
    <script src="{{ asset('frontend/asset_admin/js/jquery.sticky.js') }}"></script>
    <!-- scrollUp JS
        ============================================ -->
    <script src="{{ asset('frontend/asset_admin/js/jquery.scrollUp.min.js') }}"></script>
    <!-- mCustomScrollbar JS
        ============================================ -->
    <script src="{{ asset('frontend/asset_admin/js/scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/scrollbar/mCustomScrollbar-active.js') }}"></script>
    <!-- metisMenu JS
        ============================================ -->
    <script src="{{ asset('frontend/asset_admin/js/metisMenu/metisMenu.min.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/metisMenu/metisMenu-active.js') }}"></script>
    <!-- morrisjs JS
        ============================================ -->
    <script src="{{ asset('frontend/asset_admin/js/morrisjs/raphael-min.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/morrisjs/morris.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/morrisjs/morris-active.js') }}"></script>
    <!-- morrisjs JS
        ============================================ -->
    <script src="{{ asset('frontend/asset_admin/js/sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/sparkline/jquery.charts-sparkline.js') }}"></script>
    <!-- calendar JS
        ============================================ -->
    <script src="{{ asset('frontend/asset_admin/js/calendar/moment.min.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/calendar/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/calendar/fullcalendar-active.js') }}"></script>
    <!-- plugins JS
        ============================================ -->
    <script src="{{ asset('frontend/asset_admin/js/plugins.js') }}"></script>
    <!-- main JS
        ============================================ -->
     <script src="{{ asset('frontend/asset_admin/js/main.js') }}"></script>    
    <!-- notification JS
        ============================================ -->
    <script src="{{ asset('frontend/asset_admin/js/notifications/Lobibox.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/notifications/notification-active.js') }}"></script>
    <!-- Chart JS
        ============================================ -->
    <script src="{{ asset('frontend/asset_admin/js/chart/jquery.peity.min.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/peity/peity-active.js') }}"></script>
   

    <script src="{{ asset('frontend/asset_admin/js/dropzone/dropzone.js') }}"></script>

    <script src="{{ asset('frontend/asset_admin/js/tab.js') }}"></script>

    <script src="{{ asset('frontend/asset_admin/js/myscript.js') }}"></script>

    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    
    <script> CKEDITOR.replace('editor1'); </script>

    <script> CKEDITOR.replace('editor2'); </script>

    <script> CKEDITOR.replace('editor3'); </script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- data table JS
        ============================================ -->
    <script src="{{ asset('frontend/asset_admin/js/data-table/bootstrap-table.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/data-table/tableExport.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/data-table/data-table-active.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/data-table/bootstrap-table-editable.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/data-table/bootstrap-editable.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/data-table/bootstrap-table-resizable.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/data-table/colResizable-1.5.source.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/data-table/bootstrap-table-export.js') }}"></script>
    <!--  editable JS
        ============================================ -->
    <script src="{{ asset('frontend/asset_admin/js/editable/jquery.mockjax.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/editable/mock-active.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/editable/select2.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/editable/moment.min.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/editable/bootstrap-datetimepicker.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/editable/bootstrap-editable.js') }}"></script>
    <script src="{{ asset('frontend/asset_admin/js/editable/xediable-active.js') }}"></script>
