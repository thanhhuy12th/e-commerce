<div class="left-sidebar-pro">
    <nav id="sidebar" class="">
        <div class="sidebar-header">
            <a href="{{ route('index') }}">
                <img style="width: 120px" class="main-logo" src="{{ asset('frontend/asset_admin/img/logo/cloudone.png') }}" alt="" /></a>
            <strong>
                <img src="{{ asset('frontend/asset_admin/img/logo/cloudone.png') }}" alt="" />
            </strong>
        </div>
        <div class="left-custom-menu-adp-wrap comment-scrollbar" >
            <nav class="sidebar-nav left-sidebar-menu-pro" >
                <ul class="metismenu" id="menu1" style="font-size: 14px">
                    @if(Auth::check())
                        @if(Auth::user()->rule==0)
                        <!-- Trang chu -->
                        <li>
                            <a href="{{ route('index') }}" aria-expanded="false">
                                <i class="fa big-icon fa-home icon-wrap"></i> 
                                <span class="mini-click-non">Trang chủ</span>
                            </a>
                        </li>
                        <!-- Tai khoan -->
                        <li >
                            <a class="has-arrow" href="index.html">
                                   <i class="fa big-icon fa-users icon-wrap"></i>
                                   <span class="mini-click-non">Tài khoản</span>
                                </a>
                            <ul class="submenu-angle" aria-expanded="true">
                                <li>
                                    <a title="Quản trị viên" href="{{route('useradmin.index')}}">
                                        <i class="fa fa-user-secret sub-icon-mg" aria-hidden="true"></i> 
                                        <span class="mini-sub-pro">Quản trị viên</span>
                                    </a>
                                </li>
                                <li>
                                    <a title="Khách hàng" href="{{route('member.index')}}">
                                        <i class="fa fa-user-md sub-icon-mg" aria-hidden="true"></i> 
                                        <span class="mini-sub-pro">Khách hàng</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- San pham class="active" -->
                        <li >
                            <a class="has-arrow" href="index.html">
                                   <i class="fa big-icon fa-shopping-cart icon-wrap"></i>
                                   <span class="mini-click-non">Sản phẩm</span>
                                </a>
                            <ul class="submenu-angle" aria-expanded="true">
                                <li>
                                    <a title="Danh mục" href="{{route('dmsp.index')}}">
                                        <i class="fa fa-bars sub-icon-mg" aria-hidden="true"></i>
                                        <span class="mini-sub-pro">Danh mục</span>
                                    </a>
                                </li>
                                <li>
                                    <a title="Sản phẩm" href="{{route('sanpham.index')}}">
                                        <i class="fa fa-archive sub-icon-mg" aria-hidden="true"></i>
                                        <span class="mini-sub-pro">Sản phẩm</span>
                                    </a>
                                </li>
                                <li>
                                    <a title="Đơn hàng" href="{{route('donhang.index')}}">
                                        <i class="fa fa-opencart sub-icon-mg" aria-hidden="true"></i>
                                        <span class="mini-sub-pro">Đơn hàng</span>
                                    </a>
                                </li>
                                <li>
                                    <a title="Coupon" href="{{route('coupon.index')}}">
                                        <i class="fa fa-ticket sub-icon-mg" aria-hidden="true"></i> 
                                        <span class="mini-sub-pro">Coupon</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- Tin tuc class="active" -->
                        <li >
                            <a class="has-arrow" href="index.html">
                                   <i class="fa big-icon fa-shopping-cart icon-wrap"></i>
                                   <span class="mini-click-non">Tin tức</span>
                                </a>
                            <ul class="submenu-angle" aria-expanded="true">
                                <li>
                                    <a title="Dashboard" href="{{route('dmtt.index')}}">
                                        <i class="fa fa-bars sub-icon-mg" aria-hidden="true"></i> 
                                        <span class="mini-sub-pro">Danh mục</span>
                                    </a>
                                </li>
                                <li>
                                    <a title="Product List" href="{{route('tintuc.index')}}">
                                        <i class="fa fa-newspaper-o sub-icon-mg" aria-hidden="true"></i>
                                        <span class="mini-sub-pro">Tin tức</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- Nhan xet KH -->
                        <li>
                            <a href="{{route('nhanxet.index')}}" aria-expanded="false">
                                <i class="fa big-icon fa-commenting icon-wrap"></i> 
                                <span class="mini-click-non">Nhận xét</span>
                            </a>
                        </li>
                        <!-- Slider -->
                        <li>
                            <a href="{{route('slide.index')}}" aria-expanded="false">
                                <i class="fa big-icon fa-picture-o icon-wrap"></i> 
                                <span class="mini-click-non">Slide</span>
                            </a>
                        </li>
                        <!-- Lien he -->
                        <li>
                            <a href="{{route('lienhe.index')}}" aria-expanded="false">
                                <i class="fa big-icon fa-envelope icon-wrap"></i> 
                                <span class="mini-click-non">Liên hệ</span>
                            </a>
                        </li>
                        <!-- Cau hinh -->
                        <li>
                            <a class="has-arrow active" href="mailbox.html" aria-expanded="false">
                                <i class="fa big-icon fa-cog icon-wrap"></i>
                                <span class="mini-click-non">Cấu hình</span>
                            </a>
                            <ul class="submenu-angle" aria-expanded="false">
                                <li>
                                    <a title="Cấu hình chung" href="{{route('cauhinh.editNoId')}}">
                                        <i class="fa fa-cogs sub-icon-mg" aria-hidden="true"></i> 
                                        <span class="mini-sub-pro">Cấu hình chung</span>
                                    </a>
                                </li>
                                <li>
                                    <a title="Hình thức thanh" href="{{route('hinhthuc.index')}}">
                                        <i class="fa fa-file-pdf-o sub-icon-mg" aria-hidden="true"></i> 
                                        <span class="mini-sub-pro">Thanh toán</span>
                                    </a>
                                </li>
                                <li>
                                    <a title="Hình thức thanh" href="{{route('vanchuyen.index')}}">
                                        <i class="fa fa-file-pdf-o sub-icon-mg" aria-hidden="true"></i> 
                                        <span class="mini-sub-pro">Phí vận chuyển</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        @elseif(Auth::user()->rule==1)
                        <!-- Tin tuc class="active" -->
                        <li >
                            <a class="has-arrow" href="index.html">
                                   <i class="fa big-icon fa-shopping-cart icon-wrap"></i>
                                   <span class="mini-click-non">Tin tức</span>
                                </a>
                            <ul class="submenu-angle" aria-expanded="true">
                                <li>
                                    <a title="Dashboard" href="{{route('dmtt.index')}}">
                                        <i class="fa fa-bars sub-icon-mg" aria-hidden="true"></i> 
                                        <span class="mini-sub-pro">Danh mục</span>
                                    </a>
                                </li>
                                <li>
                                    <a title="Product List" href="{{route('tintuc.index')}}">
                                        <i class="fa fa-archive sub-icon-mg" aria-hidden="true"></i> 
                                        <span class="mini-sub-pro">Tin tức</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- Nhan xet KH -->
                        <li>
                            <a href="{{route('nhanxet.index')}}" aria-expanded="false">
                                <i class="fa big-icon fa-commenting icon-wrap"></i> 
                                <span class="mini-click-non">Nhận xét</span>
                            </a>
                        </li>
                        @elseif(Auth::user()->rule==2)
                        <!-- San pham class="active" -->
                        <li >
                            <a class="has-arrow" href="index.html">
                                   <i class="fa big-icon fa-shopping-cart icon-wrap"></i>
                                   <span class="mini-click-non">Sản phẩm</span>
                                </a>
                            <ul class="submenu-angle" aria-expanded="true">
                                <li>
                                    <a title="Danh mục" href="home">
                                        <i class="fa fa-bars sub-icon-mg" aria-hidden="true"></i> 
                                        <span class="mini-sub-pro">Danh mục</span>
                                    </a>
                                </li>
                                <li>
                                    <a title="Sản phẩm" href="{{route('sanpham.index')}}">
                                        <i class="fa fa-archive sub-icon-mg" aria-hidden="true"></i> 
                                        <span class="mini-sub-pro">Sản phẩm</span>
                                    </a>
                                </li>
                                <li>
                                    <a title="Đơn hàng" href="home">
                                        <i class="fa fa-opencart sub-icon-mg" aria-hidden="true"></i> 
                                        <span class="mini-sub-pro">Đơn hàng</span>
                                    </a>
                                </li>
                                <li>
                                    <a title="Coupon" href="{{route('coupon.index')}}">
                                        <i class="fa fa-opencart sub-icon-mg" aria-hidden="true"></i> 
                                        <span class="mini-sub-pro">Coupon</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        @endif
                    @endif
                </ul>
            </nav>
        </div>
    </nav>
</div>