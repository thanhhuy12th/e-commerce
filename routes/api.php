<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

	// Route::get('thay-doi-dia-chi', 'ApiController@getAddress')->name('trangchu.getAddress');
	Route::get('/province', 'ApiController@getProvince');
	Route::get('/province/{id}', 'ApiController@getProvince');

	Route::get('/district', 'ApiController@getDistrict');
	Route::get('/district/{id}', 'ApiController@getDistrict');

	Route::get('/ward', 'ApiController@getWard');
	Route::get('/ward/{id}', 'ApiController@getWard');

	Route::get('/hinhthuc', 'ApiController@getHinhthuc');
	Route::get('/hinhthuc/{id}', 'ApiController@getHinhthuc');

	Route::get('/vanchuyen', 'ApiController@getVanchuyen');
	Route::get('/vanchuyen/{id}', 'ApiController@getVanchuyen');

	Route::get('/giamgia', 'ApiController@getGiamgia');
