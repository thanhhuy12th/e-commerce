<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::namespace('Frontend')->group(function(){
	Route::get('/','HomeController@index')->name('trangchu.index');
	Route::get('trang-chu','HomeController@index')->name('trangchu.index');
	Route::get('danh-muc-san-pham/{alias}','HomeController@getPages')->name('trangchu.getPages');
	Route::get('danh-muc-nho/{alias}','HomeController@getPagesCon')->name('trangchu.getPagesCon');
	Route::get('chi-tiet-san-pham/{alias}','HomeController@getDetail')->name('trangchu.detail');
	Route::get('chi-tiet-tin-tuc/{alias}','HomeController@getTintucDetail')->name('trangchu.tintucdetail');
	Route::get('gioi-thieu','HomeController@Gioithieu')->name('trangchu.gioithieu');
	Route::get('lien-he','HomeController@Lienhe')->name('trangchu.lienhe');
	Route::post('lien-he/create','HomeController@store')->name('trangchu.store');
	Route::get('trang-chu/dang-nhap','LoginController@getLogin')->name('trangchu.getLogin');
	Route::post('trang-chu/dang-nhap','LoginController@postLogin')->name('trangchu.postLogin');
	Route::get('trang-chu/dang-ky','LoginController@getRegister')->name('trangchu.getRegister');
	Route::post('trang-chu/dang-ky','LoginController@postRegister')->name('trangchu.postRegister');
	Route::get('dang-xuat','LoginController@getLogout')->name('trangchu.getLogout');
	Route::get('dang-xuat2','LoginController@getLogout2')->name('trangchu.getLogout2');
	Route::get('thong-tin-ca-nhan', 'LoginController@getInformation')->name('trangchu.getInformation');
	Route::post('thay-doi-mat-khau', 'LoginController@postPassword')->name('trangchu.postPassword');
	Route::get('thay-doi-dia-chi', 'LoginController@getAddress')->name('trangchu.getAddress');
	Route::post('thay-doi-dia-chi', 'LoginController@postAddress')->name('trangchu.postAddress');
	Route::get('thay-doi-email', 'LoginController@getChangeEmail')->name('trangchu.getChangeEmail');
	Route::post('thay-doi-email', 'LoginController@postChangeEmail')->name('trangchu.postChangeEmail');
	Route::get('tim-mat-khau', 'ResetPasswordController@getTimMatKhau')->name('trangchu.getTimMatKhau');
	Route::post('tim-mat-khau', 'ResetPasswordController@sendEmail')->name('trangchu.sendEmail');
	Route::get('mat-khau-moi', 'ResetPasswordController@getNewPassword')->name('trangchu.getNewPassword');
	Route::post('mat-khau-moi', 'ResetPasswordController@postNewPassword')->name('trangchu.postNewPassword');

	Route::get('them-san-pham/{id}','ShoppingCartController@addProductToCart')->name('get.add.product');
	Route::get('gio-hang-cua-ban','ShoppingCartController@listCartProduct')->name('get.list.cart');
	Route::get('so-luong','ShoppingCartController@updownQtyProduct')->name('updown.product');
	Route::get('xoa-san-pham/{id}','ShoppingCartController@detroyProductCart')->name('get.delete.product');
	Route::get('xoa-tat-ca','ShoppingCartController@detroyAllProductCart')->name('get.delete.all.product');
	Route::get('xem-lai-hoa-don', 'ShoppingCartController@getRepostCart')->name('get.RepostCart');
	Route::get('dat-hang', 'ShoppingCartController@getDathang')->name('get.Dathang');
	Route::post('dat-hang', 'ShoppingCartController@postDathang')->name('post.Dathang');
	Route::get('/datacart', 'ShoppingCartController@getCart');
}); 


Route::namespace('Admin')->group(function(){
	Route::get('login','LoginController@getLogin')->middleware('loginal')->name('admin.getLogin');
	Route::post('login','LoginController@postLogin')->name('admin.postLogin');;
	Route::get('logout','LoginController@getLogout')->name('admin.getLogout');
});

Route::namespace('Admin')->middleware('login')->prefix('admin')->group(function(){
	Route::resource('/','HomeController');
	Route::resource('sanpham','SanphamController');
	Route::resource('useradmin','UseradminController');
	Route::resource('member','MemberController');
	Route::resource('tintuc','TintucController');
	Route::resource('dmtt','DmttController');
	Route::resource('dmsp','DmspController');
	Route::resource('slide','SlideController');
	Route::resource('nhanxet','NhanxetController');
	Route::resource('coupon','CouponController');
	Route::resource('hinhthuc','HinhthucController');
	Route::resource('cauhinh','CauhinhController');
	Route::resource('lienhe','LienheController');
	Route::resource('vanchuyen','VanchuyenController');
	Route::resource('donhang','DonhangController');
	Route::get('cauhinh','CauhinhController@editNoId')->name('cauhinh.editNoId');
	Route::put('cauhinh','CauhinhController@updateNoId')->name('cauhinh.updateNoId');
	Route::get('in-hoa-don','DonhangController@getInhoadon')->name('hoadon.getInhoadon');
	Route::post('updatedata','DonhangController@updatedata');
	// Route::post('/update_data/{id}', 'DonhangController@update_data')->name('donhang.update_data');

});

