<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCtspTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skl_ctsp', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sanpham_id');
            $table->foreign('sanpham_id')->references('id')->on('skl_sanpham');
            $table->string('filename');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skl_ctsp');
    }
}
