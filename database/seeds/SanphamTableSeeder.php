<?php

use Illuminate\Database\Seeder;

class SanphamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for($i=1;$i<=50;$i++){
	        DB::table('skl_sanpham')->insert([
	         	'tensp'	=> 'Kim loại' .$i,
	         	'alias'	=> 'KL' .$i,
	         	'danhmuc' => rand(0,10),
	         	'type_product' => rand(0,2),
	         	'soluong' => rand(0,1000),
	         	'price' => rand(5000,100000)
	        ]);
 		}
    }
}
